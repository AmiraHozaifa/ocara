# Flavor 'orangewithoutsmtk"
This package contains all the data that is dedicated to the flavor  **orangewithoutsmtk**. 
```
productFlavors {
    orangewithoutsmtk {
        dimension 'mode'
        ...
    }
    ...
}
```
