/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.adapter;

import android.content.Context;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.orange.ocara.R;
import com.orange.ocara.business.model.RuleGroupModel;
import com.orange.ocara.business.model.RuleModel;
import com.orange.ocara.data.net.model.QuestionEntity;
import com.orange.ocara.data.net.model.RuleEntity;
import com.orange.ocara.ui.activity.BrowseExplanationsActivity_;
import com.orange.ocara.ui.model.OrderedRuleGroupUiModel;
import com.orange.ocara.ui.model.OrderedRuleUiModel;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Bridge between a list of {@link QuestionEntity}s and their displaying in the view .
 * <p>
 * Each question has an expandable list of {@link RuleEntity}s.
 * <p>
 * Implementation of {@link BaseExpandableListAdapter}
 */
public class RulesExpandableRVAdapter extends ExpandableRecyclerViewAdapter<RulesExpandableRVAdapter.RuleGrpViewHolder, RulesExpandableRVAdapter.RuleViewHolder> {

//    /**
//     * a ViewHolder for {@link OrderedRuleGroupUiModel}s
//     */
//    private class RuleGroupViewHolder {
//        TextView title;
//
//        RuleGroupViewHolder(View convertView) {
//            title = convertView.findViewById(R.id.question_title);
//        }
//
//        void bind(OrderedRuleGroupUiModel q) {
//            title.setText(q.getGroupText());
//        }
//    }
//
//    /**
//     * a ViewHolder for {@link OrderedRuleUiModel}s
//     */
//    private class RuleViewHolder {
//        TextView title;
//        View infoButton;
//
//        RuleViewHolder(View convertView) {
//            title = convertView.findViewById(R.id.rule_title);
//            infoButton = convertView.findViewById(R.id.rule_button_info);
//        }
//
//        void bind(OrderedRuleUiModel r) {
//            title.setText(r.getRuleText());
//
//            if (!r.isIllustrated()) {
//                infoButton.setVisibility(View.GONE);
//            } else {
//                infoButton.setVisibility(View.VISIBLE);
//                infoButton.setOnClickListener(new RuleInfoClickListener(r));
//            }
//        }
//    }

    /**
     * Coefficient used in arithmetic operations.
     * Identified as long, in order to prevent unexpected results.
     */
    private static final long ID_FACTOR = 100000L;

//    private List<OrderedRuleGroupUiModel> groups = new ArrayList<>();


    public class RuleGrpViewHolder extends GroupViewHolder {
        TextView title;
        boolean expanded = false;

        public RuleGrpViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.question_title);
//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    expanded = !expanded;
//                    int drawable = expanded ? R.drawable.ic_arrow_up_disabled : R.drawable.ic_arrow_down_disabled;
//                    title.setCompoundDrawablesWithIntrinsicBounds(drawable, 0, 0, 0);
//                }
//            });
        }

        void bind(OrderedRuleGroupUiModel q) {
            title.setText(q.getGroupText());
        }
    }

    public class RuleViewHolder extends ChildViewHolder {
        TextView title;
        View infoButton;

        public RuleViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.rule_title);
            infoButton = itemView.findViewById(R.id.rule_button_info);
        }

        void bind(OrderedRuleUiModel r) {
            title.setText(r.getRuleText());

            if (!r.isIllustrated()) {
                infoButton.setVisibility(View.GONE);
            } else {
                infoButton.setVisibility(View.VISIBLE);
                infoButton.setOnClickListener(new RuleInfoClickListener(r));
            }
        }
    }

    private final Context context;
    private final Fragment container;

    public static RulesExpandableRVAdapter build(List<RuleGroupModel> groups, List<RuleModel> rules, Fragment container) {
        List<OrderedRuleGroupUiModel> allGroups = new ArrayList<>();
        for (int i = 0; i < groups.size(); i++) {
            RuleGroupModel item = groups.get(i);

            List<OrderedRuleUiModel> children = new ArrayList<>();
            //.put(i, new ArrayList<>());
            for (RuleModel model : rules) {
                if (model.getGroupId() == item.getId()) {
                    children.add(new OrderedRuleUiModel(model));
                }
            }
            allGroups.add(new OrderedRuleGroupUiModel(item, children));
        }
        return new RulesExpandableRVAdapter(allGroups, container);
    }

    private RulesExpandableRVAdapter(List<? extends ExpandableGroup> groups, Fragment container) {
        super(groups);
//        this.groups = groups;
        this.container = container;
        this.context = container.getContext();
    }

    @Override
    public RuleGrpViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(context).inflate(R.layout.question_item_for_rules, parent, false);
        return new RuleGrpViewHolder(view);
    }

    @Override
    public RuleViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(context).inflate(R.layout.rule_item_for_rules, parent, false);
        return new RuleViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(RuleViewHolder holder, int flatPosition, ExpandableGroup group,
                                      int childIndex) {
        final OrderedRuleUiModel rule = ((OrderedRuleGroupUiModel) group).getItems().get(childIndex);
        holder.bind(rule);
    }

    @Override
    public void onBindGroupViewHolder(RuleGrpViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.bind(((OrderedRuleGroupUiModel) group));

    }

//    private SparseArray<List<OrderedRuleUiModel>> children = new SparseArray<>();

    /**
     * Updates the content of the adapter
     * <p>
     * //     * @param groups a {@link List} of {@link RuleGroupModel}s
     * //     * @param rules  a {@link List} of {@link RuleModel}s
     */
//    public void update(List<RuleGroupModel> groups, List<RuleModel> rules) {
//        this.getGroups().clear();
//
//        //children = new SparseArray<>();
//        for (int i = 0; i < groups.size(); i++) {
//            RuleGroupModel item = groups.get(i);
//
//            List<OrderedRuleUiModel> children = new ArrayList<>();
//            //.put(i, new ArrayList<>());
//            for (RuleModel model : rules) {
//                if (model.getGroupId() == item.getId()) {
//                    children.add(new OrderedRuleUiModel(model));
//                }
//            }
//            OrderedRuleGroupUiModel G = new OrderedRuleGroupUiModel(item, children);
//
////            List<OrderedRuleGroupUiModel> l = this.getGroups();
//        }
//
//        notifyDataSetChanged();
//    }
    public void reset() {
//        update(Collections.emptyList(), Collections.emptyList());
        getGroups().clear();
        notifyDataSetChanged();
    }


    /**
     * Callback that should be invoked when a uiModel is clicked
     * <p>
     * Implementation of {@link View.OnClickListener}
     */
    private class RuleInfoClickListener implements View.OnClickListener {

        private final OrderedRuleUiModel uiModel;

        /**
         * Instantiate
         *
         * @param rule a {@link RuleEntity}
         */
        RuleInfoClickListener(OrderedRuleUiModel rule) {
            this.uiModel = rule;
        }

        @Override
        public void onClick(View v) {

//            BrowseExplanationsActivity_
//                    .intent(context)
//                    .ruleId(uiModel.getRuleId())
//                    .start();

            Bundle bundle = new Bundle();
//            bundle.putString("iconName", iconName);
            bundle.putLong("ruleId", uiModel.getRuleId());
            NavHostFragment.findNavController(container.getParentFragment()).navigate(R.id.action_browse_explainations, bundle);
        }
    }


}
