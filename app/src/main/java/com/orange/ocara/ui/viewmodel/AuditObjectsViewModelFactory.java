package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import java.lang.reflect.InvocationTargetException;

public class AuditObjectsViewModelFactory implements ViewModelProvider.Factory {

    Context context;
    private long[] selectedObjects;

    public AuditObjectsViewModelFactory(Context context, long[] selectedObjects) {
        this.context = context;
        this.selectedObjects = selectedObjects;
    }

    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (AuditObjectsViewModel.class.isAssignableFrom(modelClass)) {
            try {
                return modelClass.getConstructor(Context.class, long[].class).
                newInstance(context, selectedObjects);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return null;
//        else {
//            throw IllegalStateException("ViewModel must extend MajesticViewModel");
//        }
    }

//    @Override
//    public <T extends ViewModel> T create(Class<T> modelClass) {
//        return (T) new AuditObjectsNoviceViewModel(context, selectedObjects);
//    }
}