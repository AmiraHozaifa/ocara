/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.orange.ocara.data.cache.model.CommentEntity;
import com.orange.ocara.ui.view.CommentItemView;
import com.orange.ocara.ui.view.CommentItemView_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

@EBean
public class CommentRVAdapter extends ItemRvAdapter<CommentEntity, CommentRVAdapter.CommentViewHolder> {

    public class CommentViewHolder extends RecyclerView.ViewHolder {

        CommentItemView commentView;

        public CommentViewHolder(@NonNull View itemView) {
            super(itemView);
            commentView = (CommentItemView) itemView;
        }

        public void bind(int position) {
            CommentEntity entity = getItem(position);
            commentView.bind(entity);
        }
    }

//    @RootContext
//    Activity mActivity;

    @NonNull
    @Override
    public CommentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View itemView = mInflater.inflate(R.layout.audit_item, parent, false);

        CommentItemView itemView = CommentItemView_.build(parent.getContext());
        return new CommentViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
//        AuditEntity audit = getItem(position);
        holder.bind(position);
//        CommentEntity comment = getItem(position);
//        commentItemView.bind(comment);

    }
//
//    @Override
//    public View getView(final int position, View convertView, ViewGroup parent) {
//        CommentItemView commentItemView;
//
//        if (convertView != null && convertView instanceof CommentItemView) {
//            commentItemView = (CommentItemView) convertView;
//        } else {
//            commentItemView = CommentItemView_.build(mActivity);
//        }
//
//        CommentEntity comment = getItem(position);
//        commentItemView.bind(comment);
//        return commentItemView;
//    }
}
