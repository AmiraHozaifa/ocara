/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.fragment.editcomment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.orange.ocara.R;
import com.orange.ocara.data.cache.model.CommentEntity;
import com.orange.ocara.ui.activity.EditCommentActivity;
import com.orange.ocara.ui.viewmodel.EditCommentViewModel;
import com.orange.ocara.ui.viewmodel.EditCommentViewModelFactory;
import com.orange.ocara.ui.viewmodel.OcaraViewModelFactory;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.activity_edit_comment_text)
public class EditCommentTextFragment extends EditCommentFragment {

    @ViewById(R.id.editCommentLayout)
    LinearLayout editCommentLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.activity_edit_comment_text, container, false);
        commentId = EditCommentTextFragment_Args.fromBundle(getArguments()).getCommentId();
        commentType = CommentEntity.getTypeFromInt(EditCommentTextFragment_Args.fromBundle(getArguments()).getCommentType());
        attachmentDirectory = EditCommentTextFragment_Args.fromBundle(getArguments()).getAttachmentDirectory();
        imageName = EditCommentTextFragment_Args.fromBundle(getArguments()).getImageName();
        name = EditCommentTextFragment_Args.fromBundle(getArguments()).getName();
        auditId = EditCommentTextFragment_Args.fromBundle(getArguments()).getAuditId();
        title = EditCommentTextFragment_Args.fromBundle(getArguments()).getTitle();
        subTitle = EditCommentTextFragment_Args.fromBundle(getArguments()).getSubTitle();

        editCommentViewModel = new ViewModelProvider(this, new OcaraViewModelFactory(getActivity()))
                .get(EditCommentViewModel.class);

        rootview.findViewById(R.id.editCommentLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard();
            }
        });
        loadComment();
//        setUpComment();

        return rootview;
    }



    @Override
    protected void createAttachment() {
        // no attachment for text comment
    }


    @Override
    protected void handleAttachment() {
        // no attachment for text comment
    }

}
