package com.orange.ocara.ui.tools;

import android.view.View;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import androidx.lifecycle.MutableLiveData;

import com.orange.ocara.data.cache.model.AuditEntity;
import com.orange.ocara.ui.view.EditAuditContract;
import com.orange.ocara.ui.view.Switch;

import java.util.Locale;

public class BindingAdapters {

    @BindingAdapter("date")
    public static void setName(TextView textView, String unformattedDate) {

        Locale locale = Locale.getDefault();
        String date = RuleSetUtils.formatRulesetDate(unformattedDate, locale);
        textView.setText(date);
    }

    @BindingAdapter("android:visibility")
    public static void setVisibility(View view, Boolean value) {
        view.setVisibility(value ? View.VISIBLE : View.GONE);
    }

//    @BindingAdapter("expertiseLevel")
//    public static void setExpertiseLevel(Switch view, AuditEntity.Level level) {
//        boolean isExpertSelected = view.isChecked();
//        boolean isNoviceSelected = !view.isChecked();
//
//        if (isExpertSelected && level.getValue() == AuditEntity.Level.BEGINNER)
//        // expert selected and new one is novice
//        {
//            view.setChecked(false);
//        }
//
//        if (isNoviceSelected && level.getValue() == AuditEntity.Level.EXPERT)
//        //  novice selected and new one is expert
//        {
//            view.setChecked(true);
//        }
//    }
//
//    @InverseBindingAdapter(attribute = "expertiseLevel", event = "expertiseLevelAttrChanged")
//    public static boolean getLevel(Switch view) {
//        return view.isChecked();
//    }
}
