/*
 * Copyright (c) 2019 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 * distribute, sublicense, create a derivative work, and/or sell copies of the
 * Software in any work that is designed, intended, or marketed for pedagogical or
 * instructional purposes related to programming, coding, application development,
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works,
 * or sale is expressly withheld.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.orange.ocara.ui.viewmodel;

import android.content.Context;
import android.net.Uri;

import androidx.lifecycle.MutableLiveData;

import com.orange.ocara.business.service.RuleSetService;
import com.orange.ocara.business.service.impl.RuleSetServiceImpl_;
import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.db.ModelManagerImpl_;
import com.orange.ocara.data.cache.model.AuditEntity;
import com.orange.ocara.data.cache.model.AuditObjectEntity;
import com.orange.ocara.data.net.model.EquipmentEntity;
import com.orange.ocara.data.net.model.IllustrationEntity;
import com.orange.ocara.data.net.model.ProfileTypeEntity;
import com.orange.ocara.data.net.model.RulesetEntity;
import com.orange.ocara.tools.FileUtils;
import com.orange.ocara.ui.tools.RefreshStrategy;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.UiThread;

import java.io.File;
import java.util.List;
import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;


public class AuditReportViewModel extends OcaraViewModelWithEntity {

    private RulesetEntity mRuleSet;
    private RuleSetService mRuleSetService;
    private AuditEntity audit;
    ModelManager modelManager;

    public MutableLiveData<AuditEntity> auditLiveData;
    public MutableLiveData<RulesetEntity> ruleSetLiveData;
    Long auditId;

    public AuditReportViewModel(Context context, Long auditId) {
        super(context, auditId);
        auditLiveData = new MutableLiveData<>();
        ruleSetLiveData = new MutableLiveData<>();
        mRuleSetService = RuleSetServiceImpl_.getInstance_(context);
        this.auditId = auditId;
        modelManager = ModelManagerImpl_.getInstance_(context);
//        getAudit();
    }

    public void getAudit() {

        Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter emitter) throws Exception {
                audit = ModelManagerImpl_.getInstance_(context).getAudit(auditId);
                mRuleSet = audit.getRuleSet();

                auditLiveData.postValue(audit);
                ruleSetLiveData.postValue(mRuleSet);

            }
        }).subscribe(new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }
        });
    }

    public Completable lockAudit() {

        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter emitter) throws Exception {
                audit.setStatus(AuditEntity.Status.TERMINATED);
                modelManager.updateAudit(audit);
                auditLiveData.postValue(audit);
                emitter.onComplete();

            }
        });
    }


    //@Background
    public Completable copyFile(File file, Uri uri) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter emitter) throws Exception {
                try {
                    FileUtils.copyFile(file, context.getContentResolver().openOutputStream(uri, "w"));
//                    fileCopied(file, uri);
                    emitter.onComplete();
                } catch (Exception e) {
                    Timber.e(e, "Failed to copy file");
                }
            }
        });
    }

    public Map<String, ProfileTypeEntity> getProfilType() {
        return mRuleSetService.getProfilTypeFormRuleSet(mRuleSet);
    }
}
