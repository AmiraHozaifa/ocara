/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.orange.ocara.R;
import com.orange.ocara.data.cache.model.AuditObjectEntity;
import com.orange.ocara.data.cache.model.QuestionAnswerEntity;
import com.orange.ocara.data.cache.model.ResponseModel;
import com.orange.ocara.ui.adapter.QuestionAndRulesAnswersAdapter;
import com.orange.ocara.ui.tools.RefreshStrategy;
import com.orange.ocara.ui.viewmodel.AuditObjectsNoviceViewModel;
import com.orange.ocara.ui.viewmodel.AuditObjectsViewModelFactory;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OptionsMenu;

import java.util.List;

import javax.annotation.Nullable;

import timber.log.Timber;

/**
 * Activity dedicated to managing an {@link AuditObjectEntity}
 */

@EFragment(R.layout.fragment_audit_object)
@OptionsMenu(R.menu.audit_objects)
public class AuditObjectsNoviceFragment extends AuditObjectsFragment
        implements QuestionAndRulesAnswersAdapter.QuestionAndRulesListener {

    private static final RefreshStrategy STRATEGY = RefreshStrategy.auditObjectRefreshStrategy();
    private static final RefreshStrategy RULE_REFRESH_STRATEGY = RefreshStrategy.ruleAnswerRefreshStrategy();
    private static final RefreshStrategy QUESTION_REFRESH_STRATEGY = RefreshStrategy.questionAnswerRefreshStrategy();

//    @Bean
//    QuestionAndRulesAnswersAdapter questionAndRulesAnswersAdapter;
//
//    @ViewById(R.id.buttonbar_left_button)
//    Button leftButton;
//
//    @ViewById(R.id.buttonbar_right_button)
//    Button rightButton;
//
//    @ViewById(R.id.rules_listview)
//    ExpandableListView rulesListView;

    //    @Extra
//    boolean editMode = false;
//
//    //    @Extra
//    long[] selectedObjects;

    AuditObjectsNoviceViewModel viewModel;

    Observer<List<QuestionAnswerEntity>> questionListObserver = new Observer<List<QuestionAnswerEntity>>() {
        @Override
        public void onChanged(List<QuestionAnswerEntity> QuestionList) {
            refreshQuestionList(QuestionList);
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = super.onCreateView(inflater, container, savedInstanceState);
        editMode = AuditObjectsNoviceFragment_Args.fromBundle(getArguments()).getEditMode();
        auditId = AuditObjectsNoviceFragment_Args.fromBundle(getArguments()).getAuditId();
        selectedObjects = AuditObjectsNoviceFragment_Args.fromBundle(getArguments()).getSelectedObjects();


        viewModel = new ViewModelProvider(this, new AuditObjectsViewModelFactory(getActivity(), selectedObjects))
                .get(AuditObjectsNoviceViewModel.class);
        viewModel.questionListLiveData.observe(getViewLifecycleOwner(), questionListObserver);

//        Toast.makeText(getActivity(), "IN FRAG", Toast.LENGTH_LONG).show();

        return v;

    }

    @Click(R.id.buttonbar_left_button)
    void onLeftButton() {

        Timber.d("Clicking on the left button...");
        if (viewModel.isFirstItemSelected()) {
            onBack();
        } else {
            moveToQuestion(viewModel.getCurrentQuestionIdx() - 1);
        }
    }

    @Click(R.id.buttonbar_right_button)
    void onRightButton() {

        Timber.d("Clicking on the right button...");
        if (viewModel.isLastItemSelected()) {
            Timber.d("Checking failed...");
            finish();
            return; // blindage
        }

        Timber.d("Checking succeeded...");


        final AuditObjectEntity currentAuditObject = viewModel.getCurrentAuditObject();
        moveToQuestion(viewModel.getCurrentQuestionIdx() + 1);

        if (viewModel.getCurrentQuestionIdx() < viewModel.getNumberOfQuestions()
                && viewModel.getCurrentAuditObject() != currentAuditObject) {
            // object has changed ==> display a progress status for user
            showAuditObjectProgress(currentAuditObject, false);
            return;
        }

    }


    @Override
    protected String getTitle() {
        return getString(R.string.audit_object_novice_mode_title, getAuditObjectName(viewModel.getCurrentAuditObject()), viewModel.getCurrentQuestionIdx() + 1, viewModel.getNumberOfQuestions());
    }


    void moveToQuestion(final int newCurrentSelectedQuestion) {

        final AuditObjectEntity currentAuditObject = viewModel.getCurrentAuditObject();

        // check that audioObject can be saved

        if (currentAuditObject != null && currentAuditObject.getResponse().equals(ResponseModel.NOT_APPLICABLE)) {

            Timber.d("Could not move to next question %d", newCurrentSelectedQuestion);

            // cannot save an object with this status.
            // User MUST set at least one piece of information

            AlertDialog errorDialog = new AlertDialog.Builder(getContext())
                    .setTitle(R.string.audit_object_badglobaleresponse_title) // title
                    .setMessage(R.string.audit_object_badglobaleresponse_message) // message
                    .setPositiveButton(R.string.action_ok, null)
                    .create();

            errorDialog.show();
            viewModel.resetAndSelectAuditObject(currentAuditObject);
        } else {
//            currentSelectedQuestion = newCurrentSelectedQuestion;

            // save audit object
            Timber.d("Save audit and move to next question %d", newCurrentSelectedQuestion);
//            saveAuditObject(currentAuditObject);
//            viewModel.selectQuestion(newCurrentSelectedQuestion);

            if (viewModel.getCurrentQuestionIdx() < viewModel.getNumberOfQuestions()) {
                showAuditObjectProgress(currentAuditObject, false);
                viewModel.selectQuestion(newCurrentSelectedQuestion);
            } else {
                // show progress and terminate
                showAuditObjectProgress(currentAuditObject, true);
            }
        }

    }

    @Override
    public AuditObjectsNoviceViewModel getViewModel() {
        return viewModel;
    }
}
