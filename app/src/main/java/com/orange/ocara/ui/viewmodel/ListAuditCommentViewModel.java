package com.orange.ocara.ui.viewmodel;

import android.content.Context;
import android.widget.Filter;

import androidx.lifecycle.MutableLiveData;

import com.activeandroid.Model;
import com.orange.ocara.business.BizConfig_;
import com.orange.ocara.business.binding.ErrorBundle;
import com.orange.ocara.business.interactor.RedoAuditTask;
import com.orange.ocara.business.interactor.UseCase;
import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.db.ModelManagerImpl_;
import com.orange.ocara.data.cache.model.AuditEntity;
import com.orange.ocara.data.cache.model.CommentEntity;
import com.orange.ocara.data.cache.model.SortCriteria;
import com.orange.ocara.ui.tools.RefreshStrategy;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.orange.ocara.tools.ListUtils.emptyList;

public class ListAuditCommentViewModel extends ListCommentViewModel {

    protected AuditEntity entity;
//    long auditId;

    public ListAuditCommentViewModel(Context context, Long auditId) {
        super(context, auditId);
        this.entity = modelManager.getAudit(auditId);
    }
    public void loadComments() {
        getCommentsBg();
    }


    private void getCommentsBg() {
        Single.create(new SingleOnSubscribe<List<CommentEntity>>() {
            @Override
            public void subscribe(SingleEmitter<List<CommentEntity>> emitter) throws Exception {
                List<CommentEntity> comment = getCommentsSequential();
                emitter.onSuccess(comment);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(commentEntities -> {
            commentsLiveData.setValue(commentEntities);

        });
    }

    private List<CommentEntity> getCommentsSequential() {
        modelManager.refresh(entity, RefreshStrategy.builder().commentsNeeded(true).build());
        List<CommentEntity> comments = entity.getComments();
        return comments;
//        commentsLiveData.setValue(comments);
    }


    @Override
    public void deleteComment(CommentEntity comment) {
        Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter emitter) throws Exception {
                modelManager.deleteComment(comment);
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(() -> {
            getCommentsBg();
        });
    }

    @Override
    public void deleteAllComments() {
        Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter emitter) throws Exception {
                modelManager.deleteAllComments(entity.getComments());
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(() -> {
            getCommentsBg();
        });
    }

    @Override
    public void updateComment(Long commentId) {
        Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter emitter) throws Exception {
                CommentEntity comment = Model.load(CommentEntity.class, commentId);
                entity.attachComment(comment);
                comment.save();
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(() -> {
            getCommentsBg();
        });
    }

    @Override
    public String getEntityName() {
        if (entity != null) return entity.getName();
        return null;
    }

    @Override
    public Long getAuditId() {
        if (entity != null) return entity.getId();
        return null;
    }

}

