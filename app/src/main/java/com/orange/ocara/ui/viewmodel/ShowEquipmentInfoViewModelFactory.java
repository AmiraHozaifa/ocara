package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class ShowEquipmentInfoViewModelFactory implements ViewModelProvider.Factory {

    private Long auditId;
    private String objectDescriptionId;
    private Context context;

    public ShowEquipmentInfoViewModelFactory(Context context, Long auditId, String objectDescriptionId) {
        this.context = context;
        this.auditId = auditId;
        this.objectDescriptionId = objectDescriptionId;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new ShowEquipmentInfoViewModel(context, auditId, objectDescriptionId);
    }
}
