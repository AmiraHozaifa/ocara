/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.orange.ocara.R;
import com.orange.ocara.business.BizConfig;
import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.db.ModelManagerImpl;
import com.orange.ocara.databinding.FragmentCreateSiteBinding;
import com.orange.ocara.ui.viewmodel.CreateSiteViewModel;
import com.orange.ocara.ui.viewmodel.CreateSiteViewModelFactory;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;

import static androidx.navigation.Navigation.findNavController;

@EFragment(R.layout.fragment_create_site)
public class CreateSiteFragment extends BaseFragment {

        FragmentCreateSiteBinding binding;
    CreateSiteViewModel createSiteViewModel;


    @Bean(BizConfig.class)
    BizConfig bizConfig;


    @Bean(ModelManagerImpl.class)
    ModelManager modelManager;

    @AfterInject
    void setUp() {
        System.err.println("CreateSiteFragment AFTER INJ");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        System.err.println("CreateSiteFragment On cr");

        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_create_site, container, false);
        View view = binding.getRoot();
//
        //binding.setLifecycleOwner(this);

        createSiteViewModel = new ViewModelProvider(this, new CreateSiteViewModelFactory(modelManager)).get(CreateSiteViewModel.class);


        binding.setCreateSiteViewModel(createSiteViewModel);

        binding.siteNoimmo.addTextChangedListener(new CheckTextWatcher());
        binding.siteName.addTextChangedListener(new CheckTextWatcher());
        binding.siteStreet.addTextChangedListener(new CheckTextWatcher());
        binding.sitePostalCode.addTextChangedListener(new CheckTextWatcher());
        binding.siteCity.addTextChangedListener(new CheckTextWatcher());

        binding.createSiteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreateSiteButtonClicked(v);
            }
        });
        binding.createSiteLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard();
            }
        });
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
//        setTitle(R.string.create_site_create_site_title);
    }


    protected void updateCreateSiteButton() {
        if (createSiteViewModel.checkSiteFields()) {
            binding.createSiteButton.setEnabled(true);
        } else {
            binding.createSiteButton.setEnabled(false);
        }
    }


//    @Override
//    protected boolean isChildActivity() {
//        return true;
//    }

    void onCreateSiteButtonClicked(View v) {
        Long siteId = createSiteViewModel.validateSite();
        if (siteId != null) {

            findNavController(v).getPreviousBackStackEntry().getSavedStateHandle().set("siteId", siteId);
            findNavController(v).popBackStack();

//            Intent intent = new Intent();
//            intent.putExtra("siteId", siteId);
//            setResult(Activity.RESULT_OK, intent);
//            finish();
        }
    }


    private class CheckTextWatcher implements TextWatcher {

        @Override
        public void afterTextChanged(Editable s) {
            // do nothing yet
            updateCreateSiteButton();
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // do nothing yet
        }

        @Override
        public void onTextChanged(CharSequence userInput, int start, int before, int count) {

        }
    }

}
