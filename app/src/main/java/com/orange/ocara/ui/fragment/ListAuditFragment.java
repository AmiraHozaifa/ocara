/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ShareCompat;
import androidx.core.content.FileProvider;
import androidx.core.view.MenuItemCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.orange.ocara.BuildConfig;
import com.orange.ocara.R;
import com.orange.ocara.business.BizConfig;
import com.orange.ocara.business.model.AuditModel;
import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.db.ModelManagerImpl;
import com.orange.ocara.data.cache.model.AuditEntity;
import com.orange.ocara.data.cache.model.SortCriteria;
import com.orange.ocara.tools.FileUtils;

import com.orange.ocara.ui.activity.ResultAuditActivity_;
import com.orange.ocara.ui.adapter.AuditListAdapter;
import com.orange.ocara.ui.adapter.AuditRVAdapter;
import com.orange.ocara.ui.contract.ListAuditContract;
import com.orange.ocara.ui.dialog.OcaraDialogBuilder;
import com.orange.ocara.ui.intent.export.AuditExportService;
import com.orange.ocara.ui.intent.export.AuditExportService_;
import com.orange.ocara.ui.presenter.ListAuditPresenter;
import com.orange.ocara.ui.routing.DestinationController;
import com.orange.ocara.ui.routing.Navigation;
import com.orange.ocara.ui.viewmodel.ListAuditViewModel;
import com.orange.ocara.ui.viewmodel.OcaraViewModelFactory;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.Receiver;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static androidx.navigation.Navigation.findNavController;
/**
 * View dedicated to the display of a list of audits
 */

/**
 * a view dedicated to displaying a list of audits
 */
@EFragment(R.layout.fragment_list_audit)
@OptionsMenu(R.menu.list_all_audits)
public class ListAuditFragment extends BaseFragment
        implements MenuItemCompat.OnActionExpandListener, SearchView.OnQueryTextListener,
        AuditRVAdapter.AuditListAdapterListener, ListAuditContract.ListAuditView {

    private static final int ACTION_CREATE_NEW_DOCUMENT = 1;

    private static final int EXPORT_ACTION_SHARE = 0;
    private static final int EXPORT_ACTION_SAVE = 1;

    private static final int DELAY_IN_MILLISECONDS = 200;

//    @Bean(ModelManagerImpl.class)
//    ModelManager modelManager;

    @Bean(DestinationController.class)
    Navigation navController;

    @ViewById(R.id.audit_list)
    RecyclerView auditList;

    @ViewById(R.id.audit_list_empty)
    View emptyAuditList;

    @ViewById(R.id.audit_list_header)
    TextView auditListHeader;


//    @Bean(BizConfig.class)
//    BizConfig bizConfig;


    private MenuItem searchMenuItem = null;
    private SearchView searchView = null;
    private boolean searchActive = false;
    //    private long selectedAuditId = 0;
    private int selectedIndex = -1;
    private AuditRVAdapter auditListAdapter;
    private int exportAction = -1;
    private File exportFile;
    ListAuditViewModel listAuditViewModel;

//    private ListAuditContract.ListAuditUserActionsListener actionsListener;

//    public long getSelectedAuditId() {
//        return selectedAuditId;
//    }

    @AfterViews
    void setUpAuditList() {
//        View auditListHeader = getLayoutInflater().inflate(R.layout.list_audit_header, null, false);
        auditListAdapter = new AuditRVAdapter(getActivity(), this);
//        auditList.setEmptyView(emptyAuditList);
//        auditList.addHeaderView(auditListHeader, null, false);

        auditList.setLayoutManager(new LinearLayoutManager(getActivity()));

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(auditList.getContext(),
                DividerItemDecoration.VERTICAL);
        auditList.addItemDecoration(dividerItemDecoration);
        auditList.setAdapter(auditListAdapter);

//        actionsListener = new ListAuditPresenter(this, bizConfig.redoAuditTask());
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    Observer<List<AuditEntity>> auditListUpdateObserver = new Observer<List<AuditEntity>>() {
        @Override
        public void onChanged(List<AuditEntity> auditArrayList) {
            if (auditArrayList.isEmpty()) {
                auditListHeader.setVisibility(View.GONE);
                auditList.setVisibility(View.GONE);
                emptyAuditList.setVisibility(View.VISIBLE);
            } else {
                auditListHeader.setVisibility(View.VISIBLE);
                auditList.setVisibility(View.VISIBLE);
                emptyAuditList.setVisibility(View.GONE);
            }
            auditListAdapter.update(auditArrayList);
        }
    };


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        listAuditViewModel = new ViewModelProvider(this, new OcaraViewModelFactory(getActivity())).get(ListAuditViewModel.class);
        listAuditViewModel.auditsLiveData.observe(getViewLifecycleOwner(), auditListUpdateObserver);
//        Toast.makeText(getActivity(), "IN FRAG", Toast.LENGTH_LONG).show();

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        listAuditViewModel.loadAudits();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        searchMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);

        if (searchView != null) {
            searchView.setIconifiedByDefault(true);
            searchView.setOnQueryTextListener(this);
        }

        MenuItemCompat.setOnActionExpandListener(searchMenuItem, this);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        switch (listAuditViewModel.getSortCriteria().getType()) {
            case SITE:
                menu.findItem(R.id.sort_by_site).setChecked(true);
                break;

            case DATE:
                menu.findItem(R.id.sort_by_date).setChecked(true);
                break;
            case STATUS:
            default:
                menu.findItem(R.id.sort_by_status).setChecked(true);
                break;
        }

        menu.findItem(R.id.sort).setIcon(listAuditViewModel.getSortCriteria().isAscending() ? R.drawable.ic_sort_asc : R.drawable.ic_sort_desc);

        super.onPrepareOptionsMenu(menu);
    }

    /**
     * Callback triggered when an item is clicked in the sub-menu
     *
     * @param item     a {@link MenuItem}
     * @param position an index
     * @return true, if action matches an element of the menu. false, if not.
     */
    @Override
    public boolean onAuditSubMenuItemClick(MenuItem item, final int position) {
        final AuditEntity audit = auditListAdapter.getItem(position);
        auditListAdapter.setSelectedAuditId(audit.getId());
//        Timber.d("Message=Selecting an item for audit;AuditId=%d;", selectedAuditId);

        int i = item.getItemId();
        if (i == R.id.action_remove) {
            askToDeleteAudit(audit);
            return true;
        } else if (i == R.id.action_complete_record) {
            askToCompleteAudit(audit);
            return true;
        } else if (i == R.id.action_edit_audit) {
//            navController.navigateToAuditEditView(getActivity(), auditListAdapter.getSelectedAuditId(), 0);
            Bundle bundle = new Bundle();
            bundle.putLong("auditId", auditListAdapter.getSelectedAuditId());
            NavHostFragment.findNavController(this).navigate(R.id.action_edit_audit, bundle);
            return true;
        } else if (i == R.id.action_retest) {
            askToRetest(audit);
            return true;
        }

        return false;
    }


    private void askToDeleteAudit(final AuditEntity audit) {
        AlertDialog confirmDialog = new OcaraDialogBuilder(getActivity())
                .setTitle(R.string.audit_list_delete_audit_title) // title
                .setMessage(getString(R.string.audit_list_delete_audit_message, audit.getName())) // message
                .setPositiveButton(R.string.action_remove, (dialog, which) -> deleteAudit(audit))
                .setNegativeButton(R.string.action_cancel, null)
                .create();

        confirmDialog.show();
    }

    private void askToCompleteAudit(final AuditEntity audit) {
        AlertDialog confirmDialog = new OcaraDialogBuilder(getActivity())
                .setTitle(R.string.audit_list_complete_audit_title) // title
                .setMessage(R.string.audit_list_complete_audit_message) // message
                .setPositiveButton(R.string.action_complete, (dialog, which) ->
//                        SetupAuditPathActivity_
//                        .intent(ListAuditFragment.this)
//                        .fromListAudit(true)
//                        .auditId(audit.getId())
//                        .start()
                                startSetupPath(audit.getId())
                )
                .setNegativeButton(R.string.action_cancel, null)
                .create();

        confirmDialog.show();
    }


    private void askToRetest(final AuditEntity audit) {

        Timber.d("Message=Requesting the redo of an audit;AuditId=%d;AuditName=%s;", audit.getId(), audit.getName());

        AlertDialog confirmDialog = new OcaraDialogBuilder(getActivity())
                .setTitle(R.string.audit_list_retest_audit_title) // title
                .setMessage(getString(R.string.audit_list_retest_audit_message, audit.getName())) // message
                .setPositiveButton(R.string.action_retest, (dialog, which) -> createNewAudit(audit))
                .setNegativeButton(R.string.action_cancel, null)
                .create();

        confirmDialog.show();
    }

    @OptionsItem(R.id.add_audit)
    void onCreateAuditMenuItem() {
//        navController.navigateToHome(this);
//        NavHostFragment.findNavController(this).navigate(R.id.action_create_audit);
        finish();
    }

    @OptionsItem(R.id.sort_by_status)
    void onSortByStatusMenuItem(MenuItem menuItem) {
        onSortMenuItem(menuItem, SortCriteria.Type.STATUS);
    }

    @OptionsItem(R.id.sort_by_site)
    void onSortBySiteMenuItem(MenuItem menuItem) {
        onSortMenuItem(menuItem, SortCriteria.Type.SITE);
    }

    @OptionsItem(R.id.sort_by_date)
    void onSortByDateMenuItem(MenuItem menuItem) {
        onSortMenuItem(menuItem, SortCriteria.Type.DATE);
    }

    void onSortMenuItem(MenuItem menuItem, SortCriteria.Type sortCriteriaType) {

        SortCriteria sortCriteria;

        // If already checked toggle current sortCriteria
        if (menuItem.isChecked()) {
            sortCriteria = listAuditViewModel.getSortCriteria();
            sortCriteria.toggleOrder();
        } else {
            sortCriteria = sortCriteriaType.build();
        }

        listAuditViewModel.sortAudits(sortCriteria);
        getActivity().supportInvalidateOptionsMenu();
    }

    @Override
    public void onAuditListItemClicked(AuditEntity audit) {
//        auditListAdapter.setSelectedAuditId(audit.getId());
//        selectedAuditId = audit.getId();
        if (AuditEntity.Status.TERMINATED.equals(audit.getStatus())) {
//            ResultAuditActivity_.intent(this).auditId(audit.getId()).start();
            Bundle bundle = new Bundle();
            bundle.putLong("auditId", audit.getId());
            NavHostFragment.findNavController(getParentFragment()).navigate(R.id.action_audit_report, bundle);
        } else {
//            SetupAuditPathActivity_.intent(this).fromListAudit(true).auditId(audit.getId()).start();
            startSetupPath(audit.getId());
        }
    }


    //    @Background
    void deleteAudit(AuditEntity audit) {
        listAuditViewModel.deleteAudit(audit);
    }

    //
//    @UiThread(propagation = UiThread.Propagation.REUSE)
//    void auditDeleted() {
//        auditListAdapter.refresh();
//    }
//
//    @Background
    void createNewAudit(AuditEntity audit) {
        listAuditViewModel.createNewAudit(audit).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new DisposableSingleObserver<AuditEntity>() {
            @Override
            public void onSuccess(AuditEntity audit) {
                showSetupPath(audit);
                this.dispose();
            }

            @Override
            public void onError(Throwable e) {
                this.dispose();
            }
        });
    }

    @Override
    @UiThread(propagation = UiThread.Propagation.REUSE)
    public void showSetupPath(AuditModel audit) {
//        selectedAuditId = audit.getId();
        auditListAdapter.setSelectedAuditId(audit.getId());
//        SetupAuditPathActivity_
//                .intent(this)
//                .fromListAudit(true)
//                .auditId(audit.getId())
//                .start();
        startSetupPath(audit.getId());
    }

    /**
     * Converts an audit into a document
     *
     * @param audit an identifier for an {@link AuditEntity}
     */
    @Override
    public void onExportAuditToWord(final AuditEntity audit) {
        exportAction = -1;
        exportFile = null;
//        AuditEntity audit = modelManager.getAudit(auditId);

        int message;
        if (audit.getStatus().equals(AuditEntity.Status.TERMINATED)) {
            message = R.string.export_docx_message_closed_audit;
        } else {
            message = R.string.export_docx_message;
        }

        AlertDialog dialog = new OcaraDialogBuilder(getActivity())
                .setTitle(R.string.export_docx_title) // title
                .setMessage(message) // message
                .setNeutralButton(R.string.action_cancel, null)
                .setNegativeButton(R.string.action_share, (dialog1, which) -> {
                    exportAction = EXPORT_ACTION_SHARE;

                    setLoading(true);
                    AuditExportService_
                            .intent(getActivity())
                            .toDocx(audit.getId())
                            .start();
                })
                .setPositiveButton(R.string.action_save, (dialog12, which) -> {
                    exportAction = EXPORT_ACTION_SAVE;

                    setLoading(true);
                    AuditExportService_
                            .intent(getActivity())
                            .toDocx(audit.getId())
                            .start();
                })

                .create();

        dialog.show();
    }

    @Receiver(actions = AuditExportService.EXPORT_SUCCESS, local = true, registerAt = Receiver.RegisterAt.OnResumeOnPause)
    @UiThread(propagation = UiThread.Propagation.REUSE)
    void onExportAuditDone(@Receiver.Extra String path) {
        setLoading(false);

        exportFile = new File(path);

        switch (exportAction) {
            case EXPORT_ACTION_SHARE: {

                Uri uri;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Timber.i("Message=Configuring intent for version >= VERSION_CODES.N");
                    uri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".provider", exportFile);
                } else {
                    Timber.i("Message=Configuring intent for version < VERSION_CODES.N");
                    uri = Uri.fromFile(exportFile);
                }

                // create an intent, so the user can choose which application he/she wants to use to share this file
                final Intent intent = ShareCompat
                        .IntentBuilder
                        .from(getActivity())
                        .setType(MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(path)))
                        .setStream(uri)
                        .setChooserTitle(R.string.list_audit_export_action)
                        .createChooserIntent()
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
                        .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                startActivity(intent);
                break;
            }

            case EXPORT_ACTION_SAVE: {
                createNewDocument(path);
                break;
            }
            default:
                throw new IllegalStateException("Should not get here");
        }
    }

    @Receiver(actions = AuditExportService.EXPORT_FAILED, local = true, registerAt = Receiver.RegisterAt.OnResumeOnPause)
    @UiThread(propagation = UiThread.Propagation.REUSE)
    void exportAuditDocxFailed() {
        setLoading(false);
    }

    private void createNewDocument(String path) {
        Intent intent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        } else {
            intent = new Intent();
        }
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType(MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(path)));
        startActivityForResult(intent, ACTION_CREATE_NEW_DOCUMENT);
    }

    @OnActivityResult(ACTION_CREATE_NEW_DOCUMENT)
    void onCreateDocument(int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            copyFile(exportFile, uri);
        }
    }

    @Background
    void copyFile(File file, Uri uri) {
        try {
            FileUtils.copyFile(file, getActivity().getContentResolver().openOutputStream(uri, "w"));
            fileSaved(uri);
        } catch (Exception e) {
            Timber.e(e, "Failed to copy file");
        }
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    void fileSaved(Uri uri) {
        Toast.makeText(getActivity(), "File  saved to " + uri.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
        searchActive = true;
        return true;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        searchActive = false;
        listAuditViewModel.searchAudits("");
        return true;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        Timber.d("text change = %s active=%b", query, searchActive);

        if (searchActive) {
            listAuditViewModel.searchAudits(query);
        }

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Timber.d("text submit = %s", query);
        searchView.setQuery(query, false);
        hideSoftKeyboard();
        return true;
    }

    @Override
    public void refreshAuditList(final List<AuditEntity> audits) {
        Handler handler = new Handler();

        handler.postDelayed(() -> {
            Timber.v("ListAuditActivity nombre " + auditListAdapter.getCount());
            Timber.v("ListAuditActivity selectedAuditId " + auditListAdapter.getSelectedAuditId());

            if (auditListAdapter.getSelectedAuditId() != 0) {

                for (int i = 0; i < auditListAdapter.getCount(); i++) {
                    AuditEntity audit = auditListAdapter.getItem(i);
                    Timber.v("ListAuditActivity auditId " + audit.getId());
                    if (audit.getId() == auditListAdapter.getSelectedAuditId()) {
                        selectedIndex = i;
                        auditList.post(() -> auditList.smoothScrollToPosition(selectedIndex));
                        break;
                    }
                }
            }

        }, DELAY_IN_MILLISECONDS);
    }


    private void startSetupPath(Long auditId) {
        Bundle bundle = new Bundle();
        bundle.putLong("auditId", auditId);
        bundle.putBoolean("fromListAudit", true);
        NavHostFragment.findNavController(this).navigate(R.id.action_setup_path, bundle);
    }
}
