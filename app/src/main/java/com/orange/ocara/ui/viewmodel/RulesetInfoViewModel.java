package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.orange.ocara.business.BizConfig;
import com.orange.ocara.business.BizConfig_;
import com.orange.ocara.business.binding.BizError;
import com.orange.ocara.business.binding.ErrorBundle;
import com.orange.ocara.business.interactor.CheckRulesetIsUpgradableTask;
import com.orange.ocara.business.interactor.CheckRulesetRulesExistenceTask;
import com.orange.ocara.business.interactor.LoadRulesetTask;
import com.orange.ocara.business.interactor.UpgradeRulesetTask;
import com.orange.ocara.business.interactor.UseCase;
import com.orange.ocara.business.model.RulesetModel;
import com.orange.ocara.business.model.VersionableModel;


import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import timber.log.Timber;

public class RulesetInfoViewModel extends ViewModel {

    public RulesetModel ruleset;

    //    public boolean rulesetDetailsAvailable;
//    public boolean rulesetDetailsDownloadable;
    public MutableLiveData<Boolean> rulesetDetailsAvailable;
    public MutableLiveData<Boolean> rulesetDetailsDownloadable;

    boolean rulesetUpgradable;


    public RulesetModel getRuleset() {
        return ruleset;
    }

    public RulesetInfoViewModel(Context context, RulesetModel ruleset, boolean rulesetUpgradable) {
        this.ruleset = ruleset;
        this.rulesetUpgradable = rulesetUpgradable;

        rulesetDetailsAvailable = new MutableLiveData<Boolean>();
        rulesetDetailsDownloadable = new MutableLiveData<Boolean>();
        rulesetDetailsAvailable.postValue(false);
        rulesetDetailsDownloadable.postValue(false);
        BizConfig bizConfig = BizConfig_.getInstance_(context);

        if (rulesetUpgradable) {
            Timber.i("ConfigMessage=Ruleset is readable and upgradable");

            upgradeRulesetTask = bizConfig.upgradeRulesetTask();
            checkRulesetRulesExistenceTask = bizConfig.checkRulesetRulesExistenceTask();
            checkRulesetIsUpgradableTask = bizConfig.checkRulesetIsUpgradableTask();
            loadRulesetTask = bizConfig.loadRulesetTask();
        } else {
            Timber.i("ConfigMessage=Ruleset is only readable");
            upgradeRulesetTask = alwaysError();
            checkRulesetRulesExistenceTask = bizConfig.checkRulesetRulesExistenceTask();
            checkRulesetIsUpgradableTask = alwaysFalse();
            loadRulesetTask = bizConfig.loadRulesetTask();
        }
    }


    private final UseCase<UpgradeRulesetTask.UpgradeRulesetRequest, UpgradeRulesetTask.UpgradeRulesetResponse> upgradeRulesetTask;

    private final UseCase<CheckRulesetRulesExistenceTask.CheckRulesetRulesExistenceRequest, CheckRulesetRulesExistenceTask.CheckRulesetRulesExistenceResponse> checkRulesetRulesExistenceTask;

    private final UseCase<CheckRulesetIsUpgradableTask.CheckRulesetIsUpgradableRequest, CheckRulesetIsUpgradableTask.CheckRulesetIsUpgradableResponse> checkRulesetIsUpgradableTask;

    public final UseCase<LoadRulesetTask.LoadRulesetRequest, LoadRulesetTask.LoadRulesetResponse> loadRulesetTask;

    public Observable upgradeRuleset(final VersionableModel ruleset) {
        Timber.i(
                "PresenterMessage=Start upgrading ruleset;RulesetReference=%s;RulesetVersion=%s",
                ruleset.getReference(), ruleset.getVersion());

        Observable upgradeRulesetObservable = Observable.create(new ObservableOnSubscribe<RulesetModel>() {
            @Override
            public void subscribe(ObservableEmitter emitter) {

                upgradeRulesetTask.executeUseCase(
                        new UpgradeRulesetTask.UpgradeRulesetRequest(ruleset),
                        new UseCase.UseCaseCallback<UpgradeRulesetTask.UpgradeRulesetResponse>() {
                            @Override
                            public void onComplete(UpgradeRulesetTask.UpgradeRulesetResponse response) {
                                Timber.d("PresenterMessage=Item upgraded successfully;PresenterResponse=%s", response.getCode());
                                emitter.onNext(response.getRuleset());
                                emitter.onComplete();
                            }

                            @Override
                            public void onError(ErrorBundle errors) {
                                Timber.e(errors.getCause(), "PresenterMessage=Item upgrading failed;PresenterError=%s", errors.getMessage());
                                emitter.onError(null);
                            }
                        }
                );
            }
        });

        return upgradeRulesetObservable;
    }


//    public Observable checkRulesetExistance(final VersionableModel ruleset) {
//        Observable checkRulesetIsValidObservable = Observable.create(new ObservableOnSubscribe<Boolean>() {
//            @Override
//            public void subscribe(ObservableEmitter emitter) {
//
//
//                checkRulesetRulesExistenceTask.executeUseCase(
//                        new CheckRulesetRulesExistenceTask.CheckRulesetRulesExistenceRequest(ruleset),
//                        new UseCase.UseCaseCallback<CheckRulesetRulesExistenceTask.CheckRulesetRulesExistenceResponse>() {
//                            @Override
//                            public void onComplete(CheckRulesetRulesExistenceTask.CheckRulesetRulesExistenceResponse response) {
//                                Timber.d("PresenterMessage=Item availability checking is successful;PresenterResponse=%s", response.getCode());
//                                if (response.isOk()) {
//                                    emitter.onNext(true);
//                                } else {
//                                    emitter.onNext(false);
//                                }
//                            }
//
//                            @Override
//                            public void onError(ErrorBundle errors) {
//                                Timber.e(errors.getCause(), "PresenterMessage=Item availability checking failed;TaskError=%s", errors.getMessage());
//                                emitter.onError(null);
//                            }
//                        }
//                );
//
//            }
//        });
//
//        return checkRulesetIsValidObservable;
//    }

//    public Observable checkRulesetIsUpgradable(final VersionableModel ruleset) {
//
//        Observable checkRulesetIsValidObservable = Observable.create(new ObservableOnSubscribe<Boolean>() {
//            @Override
//            public void subscribe(ObservableEmitter emitter) {
//
//
//                checkRulesetIsUpgradableTask.executeUseCase(
//                        new CheckRulesetIsUpgradableTask.CheckRulesetIsUpgradableRequest(ruleset),
//                        new UseCase.UseCaseCallback<CheckRulesetIsUpgradableTask.CheckRulesetIsUpgradableResponse>() {
//                            @Override
//                            public void onComplete(CheckRulesetIsUpgradableTask.CheckRulesetIsUpgradableResponse response) {
//                                Timber.d("PresenterMessage=Item upgrade checking is successful;PresenterResponse=%s", response.getCode());
//                                if (response.isOk()) {
//                                    emitter.onNext(true);
//                                    emitter.onComplete();
//                                } else {
//                                    emitter.onNext(true);
//                                    emitter.onComplete();
//                                }
//                            }
//
//                            @Override
//                            public void onError(ErrorBundle errors) {
//                                Timber.e(errors.getCause(), "PresenterMessage=Item upgrade checking failed;PresenterError=%s", errors.getMessage());
//                                emitter.onError(null);
//
//                            }
//                        }
//                );
//            }
//        });
//
//        return checkRulesetIsValidObservable;
//    }

    public void checkRulesetIsValid(final VersionableModel ruleset) {

//        view.disableRulesetDetails();
//        view.disableRulesetUpgrade();
        rulesetDetailsAvailable.postValue(false);
        rulesetDetailsDownloadable.postValue(false);

//        Completable checkRulesetIsValidObservable = Observable.create(new ObservableOnSubscribe<Boolean>() {
//            @Override
//            public void subscribe(ObservableEmitter emitter) {


        checkRulesetRulesExistenceTask.executeUseCase(
                new CheckRulesetRulesExistenceTask.CheckRulesetRulesExistenceRequest(ruleset),
                new UseCase.UseCaseCallback<CheckRulesetRulesExistenceTask.CheckRulesetRulesExistenceResponse>() {
                    @Override
                    public void onComplete(CheckRulesetRulesExistenceTask.CheckRulesetRulesExistenceResponse response) {
                        Timber.d("PresenterMessage=Item availability checking is successful;PresenterResponse=%s", response.getCode());
                        if (response.isOk()) {
//                                    view.enableRulesetDetails();
                            rulesetDetailsAvailable.postValue(true);
                        } else {
//                                    view.disableRulesetDetails();
                            rulesetDetailsAvailable.postValue(false);
                        }
                    }

                    @Override
                    public void onError(ErrorBundle errors) {
                        Timber.e(errors.getCause(), "PresenterMessage=Item availability checking failed;TaskError=%s", errors.getMessage());
//                                emitter.onError(null);
                        rulesetDetailsAvailable.postValue(false);
//                                view.disableRulesetDetails();
//                                view.showInvalidRuleset();
                    }
                }
        );

        checkRulesetIsUpgradableTask.executeUseCase(
                new CheckRulesetIsUpgradableTask.CheckRulesetIsUpgradableRequest(ruleset),
                new UseCase.UseCaseCallback<CheckRulesetIsUpgradableTask.CheckRulesetIsUpgradableResponse>() {
                    @Override
                    public void onComplete(CheckRulesetIsUpgradableTask.CheckRulesetIsUpgradableResponse response) {
                        Timber.d("PresenterMessage=Item upgrade checking is successful;PresenterResponse=%s", response.getCode());
                        if (response.isOk()) {
//                                    emitter.onNext(true);
//                                    emitter.onComplete();
//                                    view.enableRulesetUpgrade();
                            rulesetDetailsDownloadable.postValue(true);
                        } else {
//                                    emitter.onNext(true);
//                                    emitter.onComplete();
//                                    view.disableRulesetUpgrade();
                            rulesetDetailsDownloadable.postValue(false);
                        }
                    }

                    @Override
                    public void onError(ErrorBundle errors) {
                        Timber.e(errors.getCause(), "PresenterMessage=Item upgrade checking failed;PresenterError=%s", errors.getMessage());
//                                emitter.onError(null);
//                                view.disableRulesetUpgrade();
//                                view.showInvalidRuleset();
                        rulesetDetailsDownloadable.postValue(false);
                    }
                }
        );
//            }
//        });

//        return checkRulesetIsValidObservable;
    }

    public Observable loadRulesetObservable = Observable.create(new ObservableOnSubscribe<RulesetModel>() {
        @Override
        public void subscribe(ObservableEmitter emitter) {
            System.err.println("loadRuleset subscribe");
            loadRulesetTask.executeUseCase(new LoadRulesetTask.LoadRulesetRequest(ruleset), new UseCase.UseCaseCallback<LoadRulesetTask.LoadRulesetResponse>() {
                @Override
                public void onComplete(LoadRulesetTask.LoadRulesetResponse response) {
//                        System.err.println("loadRuleset subscribe");
                    System.err.println("loadRuleset comp");
                    emitter.onNext(response.getContent());
                    emitter.onComplete();
                }

                @Override
                public void onError(ErrorBundle errors) {
                    System.err.println("loadRuleset ERR");
                    emitter.onError(null);
                }
            });

        }
    });

    //    @Override
    public Observable loadRuleset(/*VersionableModel ruleset*/) {
        System.err.println("loadRuleset");

        Observable loadRulesetObservable = Observable.create(new ObservableOnSubscribe<RulesetModel>() {
            @Override
            public void subscribe(ObservableEmitter emitter) {
                System.err.println("loadRuleset subscribe");
                loadRulesetTask.executeUseCase(new LoadRulesetTask.LoadRulesetRequest(ruleset), new UseCase.UseCaseCallback<LoadRulesetTask.LoadRulesetResponse>() {
                    @Override
                    public void onComplete(LoadRulesetTask.LoadRulesetResponse response) {
//                        System.err.println("loadRuleset subscribe");

                        emitter.onNext(response.getContent());
                        emitter.onComplete();
                    }

                    @Override
                    public void onError(ErrorBundle errors) {
                        emitter.onError(null);
                    }
                });

            }
        });
        return loadRulesetObservable;
    }


    /**
     * @return a basic {@link UseCase} that returns a KO-{@link CheckRulesetIsUpgradableTask.CheckRulesetIsUpgradableResponse},
     * whatever the given {@link CheckRulesetIsUpgradableTask.CheckRulesetIsUpgradableRequest}
     */
    private static UseCase<CheckRulesetIsUpgradableTask.CheckRulesetIsUpgradableRequest, CheckRulesetIsUpgradableTask.CheckRulesetIsUpgradableResponse> alwaysFalse
    () {
        return (request, callback) -> callback.onComplete(CheckRulesetIsUpgradableTask.CheckRulesetIsUpgradableResponse.ko());
    }

    /**
     * @return a basic {@link UseCase} that raises a {@link com.orange.ocara.business.binding.ErrorBundle}, whatever the given {@link UpgradeRulesetTask.UpgradeRulesetRequest}
     */
    private static UseCase<UpgradeRulesetTask.UpgradeRulesetRequest, UpgradeRulesetTask.UpgradeRulesetResponse> alwaysError
    () {
        return (request, callback) -> callback.onError(new BizError("Impossible operation"));
    }

}
