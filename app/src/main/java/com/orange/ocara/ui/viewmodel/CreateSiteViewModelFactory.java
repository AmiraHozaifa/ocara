package com.orange.ocara.ui.viewmodel;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.model.SiteEntity;
import com.orange.ocara.tools.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreateSiteViewModelFactory implements ViewModelProvider.Factory {

    private ModelManager modelManager;


    public CreateSiteViewModelFactory(ModelManager modelManager) {
        this.modelManager = modelManager;
    }


    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new CreateSiteViewModel(modelManager);
    }

}