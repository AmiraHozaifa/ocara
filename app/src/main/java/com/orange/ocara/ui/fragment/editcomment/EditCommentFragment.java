/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.fragment.editcomment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.fragment.NavHostFragment;

import com.activeandroid.Model;
import com.orange.ocara.R;
import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.db.ModelManagerImpl;
import com.orange.ocara.data.cache.model.CommentEntity;
import com.orange.ocara.tools.FileUtils;
import com.orange.ocara.tools.StringUtils;
import com.orange.ocara.ui.fragment.BaseFragment;
import com.orange.ocara.ui.fragment.BaseFragmentAuditMangment;
import com.orange.ocara.ui.viewmodel.EditCommentViewModel;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.GONE;
import static androidx.navigation.Navigation.findNavController;

@EFragment
public abstract class EditCommentFragment extends BaseFragment {

//    @Bean(ModelManagerImpl.class)
//    ModelManager modelManager;

    @ViewById(R.id.comment)
    protected EditText commentContent;

    //    @Extra("commentId")
    protected Long commentId;
    //    @Extra("commentType")
    protected CommentEntity.Type commentType;
    //    @Extra("attachmentDirectory")
    protected String attachmentDirectory;
    //    @Extra
    protected String imageName;
    //    @Extra("name")
    protected String name;
    //    @Extra("auditId")
    protected Long auditId;
    //    @Extra("title")
    protected String title;
    //    @Extra("subTitle")
    protected String subTitle;

    protected CommentEntity comment;

//    @ViewById(R.id.title)
//    TextView customTitle;
//    @ViewById(R.id.subtitle)
//    TextView customSubtitle;

    protected EditCommentViewModel editCommentViewModel;

    protected void loadComment() {
        if (isNewComment()) {
            this.comment = new CommentEntity(commentType, "");
        } else {
            this.comment = Model.load(CommentEntity.class, commentId);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }
    //    @Override
//    void setUpToolbar() {
//        super.setUpToolbar();
//
//        final View responseButtonBar = LayoutInflater.from(this).inflate(R.layout.audit_object_toolbar, null);
//        responseButtonBar.findViewById(R.id.response_ok_button).setVisibility(View.GONE);
//        responseButtonBar.findViewById(R.id.response_no_answer_button).setVisibility(GONE);
//        responseButtonBar.findViewById(R.id.response_nok_button).setVisibility(View.GONE);
//
//        Toolbar.LayoutParams lp = new Toolbar.LayoutParams(Gravity.LEFT);
//        responseButtonBar.setLayoutParams(lp);
//
//        toolbar.addView(responseButtonBar);
//    }

//    @Override
//    public void setTitle(CharSequence title) {
//        super.setTitle("");
//        customTitle.setText(title);
//    }

//    void setSubtitle(String subTitle) {
//        if (!TextUtils.isEmpty(subTitle)) {
//            customSubtitle.setVisibility(View.VISIBLE);
//            customSubtitle.setText(subTitle);
//        }
//    }


    private void updateActionBar() {
//        ActionBar supportActionBar = getSupportActionBar();
//        supportActionBar.setDisplayShowTitleEnabled(false);
//        supportActionBar.setDisplayUseLogoEnabled(true);
        setTitle(title);
        if (subTitle != null)
            setSubtitle(subTitle);
        updateLogo(imageName);

    }

        @AfterViews
    void setUpComment() {

        if (title != null && StringUtils.isNotBlank(title) && StringUtils.isNotBlank(imageName)) {
            updateActionBar();
        } else {
            if (auditId != null) {
                loadAudit();
            }
        }

        commentContent.setText(comment.getContent());
        commentContent.requestFocus();

        if (isNewComment()) {
            createAttachment();
        } else {
            handleAttachment();
        }
    }

    void loadAudit() {
        editCommentViewModel.getAuditName(auditId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).
                subscribe(auditName -> {
                    initTitle(auditName);

                });
    }

    @UiThread
    void initTitle(String auditName) {
        String titleAudit = getString(R.string.edit_comment_title, auditName);
        setTitle(titleAudit);
    }

    @Click(R.id.validate_comment)
    void onValidateComment() {
        comment.setContent(commentContent.getText().toString());
        comment.save();

        NavHostFragment.findNavController(this).getPreviousBackStackEntry().getSavedStateHandle().set("commentId", comment.getId());
//        NavHostFragment.findNavController(this).popBackStack();

//        Intent intent = new Intent();
//        intent.putExtra("commentId", comment.getId());

//        setResult(RESULT_OK, intent);
        finish();
    }

    protected abstract void createAttachment();

    protected abstract void handleAttachment();

    protected String makeTimestampedFilename(String prefix) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        return prefix + "_" + timeStamp + "_";
    }


    protected String copyAsAttachment(File tempAttachment) throws IOException {
        return copyAsAttachment(tempAttachment, null);
    }

    protected String copyAsAttachment(File tempAttachment, String attachmentName) throws IOException {
        if (StringUtils.isBlank(attachmentName)) {
            attachmentName = tempAttachment.getName();
        }
        File targetAttachment = new File(attachmentDirectory, attachmentName);

        FileUtils.copyFile(tempAttachment, targetAttachment);
        return targetAttachment.getAbsolutePath();
    }


    /**
     * @return true if we are creating a new comment, false in case of editing an existing one.
     */
    protected boolean isNewComment() {
        return commentId == -1;
    }

}
