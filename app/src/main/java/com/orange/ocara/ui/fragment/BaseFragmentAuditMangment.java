package com.orange.ocara.ui.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;

import androidx.appcompat.app.AlertDialog;
import androidx.navigation.fragment.NavHostFragment;

import com.orange.ocara.R;
import com.orange.ocara.data.cache.db.ModelManagerImpl_;
import com.orange.ocara.data.cache.model.AuditEntity;
import com.orange.ocara.data.cache.model.AuditObjectEntity;
import com.orange.ocara.ui.activity.AuditObjectsActivity_;
import com.orange.ocara.ui.dialog.NotificationDialogBuilder;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public abstract class BaseFragmentAuditMangment extends BaseFragment {

    protected Long auditId;
    private static final long DIALOG_SHOW_DELAY_IN_MILLISECONDS = 5000;
    private static final long DIALOG_DISMISS_DELAY_IN_MILLISECONDS = 10000;


    private static long[] toPrimitive(final Long[] array) {
        if (array == null) {
            return null;
        } else if (array.length == 0) {
            return new long[0];
        }
        final long[] result = new long[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }

    protected List<Long> buildSelectedAuditObjectsList(AuditObjectEntity... auditObjects) {
        List<Long> selectedObjectIds = new ArrayList<>();
        for (AuditObjectEntity auditObject : auditObjects) {
            selectedObjectIds.add(auditObject.getId());
        }
        return selectedObjectIds;
    }

    public void launchAuditObjectsTest(boolean editMode, AuditObjectEntity... auditObjects) {
        Long[] selectedAuditObjectIds = buildSelectedAuditObjectsList(auditObjects).toArray(new Long[]{});
        long[] selectedObjects = toPrimitive(selectedAuditObjectIds);
        AuditEntity audit = ModelManagerImpl_.getInstance_(getContext()).getAudit(auditId);

        Bundle bundle = new Bundle();
        bundle.putLongArray("selectedObjects", selectedObjects);
        bundle.putLong("auditId", auditId);
        bundle.putBoolean("editMode", editMode);

        if (audit.getLevel() == AuditEntity.Level.EXPERT) {
//            AuditObjectsActivity_
//                    .intent(this)
//                    .auditId(auditId)
//                    .editMode(editMode)
//                    .selectedObjects(selectedObjects)
//                    .start();
            NavHostFragment.findNavController(this).navigate(R.id.action_test_audit_object_expert, bundle);
        } else {
            NavHostFragment.findNavController(this).navigate(R.id.action_test_audit_object_novice, bundle);
        }
    }

    private AlertDialog dialog = null;


    /**
     * Updates the view
     *
     * @param auditObject               an instance of {@link AuditObjectEntity}
     * @param terminateActivityWhenDone if true, ends the current {@link android.app.Activity}
     */
    public void showAuditObjectProgress(AuditObjectEntity auditObject, final boolean terminateActivityWhenDone) {
        CharSequence info = getText(R.string.auditing_progress_info);

        Timber.d("Show audit object progress;TerminateActivity=%b", terminateActivityWhenDone);

        Spannable auditingStatus = new SpannableString("");
        int color = getResources().getColor(R.color.black);
        switch (auditObject.getResponse()) {
            case OK:
                auditingStatus = new SpannableString(getText(R.string.auditing_progress_status_ok));
                color = getResources().getColor(R.color.green);
                break;
            case NOK:
                if (auditObject.hasAtLeastOneBlockingRule()) {
                    auditingStatus = new SpannableString(getText(R.string.auditing_progress_status_nok));
                    color = getResources().getColor(R.color.red);
                } else {

                    auditingStatus = new SpannableString(getText(R.string.auditing_progress_status_anoying));
                    color = getResources().getColor(R.color.orange);
                }
                break;
            case DOUBT:
                auditingStatus = new SpannableString(getText(R.string.auditing_progress_status_doubt));
                color = getResources().getColor(R.color.yellow);
                break;
            case NO_ANSWER:
                auditingStatus = new SpannableString(getText(R.string.auditing_progress_status_no_answer));
                color = getResources().getColor(R.color.blue);
                break;
        }

        auditingStatus.setSpan(new ForegroundColorSpan(color), info.length(), auditingStatus.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        StringBuilder stringBuffer = new StringBuilder(info);
        stringBuffer.append("<br>")
                .append(auditingStatus);

        if (terminateActivityWhenDone) {
            Timber.d("Terminate activity...");

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            if (sharedPreferences.getString(getString(R.string.setting_display_auditing_progress_key), "1").equals("1")) {
                final NotificationDialogBuilder dialogBuilder = new NotificationDialogBuilder(getActivity());
                dialog = dialogBuilder
                        .setInfo(auditingStatus)
                        .setOption(getString(R.string.auditing_progress_option))
                        .setCancelable(false)
                        .setTitle(R.string.auditing_progress_title)
                        .setOnDismissListener(currentDialog -> {
                            Timber.d("Message=Dismissing the alert dialog");
                            auditingProgressDismiss(dialogBuilder.getOptionValue());
                            dialog = null;
//                            BaseActivityManagingAudit.this.finish();
                            NavHostFragment.findNavController(this).navigateUp();

                        })
                        .setPositiveButton(R.string.action_close, null)
                        .create();

                Handler handler = new Handler();
                handler.postDelayed(() -> {
                    if (dialog != null && dialog.isShowing()) {
                        Timber.d("Message=Triggering the dismissing of the alert dialog");
                        dialog.dismiss();
                        dialog = null;
                    }
                }, DIALOG_DISMISS_DELAY_IN_MILLISECONDS);

                /**
                 * The call to dialog.show() is made in this {@link Handler}, so as to avoid a
                 * WindowLeaked error
                 */
                handler.postDelayed(() -> {
                    if (dialog != null && !dialog.isShowing()) {
                        Timber.d("Message=Triggering the display of the alert dialog");
                        dialog.show();
                    }
                }, DIALOG_SHOW_DELAY_IN_MILLISECONDS);

            }
            auditingProgressDismiss(true);
//            BaseActivityManagingAudit.this.finish();
            NavHostFragment.findNavController(this).navigateUp();

        }
    }

    private void auditingProgressDismiss(boolean doNotDisplayThisMessageAgain) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (doNotDisplayThisMessageAgain) {
            editor.putString(getString(R.string.setting_display_auditing_progress_key), "0");
        } else {
            editor.putString(getString(R.string.setting_display_auditing_progress_key), "1");
        }
        editor.apply();
    }


    @Override
    public void onPause() {
        super.onPause();
        if (dialog != null) {
            Timber.d("Message=Triggering the dismissing of the alert dialog while being pausing the activity");
            dialog.dismiss();
            dialog = null;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (dialog != null) {
            Timber.d("Message=Triggering the dismissing of the alert dialog while being stopping the activity");
            dialog.dismiss();
            dialog = null;
        }
    }
}
