package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import androidx.lifecycle.ViewModel;

import com.orange.ocara.business.BizConfig;
import com.orange.ocara.business.BizConfig_;
import com.orange.ocara.business.binding.ErrorBundle;
import com.orange.ocara.business.interactor.LoadRulesTask;
import com.orange.ocara.business.interactor.UseCase;
import com.orange.ocara.business.interactor.UseCaseHandler;
import com.orange.ocara.business.service.EquipmentService;
import com.orange.ocara.business.service.impl.EquipmentServiceImpl_;
import com.orange.ocara.data.net.model.Equipment;
import com.orange.ocara.ui.contract.ListEquipmentsContract;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;

@Deprecated
public class BrowseEquipmentsViewModel extends ViewModel {

    Context context;

    private EquipmentService equipmentService;
    private final UseCaseHandler useCaseHandler;
    private final UseCase<LoadRulesTask.LoadRulesRequest, LoadRulesTask.LoadRulesResponse> task;

    public Single<LoadRulesTask.LoadRulesResponse> loadRulesResponse;
    long selectedRulesetId;
    long selectedEquipmentId;

//    private final ListEquipmentsContract.ListEquipmentsView view;


    public BrowseEquipmentsViewModel(Context context) {
        System.err.println("OCARA : AUDIT BrowseEquipmentsViewModel const");
        equipmentService = EquipmentServiceImpl_.getInstance_(context);
        useCaseHandler = BizConfig.USE_CASE_HANDLER;
        task = BizConfig_.getInstance_(context).browseRulesTask();
        createLoadRulesResponse();
    }

    private void createLoadRulesResponse() {
        loadRulesResponse = Single.create(new SingleOnSubscribe<LoadRulesTask.LoadRulesResponse>() {
            @Override
            public void subscribe(SingleEmitter<LoadRulesTask.LoadRulesResponse> emitter) throws Exception {
                useCaseHandler.execute(task, new LoadRulesTask.LoadRulesRequest(selectedRulesetId, selectedEquipmentId), new UseCase.UseCaseCallback<LoadRulesTask.LoadRulesResponse>() {
                    @Override
                    public void onComplete(LoadRulesTask.LoadRulesResponse response) {
                        emitter.onSuccess(response);
//                        showRules(response.getGroups(), response.getRules());
                    }

                    @Override
                    public void onError(ErrorBundle errors) {
                        emitter.onError(null);
                        // do nothing yet
                    }
                });
            }
        });

    }

    public List<Equipment> loadAllEquipmentsByRulesetId(Long rulesetId) {
        return equipmentService.retrieveObjectDescriptionsByRulesetId(rulesetId);
    }

    public void loadAllRulesByEquipmentId(long rulesetId, long equipmentId/*, UseCase.UseCaseCallback<LoadRulesTask.LoadRulesResponse> callback*/) {
        selectedEquipmentId = equipmentId;
        selectedRulesetId = rulesetId;
//        loadRulesResponse.subscribe();
    }
}
