package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import androidx.lifecycle.ViewModel;

import com.orange.ocara.business.BizConfig;
import com.orange.ocara.business.BizConfig_;
import com.orange.ocara.business.binding.ErrorBundle;
import com.orange.ocara.business.interactor.LoadRulesTask;
import com.orange.ocara.business.interactor.UseCase;
import com.orange.ocara.business.interactor.UseCaseHandler;
import com.orange.ocara.business.service.EquipmentService;
import com.orange.ocara.business.service.RuleSetService;
import com.orange.ocara.business.service.impl.EquipmentServiceImpl_;
import com.orange.ocara.business.service.impl.RuleSetServiceImpl_;
import com.orange.ocara.data.net.model.Equipment;
import com.orange.ocara.data.net.model.RulesetEntity;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.UiThread;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;

public class BrowseRuleSetsViewModel extends ViewModel {

    Context context;
    RuleSetService mRuleSetService;
    public Single<List<RulesetEntity>> ruleSets;

    private RulesetEntity selectedRuleset;
    private Long selectedEquipmentId;
    private int selectedRuleIdx = -1;


    public BrowseRuleSetsViewModel(Context context) {
        System.err.println("OCARA : AUDIT BrowseRuleSetsViewModel constructor");
        this.context = context;
        mRuleSetService = RuleSetServiceImpl_.getInstance_(context);

        ruleSets = Single.create(new SingleOnSubscribe<List<RulesetEntity>>() {
            @Override
            public void subscribe(SingleEmitter<List<RulesetEntity>> emitter) throws Exception {
                List<RulesetEntity> retrievedRulesets = mRuleSetService.getDownloadedRulesetDetails();
                emitter.onSuccess(retrievedRulesets);
            }
        });

        equipmentService = EquipmentServiceImpl_.getInstance_(context);
        useCaseHandler = BizConfig.USE_CASE_HANDLER;
        task = BizConfig_.getInstance_(context).browseRulesTask();

    }

    public RulesetEntity getSelectedRuleset() {
        return selectedRuleset;
    }

    public void setSelectedRuleset(RulesetEntity selectedRuleset) {
        this.selectedRuleset = selectedRuleset;
    }

    public Long getSelectedEquipment() {
        return selectedEquipmentId;
    }

    public void setSelectedEquipment(Long selectedEquipmentReference) {
        this.selectedEquipmentId = selectedEquipmentReference;
    }

    public int getSelectedRuleIdx() {
        return selectedRuleIdx;
    }

    public void setSelectedRuleIdx(int selectedRuleIdx) {
        this.selectedRuleIdx = selectedRuleIdx;
    }
    //    @Background
//    public void loadRulesets() {
//
//    }
//
//    @UiThread
//    void displayRulesets(List<RulesetEntity> rulesets) {
//        mListRulesView.showRulesets(rulesets);
//    }


    private EquipmentService equipmentService;
    private final UseCaseHandler useCaseHandler;
    private final UseCase<LoadRulesTask.LoadRulesRequest, LoadRulesTask.LoadRulesResponse> task;

//    public Single<LoadRulesTask.LoadRulesResponse> loadRulesResponse;
//    long selectedRulesetId;
//    long selectedEquipmentId;

    public Single loadRules(long selectedRulesetId, long selectedEquipmentId) {
        Single<LoadRulesTask.LoadRulesResponse> loadRulesResponse;
        loadRulesResponse = Single.create(new SingleOnSubscribe<LoadRulesTask.LoadRulesResponse>() {
            @Override
            public void subscribe(SingleEmitter<LoadRulesTask.LoadRulesResponse> emitter) throws Exception {
                useCaseHandler.execute(task, new LoadRulesTask.LoadRulesRequest(selectedRulesetId, selectedEquipmentId), new UseCase.UseCaseCallback<LoadRulesTask.LoadRulesResponse>() {
                    @Override
                    public void onComplete(LoadRulesTask.LoadRulesResponse response) {
                        emitter.onSuccess(response);
//                        showRules(response.getGroups(), response.getRules());
                    }

                    @Override
                    public void onError(ErrorBundle errors) {
                        emitter.onError(null);
                        // do nothing yet
                    }
                });
            }
        });
        return loadRulesResponse;
    }

    public List<Equipment> loadAllEquipmentsByRulesetId(Long rulesetId) {
        return equipmentService.retrieveObjectDescriptionsByRulesetId(rulesetId);
    }

//    public void loadAllRulesByEquipmentId(long rulesetId, long equipmentId/*, UseCase.UseCaseCallback<LoadRulesTask.LoadRulesResponse> callback*/) {
//        selectedEquipmentId = equipmentId;
//        selectedRulesetId = rulesetId;
////        loadRulesResponse.subscribe();
//    }
}
