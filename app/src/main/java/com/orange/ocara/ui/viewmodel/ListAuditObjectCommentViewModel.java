package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;

import com.activeandroid.Model;
import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.db.ModelManagerImpl_;
import com.orange.ocara.data.cache.model.AuditObjectEntity;
import com.orange.ocara.data.cache.model.CommentEntity;
import com.orange.ocara.ui.tools.RefreshStrategy;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ListAuditObjectCommentViewModel extends ListCommentViewModel {

    protected AuditObjectEntity entity;

    public ListAuditObjectCommentViewModel(Context context, Long auditObjectId) {
        super(context, auditObjectId);
        this.entity = modelManager.getAuditObject(auditObjectId);

    }

    public void loadComments() {
        getCommentsBg();
    }

    private void getCommentsBg() {
        Single.create(new SingleOnSubscribe<List<CommentEntity>>() {
            @Override
            public void subscribe(SingleEmitter<List<CommentEntity>> emitter) throws Exception {
                List<CommentEntity> comment = getCommentsSequential();
                emitter.onSuccess(comment);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).
                subscribe(commentEntities -> {
                    commentsLiveData.setValue(commentEntities);

                });
    }

    private List<CommentEntity> getCommentsSequential() {
        modelManager.refresh(entity, RefreshStrategy.builder().commentsNeeded(true).build());
        List<CommentEntity> comments = entity.getComments();
        return comments;
//        commentsLiveData.setValue(comments);
    }

    @Override
    public void deleteComment(CommentEntity comment) {
        Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter emitter) throws Exception {
                modelManager.deleteComment(comment);
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(() -> {
            getCommentsBg();
        });
    }

    @Override
    public void deleteAllComments() {
        Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter emitter) throws Exception {
                modelManager.deleteAllComments(entity.getComments());
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(() -> {
            getCommentsBg();
        });
    }

    @Override
    public void updateComment(Long commentId) {
        Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter emitter) throws Exception {
                CommentEntity comment = Model.load(CommentEntity.class, commentId);
                entity.attachComment(comment);
                comment.save();
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(() -> {
            getCommentsBg();
        });
    }

    public String getAuditObjectCharacteristicsTitle() {
        String result = "";
        for (AuditObjectEntity characteristics : entity.getChildren()) {
            if (!result.isEmpty()) {
                result += " ";
            }
            result += "+" + "\u00A0" + characteristics.getName();
        }
        return result;
    }

    public String getEntityIcon() {
        return entity.getObjectDescription() == null ? null : entity.getObjectDescription().getIcon();
    }

    public String getEntityName() {
        return entity.getName();
    }

    public Long getAuditId() {
        return entity.getAudit().getId();
    }

}

