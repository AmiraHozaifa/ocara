/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.viewmodel;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;

import com.orange.ocara.R;
import com.orange.ocara.business.service.RuleSetService;
import com.orange.ocara.business.service.impl.RuleSetServiceImpl;
import com.orange.ocara.business.service.impl.RuleSetServiceImpl_;
import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.db.ModelManagerImpl_;
import com.orange.ocara.data.cache.model.AuditEntity;
import com.orange.ocara.data.net.model.Equipment;
import com.orange.ocara.data.net.model.EquipmentEntity;
import com.orange.ocara.data.net.model.IllustrationEntity;
import com.orange.ocara.ui.activity.BrowseRulesetsActivity_;
import com.orange.ocara.ui.fragment.BaseFragmentAuditMangment;
import com.orange.ocara.ui.view.IllustrationCommentItemView;
import com.orange.ocara.ui.view.IllustrationCommentItemView_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.orange.ocara.R.id.illustrations;

/**
 * displays information about an equipment
 */
public class ShowEquipmentInfoViewModel extends OcaraViewModel {

    private Long auditId;
    private String objectDescriptionId;
    private RuleSetService mRuleSetService;
    private AuditEntity audit;

    public MutableLiveData<EquipmentEntity> equipmentLiveData;
    public MutableLiveData<List<EquipmentEntity>> characteristicsLiveData;
    public MutableLiveData<List<IllustrationEntity>> illustrationsLiveData;


    public ShowEquipmentInfoViewModel(Context context, Long auditId, String objectDescriptionId) {
        super(context);
        this.auditId = auditId;
        this.objectDescriptionId = objectDescriptionId;

        ModelManager modelManager = ModelManagerImpl_.getInstance_(context);
        audit = modelManager.getAudit(auditId);
        mRuleSetService = RuleSetServiceImpl_.getInstance_(context);

        equipmentLiveData = new MutableLiveData<>();
        characteristicsLiveData = new MutableLiveData<>();
        illustrationsLiveData = new MutableLiveData<>();

        EquipmentEntity equipment = mRuleSetService.getObjectDescriptionFromRef(audit.getRuleSetRef(),
                audit.getRuleSetVersion(), objectDescriptionId);
        equipmentLiveData.setValue(equipment);
        updateObjectCharacteristics();
        updateObjectIllustrations();

    }

    private void updateObjectCharacteristics() {
        List<EquipmentEntity> characteristics = new ArrayList<>();
        if (equipmentLiveData.getValue() != null && equipmentLiveData.getValue().getSubObject() != null) {
            for (String ref : equipmentLiveData.getValue().getSubObject()) {
                characteristics.add(mRuleSetService.getObjectDescriptionFromRef(audit.getRuleSetRef(), audit.getRuleSetVersion(), ref));
            }
        }
        characteristicsLiveData.setValue(characteristics);
    }

    private void updateObjectIllustrations() {
        final List<IllustrationEntity> illustrations = new ArrayList<>();
        for (String ref : equipmentLiveData.getValue().getIllustration()) {
            illustrations.add(mRuleSetService.getIllutrationFormRef(equipmentLiveData.getValue().getRuleSetDetail().getReference(),
                    Integer.valueOf(equipmentLiveData.getValue().getRuleSetDetail().getVersion()), ref));
        }
        illustrationsLiveData.setValue(illustrations);
    }

    public AuditEntity getAudit() {
        return audit;
    }

}
