package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import com.orange.ocara.business.BizConfig;
import com.orange.ocara.business.BizConfig_;
import com.orange.ocara.business.interactor.LoadRuleExplanationsTask;
import com.orange.ocara.business.interactor.UseCase;
import com.orange.ocara.business.interactor.UseCaseHandler;

public class BrowseExplanationsViewModel extends OcaraViewModel {

    public BrowseExplanationsViewModel(Context context) {
        super(context);
        BizConfig bizConfig = BizConfig_.getInstance_(context);
        this.useCaseHandler = BizConfig.USE_CASE_HANDLER;
        task = bizConfig.loadExplanationsTask();
    }


    private final UseCaseHandler useCaseHandler;

    private final UseCase<LoadRuleExplanationsTask.LoadRuleExplanationsRequest, LoadRuleExplanationsTask.LoadRuleExplanationsResponse> task;


    public void loadExplanations(Long ruleId, UseCase.UseCaseCallback<LoadRuleExplanationsTask.LoadRuleExplanationsResponse> callback) {

        useCaseHandler.execute(task, new LoadRuleExplanationsTask.LoadRuleExplanationsRequest(ruleId), callback);
    }
}
