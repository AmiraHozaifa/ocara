package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import java.lang.reflect.InvocationTargetException;

public class ListCommentsViewModelFactory implements ViewModelProvider.Factory {
    private Long entityId;
    private Context context;

    public ListCommentsViewModelFactory(Context context, Long entityId) {
        this.context = context;
        this.entityId = entityId;
    }

    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (ListCommentViewModel.class.isAssignableFrom(modelClass)) {
            try {
                return modelClass.getConstructor(Context.class, Long.class).newInstance(context, entityId);
            } catch (NoSuchMethodException e) {

            } catch (IllegalAccessException e) {

            } catch (InstantiationException e) {

            } catch (InvocationTargetException e) {

            }
        }
        return null;
//        else {
//            throw IllegalStateException("ViewModel must extend MajesticViewModel");
//        }
    }
}