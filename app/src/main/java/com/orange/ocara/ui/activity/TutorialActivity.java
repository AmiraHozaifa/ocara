package com.orange.ocara.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.orange.ocara.R;
import com.orange.ocara.business.binding.ErrorBundle;
import com.orange.ocara.business.interactor.ChangeOnboardingStepTask;
import com.orange.ocara.business.interactor.CompleteOnboardingTask;
import com.orange.ocara.business.interactor.LoadOnboardingItemsTask;
import com.orange.ocara.business.interactor.SkipOnboardingTask;
import com.orange.ocara.business.interactor.UseCase;
import com.orange.ocara.business.model.OnboardingItemModel;
import com.orange.ocara.ui.adapter.TutorialContentAdapter;
import com.orange.ocara.ui.viewmodel.OcaraViewModelFactory;
import com.orange.ocara.ui.viewmodel.TutorialViewModel;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import static android.widget.Toast.LENGTH_SHORT;
import static android.widget.Toast.makeText;
import static timber.log.Timber.d;

@EActivity(R.layout.fragment_layout_tutorial)
public class TutorialActivity extends AppCompatActivity
//        implements TutorialDisplayView
{

    @ViewById(R.id.screen_viewpager)
    ViewPager screenPager;

    TutorialContentAdapter introViewPagerAdapter;

    @ViewById(R.id.tab_indicator)
    TabLayout tabIndicator;

    @ViewById(R.id.btn_next)
    Button btnNext;

    @ViewById(R.id.btn_get_started)
    Button btnGetStarted;

    @ViewById(R.id.btn_skip)
    Button btnSkip;

    TutorialViewModel viewModel;
//    @Bean(TutorialDisplayUiConfig.class)
//    TutorialDisplayUiConfig uiConfig;

    /**
     * a listener for the actions in this view
     */
//    private TutorialDisplayUserActionsListener actionsListener;

    private Animation btnAnim;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        makeText(this , "NEW ACTIVITY", Toast.LENGTH_LONG).show();
        viewModel = new ViewModelProvider(this, new OcaraViewModelFactory(this)).get(TutorialViewModel.class);
        viewModel.loadOnboarding(new LoadOnboardingItemsCallback());

    }

//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View v = super.onCreateView(inflater, container, savedInstanceState);
//
//        viewModel = new ViewModelProvider(this, new OcaraViewModelFactory(getContext())).get(TutorialViewModel.class);
//        viewModel.loadOnboarding(new TutorialFragment.LoadOnboardingItemsCallback());
//        return v;
//
//    }

//    @AfterInject
//    void setUp() {
//
//        actionsListener = uiConfig.actionsListener();
//        actionsListener.loadOnboarding(new LoadOnboardingItemsCallback(this));
//    }

    /**
     * hides the view
     */
//    @Override
    public void finishView() {

        makeText(this, R.string.tutorial_ended, LENGTH_SHORT)
                .show();
        onBackPressed();

    }

    /**
     * hides the view
     */
//    @Override
    public void cancelView() {

        makeText(this, R.string.tutorial_cancelled, LENGTH_SHORT)
                .show();

        onBackPressed();
    }

    /**
     * initializes all the components of the view with some content
     *
     * @param items a bunch of data
     */
//    @Override
    public void showItems(List<OnboardingItemModel> items) {

        // init views
        btnAnim = AnimationUtils.loadAnimation(this, R.anim.button_animation);

        // setting up viewpager
        introViewPagerAdapter = new TutorialContentAdapter(items);
        screenPager.setAdapter(introViewPagerAdapter);

        // setting up tablayout
        tabIndicator.setupWithViewPager(screenPager);

        // tab layout add change listener
        tabIndicator.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                d("ActivityMessage=New tab selected;SelectedPosition=%d", tab.getPosition());
                viewModel.notifyStepChanged(tab.getPosition(), new NotifyOnboardingStepChangedCallback());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                // do nothing yet
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                // do nothing yet
            }
        });

        // NEXT button click listener
        btnNext.setOnClickListener(v -> {

            d("ActivityMessage=Clicking on the NEXT button");
            viewModel.openNextStep(new ChangeOnboardingStepCallback());
        });

        // GET STARTED button click listener
        btnGetStarted.setOnClickListener(v -> {
            d("ActivityMessage=Clicking on the GET STARTED button");
            viewModel.completeOnboarding(new CompleteOnboardingCallback());
        });

        // SKIP button click listener
        btnSkip.setOnClickListener(v -> {
            d("ActivityMessage=Clicking on the SKIP button");
            viewModel.skipOnboarding(new SkipOnboardingCallback());
        });
    }


    public void showError(String message) {

        // do nothing yet
    }

    /**
     * updates the {@link ViewPager}
     *
     * @param position an index
     */
//    @Override
    public void showStep(int position) {

        d("ActivityMessage=Showing new step;PreviousIndex=%d;NewIndex=%d", screenPager.getCurrentItem(), position);
        screenPager.setCurrentItem(position);
    }

    /**
     * hides the GET STARTED button, shows the tabs indicator and the NEXT button
     */
//    @Override
    public void showNextButton() {

        btnGetStarted.setVisibility(View.INVISIBLE);
        btnNext.setVisibility(View.VISIBLE);
        tabIndicator.setVisibility(View.VISIBLE);
    }

    /**
     * shows the GET STARTED button, hides the tabs indicator and hides the NEXT button
     */
//    @Override
    public void showStartButton() {

        btnGetStarted.setVisibility(View.VISIBLE);
        btnNext.setVisibility(View.INVISIBLE);
        tabIndicator.setVisibility(View.INVISIBLE);

        // setup animation on the GET STARTED button
        btnGetStarted.setAnimation(btnAnim);
    }

    /** an implementation of {@link PagerAdapter} that manages the content of the tutorial */
//    static class TutorialContentAdapter extends PagerAdapter {
//
//        private final Context mContext;
//
//        private final List<OnboardingItemModel> mListScreen;
//
//        /**
//         * instantiate
//         *
//         * @param mContext    context of the adapter
//         * @param mListScreen a bunch of items to handle
//         */
//        TutorialContentAdapter(Context mContext, List<OnboardingItemModel> mListScreen) {
//            this.mContext = mContext;
//            this.mListScreen = mListScreen;
//        }
//
//        @Override
//        public int getCount() {
//            return mListScreen.size();
//        }
//
//        @Override
//        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
//            return view == object;
//        }
//
//        @Override
//        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
//            container.removeView((View) object);
//        }
//
//        @NonNull
//        @Override
//        public Object instantiateItem(@NonNull ViewGroup container, int position) {
//
//            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View layoutScreen = inflater.inflate(R.layout.layout_screen, null);
//
//            ImageView imgSlide = layoutScreen.findViewById(R.id.intro_img);
//            TextView title = layoutScreen.findViewById(R.id.intro_title);
//            TextView description = layoutScreen.findViewById(R.id.intro_description);
//
//            title.setText(mListScreen.get(position).getTitle());
//            description.setText(mListScreen.get(position).getDescription());
//            imgSlide.setImageResource((mListScreen.get(position).getScreenImg()));
//
//            container.addView(layoutScreen);
//
//            return layoutScreen;
//        }
//    }

    /**
     * a callback that deals with the content of the tutorial
     */
//    @RequiredArgsConstructor
    class LoadOnboardingItemsCallback implements UseCase.UseCaseCallback<LoadOnboardingItemsTask.LoadOnboardingItemsResponse> {

//        private final TutorialDisplayView view;

        @Override
        public void onComplete(LoadOnboardingItemsTask.LoadOnboardingItemsResponse response) {

            showItems(response.getItems());
        }

        @Override
        public void onError(ErrorBundle errors) {

            showError(errors.getMessage());
        }
    }

    /**
     * a callback that notifies that the tutorial has been done
     */
//    @RequiredArgsConstructor
    class CompleteOnboardingCallback implements UseCase.UseCaseCallback<CompleteOnboardingTask.CompleteOnboardingResponse> {

//        private final TutorialDisplayView view;

        @Override
        public void onComplete(CompleteOnboardingTask.CompleteOnboardingResponse response) {

            finishView();
        }

        @Override
        public void onError(ErrorBundle errors) {

            showError(errors.getMessage());
        }
    }

    /**
     * a callback that notifies that the tutorial has been interrupted
     */
    class SkipOnboardingCallback implements UseCase.UseCaseCallback<SkipOnboardingTask.SkipOnboardingResponse> {

//        private final TutorialDisplayView view;

        @Override
        public void onComplete(SkipOnboardingTask.SkipOnboardingResponse response) {

            cancelView();
        }

        @Override
        public void onError(ErrorBundle errors) {

            showError(errors.getMessage());
        }
    }

    /**
     * a callback that notifies about a change in the item to display
     */
    class ChangeOnboardingStepCallback implements UseCase.UseCaseCallback<ChangeOnboardingStepTask.ChangeOnboardingStepResponse> {

//        private final TutorialDisplayView view;

        @Override
        public void onComplete(ChangeOnboardingStepTask.ChangeOnboardingStepResponse response) {

            if (response.isLastStep()) {
                showStartButton();
            } else {
                showNextButton();
            }
            showStep(response.getTargetPosition());
        }

        @Override
        public void onError(ErrorBundle errors) {

            showError(errors.getMessage());
        }
    }


    /**
     * a callback that notifies about a change in the item to display
     */

    class NotifyOnboardingStepChangedCallback implements UseCase.UseCaseCallback<ChangeOnboardingStepTask.ChangeOnboardingStepResponse> {

//        private final TutorialDisplayView view;

        @Override
        public void onComplete(ChangeOnboardingStepTask.ChangeOnboardingStepResponse response) {

            if (response.isLastStep()) {
                showStartButton();
            } else {
                showNextButton();
            }
        }

        @Override
        public void onError(ErrorBundle errors) {

            showError(errors.getMessage());
        }
    }
}
