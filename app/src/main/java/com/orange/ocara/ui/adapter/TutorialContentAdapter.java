package com.orange.ocara.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.orange.ocara.R;
import com.orange.ocara.business.model.OnboardingItemModel;

import java.util.List;

public class TutorialContentAdapter extends PagerAdapter {

//    private final Context mContext;

    private final List<OnboardingItemModel> mListScreen;

    /**
     * instantiate
     *
//     * @param mContext    context of the adapter
     * @param mListScreen a bunch of items to handle
     */
    public TutorialContentAdapter( List<OnboardingItemModel> mListScreen) {
//        this.mContext = mContext;
        this.mListScreen = mListScreen;
    }

    @Override
    public int getCount() {
        return mListScreen.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        LayoutInflater inflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layoutScreen = inflater.inflate(R.layout.layout_screen, null);

        ImageView imgSlide = layoutScreen.findViewById(R.id.intro_img);
        TextView title = layoutScreen.findViewById(R.id.intro_title);
        TextView description = layoutScreen.findViewById(R.id.intro_description);

        title.setText(mListScreen.get(position).getTitle());
        description.setText(mListScreen.get(position).getDescription());
        imgSlide.setImageResource((mListScreen.get(position).getScreenImg()));

        container.addView(layoutScreen);

        return layoutScreen;
    }
}

