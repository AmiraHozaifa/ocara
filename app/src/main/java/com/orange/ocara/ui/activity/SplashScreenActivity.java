package com.orange.ocara.ui.activity;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProvider;

import com.orange.ocara.R;
import com.orange.ocara.business.binding.ErrorBundle;
import com.orange.ocara.business.interactor.CheckTermsStatusTask.CheckTermsStatusResponse;
import com.orange.ocara.business.interactor.InitTask;
import com.orange.ocara.business.interactor.UseCase.UseCaseCallback;
import com.orange.ocara.ui.fragment.TermsOfUseSelectionActivity;
import com.orange.ocara.ui.viewmodel.OcaraViewModelFactory;
import com.orange.ocara.ui.viewmodel.SplashActivityViewModel;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;

import lombok.RequiredArgsConstructor;

import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.makeText;
import static com.orange.ocara.business.interactor.CheckOnboardingStatusTask.CheckOnboardingStatusResponse;
import static com.orange.ocara.business.interactor.CheckRemoteStatusTask.CheckRemoteStatusResponse;
import static timber.log.Timber.d;
import static timber.log.Timber.i;

public class SplashScreenActivity extends AppCompatActivity {

    public static final int SHOW_TERMS_REQUEST_CODE = 1;

    public static final int SHOW_TUTORIAL_REQUEST_CODE = 2;

    public static final int LEAVE_INTRO_REQUEST_CODE = 3;

    SplashActivityViewModel splashActivityViewModel;

    public static final int PERMISSION_REQUEST = 10;

//    @Bean(DestinationController.class)
//    Navigation navController;

//    @Bean(IntroUiConfig.class)
//    IntroUiConfig uiConfig;

//    private IntroUserActionsListener actionsListener;

//    @AfterInject
//    void afterInject() {
//
//        actionsListener = uiConfig.actionsListener();
//        this.start();
//    }
//
//    @Override
//    public void start() {
//
//        this.actionsListener.checkRemote(new RemoteCallback(this));
//        this.actionsListener.checkInit(new InitCallback(this));
//    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        splashActivityViewModel = new ViewModelProvider(this, new OcaraViewModelFactory(this))
                .get(SplashActivityViewModel.class);


        splashActivityViewModel.checkRemote(new RemoteCallback());
        splashActivityViewModel.checkInit(new InitCallback());

    }

    @Override
    protected void onResume() {
        super.onResume();}


    public void navigateToHome() {

        // we leave this activity, and try to open the main activity
        d("ActivityMessage=Opening main activity");
//        navController.navigateToHome(IntroActivity.this, LEAVE_INTRO_REQUEST_CODE);


        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
//        Intent intent = new Intent(this ,MainActivity.class);
//        intent.putExtra("auditId" , null);
//        startActivityForResult(intent , LEAVE_INTRO_REQUEST_CODE);
    }

    public void showTerms() {

        d("ActivityMessage=Showing terms");
//        TermsOfUseAcceptanceFragment_
//                .intent(SplashScreenActivity.this)
//                .startForResult(SHOW_TERMS_REQUEST_CODE);
        Intent intent = new Intent(SplashScreenActivity.this, TermsOfUseSelectionActivity.class);
        startActivityForResult(intent, SHOW_TERMS_REQUEST_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == SHOW_TERMS_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK ) {
//                this.attemptNextDialog(SHOW_TUTORIAL_REQUEST_CODE);
                checkPermissions();
            }else {
                finish();
            }
        }
        else if (requestCode == SHOW_TUTORIAL_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK || resultCode == Activity.RESULT_CANCELED) {
                this.attemptNextDialog(LEAVE_INTRO_REQUEST_CODE);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

//    @OnActivityResult(SHOW_TUTORIAL_REQUEST_CODE)
//    void onTutorialClosed(int resultCode) {
//
//        d("ActivityMessage=Tutorial completed;ResultCode=%d", resultCode);
//        if (resultCode == Activity.RESULT_OK || resultCode == Activity.RESULT_CANCELED) {
//            this.attemptNextDialog(LEAVE_INTRO_REQUEST_CODE);
//        }
//    }

//    @OnActivityResult(SHOW_TERMS_REQUEST_CODE)
//    void onTermsClosed(int resultCode) {
//
//        d("ActivityMessage=Terms completed;ResultCode=%d", resultCode);
//        if (resultCode == Activity.RESULT_OK || resultCode == Activity.RESULT_CANCELED) {
//            this.attemptNextDialog(SHOW_TUTORIAL_REQUEST_CODE);
//        }
//    }


    public void attemptNextDialog(int code) {
        if (code == SHOW_TERMS_REQUEST_CODE) {
            splashActivityViewModel.checkTerms(new CheckTermsStatusCallback());
        } else if (code == SHOW_TUTORIAL_REQUEST_CODE) {
            splashActivityViewModel.checkTutorial(new CheckTutorialStatusCallback());
        } else {
            this.navigateToHome();
        }
    }


    public void showTutorial() {

        d("ActivityMessage=Showing tutorial");
//        TutorialDisplayFragment_
//                .intent(IntroActivity.this)
//                .startForResult(SHOW_TUTORIAL_REQUEST_CODE);
        TutorialActivity_
                .intent(SplashScreenActivity.this)
                .startForResult(SHOW_TUTORIAL_REQUEST_CODE);

    }


    public void showError(String message) {

        // do nothing yet
        i("ErrorMessage=%s;", message);
    }

    @RequiredArgsConstructor
    class CheckTermsStatusCallback implements UseCaseCallback<CheckTermsStatusResponse> {

//        private final IntroView view;

        @Override
        public void onComplete(CheckTermsStatusResponse response) {

            if (response.isAccepted()) {
                // the terms have been accepted. We can go to the next step.
//                attemptNextDialog(SHOW_TUTORIAL_REQUEST_CODE);
                checkPermissions();
            } else {
                // the terms have never been findAll, or they were previously declined. So we show them again.
                showTerms();
            }
        }

        @Override
        public void onError(ErrorBundle errors) {

            showError(errors.getMessage());
        }
    }

    /**
     * Callback that manages the response to the loading of a tutorial
     */
    @RequiredArgsConstructor
    class CheckTutorialStatusCallback implements UseCaseCallback<CheckOnboardingStatusResponse> {

//        private final IntroView view;

        @Override
        public void onComplete(CheckOnboardingStatusResponse response) {

            if (response.isOngoing()) {
                // the tutorial has not been completed yet
                showTutorial();
            } else {
                // the tutorial is completed
                attemptNextDialog(LEAVE_INTRO_REQUEST_CODE);
            }
        }

        @Override
        public void onError(ErrorBundle errors) {

            // if the process fails, we still should proceed to the next step
//            view.showError(errors.getMessage());
            attemptNextDialog(LEAVE_INTRO_REQUEST_CODE);
        }
    }

    /**
     * Callback that manages the response to the initialization of the app
     */
    @RequiredArgsConstructor
    class InitCallback implements UseCaseCallback<InitTask.InitResponse> {

//        private final IntroView view;

        @Override
        public void onComplete(InitTask.InitResponse initResponse) {
            attemptNextDialog(SHOW_TERMS_REQUEST_CODE);
        }

        @Override
        public void onError(ErrorBundle errors) {

            // if the process fails, we still should proceed to the next step
//            view.showError(errors.getMessage());
            attemptNextDialog(SHOW_TERMS_REQUEST_CODE);
        }
    }

    @RequiredArgsConstructor
    class RemoteCallback implements UseCaseCallback<CheckRemoteStatusResponse> {

//        private final Context context;

        @Override
        public void onComplete(CheckRemoteStatusResponse response) {

            if (!response.isSuccess()) {
                makeText(SplashScreenActivity.this, R.string.running_offline, LENGTH_LONG).show();
            }
        }

        @Override
        public void onError(ErrorBundle errors) {

            // do nothing yet...
        }
    }


    private void checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO,
            }, PERMISSION_REQUEST);
        }
        else{
            attemptNextDialog(SHOW_TUTORIAL_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST) {
            boolean permissionIsMissing = false;
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    permissionIsMissing = true;
                }
            }
            if (permissionIsMissing) {
                Toast.makeText(this, R.string.error_missing_permission, Toast.LENGTH_LONG).show();
                finish();
            }else {
                attemptNextDialog(SHOW_TUTORIAL_REQUEST_CODE);
            }
        }
    }
}