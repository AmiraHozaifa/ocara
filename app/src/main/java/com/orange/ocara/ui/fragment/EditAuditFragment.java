/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.orange.ocara.R;
import com.orange.ocara.business.model.RulesetModel;
import com.orange.ocara.data.cache.model.AuditEntity;
import com.orange.ocara.data.cache.model.AuditorEntity;
import com.orange.ocara.databinding.FragmentEditAuditBindingImpl;
import com.orange.ocara.tools.ListUtils;
import com.orange.ocara.ui.adapter.AuditorItemListAdapter;
import com.orange.ocara.ui.adapter.RulesetStatusAdapter;
import com.orange.ocara.ui.contract.UpdateAuditContract;
import com.orange.ocara.ui.model.AuditFormUiModel;
import com.orange.ocara.ui.presenter.UpdateAuditPresenter;
import com.orange.ocara.ui.viewmodel.EditAuditViewModel;
import com.orange.ocara.ui.viewmodel.EditAuditViewModelFactory;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.UiThread;

import io.reactivex.observers.DisposableCompletableObserver;

import static android.widget.Toast.LENGTH_SHORT;
import static android.widget.Toast.makeText;
import static timber.log.Timber.d;
import static timber.log.Timber.i;

/**
 * View that displays a form which aims on editing an audit
 * <p>
 * This is a view (in the MVP way), so it implements {@link UpdateAuditContract.UpdateAuditView}.
 * Then it is also related to {@link UpdateAuditContract.UpdateAuditUserActionsListener}, whose
 * implementation is a presenter cleverly named {@link UpdateAuditPresenter}.
 * <p>
 * This also implements {@link AuditFormUiModel} as it requires fields validations, as forms usually do.
 */
@EFragment(R.layout.fragment_edit_audit)
public class EditAuditFragment extends BaseFragment
//        implements UpdateAuditContract.UpdateAuditView, AuditFormUiModel
{


    @Bean
    AuditorItemListAdapter authorItemListAdapter;

//    private ProgressDialog mInitPageDialog;


    private RulesetStatusAdapter rulesAdapter;

    //    /**
//     * Listener for user's actions, aka the presenter in the MVP pattern
//     */
    private UpdateAuditContract.UpdateAuditUserActionsListener actionsListener;
    FragmentEditAuditBindingImpl binding;
    EditAuditViewModel updateAuditViewModel;
    Observer<String> fieldObserver = new Observer<String>() {
        @Override
        public void onChanged(String s) {
            updateAuditViewModel.validateUiAudit();
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
        long auditId = EditAuditFragment_Args.fromBundle(getArguments()).getAuditId();
//Toast.makeText(getActivity() , "IN FR" , Toast.LENGTH_LONG).show();
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_audit, container, false);
        View rootView = binding.getRoot();

//        ViewModelProvider provider = ;
        updateAuditViewModel = new ViewModelProvider(this, new EditAuditViewModelFactory(getActivity(), auditId)).get(EditAuditViewModel.class);
        binding.setUpdateAuditViewModel(updateAuditViewModel);
        binding.setLifecycleOwner(this);

        updateAuditViewModel.uiAuditName.observe(getViewLifecycleOwner(), fieldObserver);
        updateAuditViewModel.uiAuditAuthorName.observe(getViewLifecycleOwner(), fieldObserver);

        showLevel(updateAuditViewModel.getDatabaseAudit().getLevel());
        showAuthor();
        binding.editAuditLayout.setOnClickListener(view -> hideSoftKeyboard());
        rulesAdapter = new RulesetStatusAdapter(getContext(), ListUtils.newArrayList(updateAuditViewModel.getRuleSet()));
        binding.rulesetType.setAdapter(rulesAdapter);


        return rootView;

    }

    @UiThread
    public void showAuthor() {
        authorItemListAdapter.setModelManager(updateAuditViewModel.getModelManager());

        binding.authorCompleteView.setAdapter(authorItemListAdapter);
    }

    /**
     * displays the kind of experience required by the {@link AuditorEntity} to make the current {@link AuditEntity}
     *
     * @param level a {@link AuditEntity.Level}
     */
//    @UiThread
//    @Override
    public void showLevel(AuditEntity.Level level) {

        binding.level.requestFocus();
        binding.level.setChecked(AuditEntity.Level.EXPERT.equals(level));
        binding.level.setOnCheckedChangeListener((compoundButton, b) -> {
            updateAuditViewModel.setUiAuditLevel(binding.level.isChecked() ? AuditEntity.Level.EXPERT : AuditEntity.Level.BEGINNER);
        });
    }

    @TextChange(R.id.author_complete_view)
    void onAuthorTextChange(CharSequence text) {
//        validateAudit();
        authorItemListAdapter.getFilter().filter(text);
    }

    public void showRuleset(RulesetModel ruleset) {
        d("ActivityMessage=Showing a ruleset;RulesetRef=%s;RulesetVersion=%s", ruleset.getReference(), ruleset.getVersion());

        rulesAdapter = new RulesetStatusAdapter(getActivity(), ListUtils.newArrayList(ruleset));
        binding.rulesetType.setAdapter(rulesAdapter);

        showRulesetIcons();
    }

    @UiThread
    public void showRulesetIcons() {
        binding.rulesInfo.setVisibility(View.VISIBLE);
    }


    @Click(R.id.start_audit_button)
    void onAuditStartClicked() {
        i("ActivityMessage=Start button has been clicked...");
        updateAudit();
    }

    public void updateAudit() {
        updateAuditViewModel.updateAudit().subscribe(new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                this.dispose();
                NavHostFragment.findNavController(EditAuditFragment.this).navigateUp();
            }

            @Override
            public void onError(Throwable e) {
                this.dispose();
            }
        });

    }

    @LongClick(R.id.rules_info)
    public void showRulesInfoLabel() {
        makeText(getContext(), R.string.edit_audit_show_rules_information, LENGTH_SHORT).show();
    }

}
