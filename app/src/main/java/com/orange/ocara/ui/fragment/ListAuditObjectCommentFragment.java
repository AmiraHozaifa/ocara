/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.fragment;

import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.orange.ocara.R;
import com.orange.ocara.data.cache.model.CommentEntity;
import com.orange.ocara.ui.action.EditCommentActionBuilder;
import com.orange.ocara.ui.dialog.OcaraDialogBuilder;
import com.orange.ocara.ui.intent.EditCommentIntentBuilder;
import com.orange.ocara.ui.viewmodel.ListAuditObjectCommentViewModel;
import com.orange.ocara.ui.viewmodel.ListCommentViewModel;
import com.orange.ocara.ui.viewmodel.ListCommentsViewModelFactory;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import static com.orange.ocara.R.id.buttonbar_left_button;

@EFragment(R.layout.fragment_list_comment)
@OptionsMenu(R.menu.comment_list)
public class ListAuditObjectCommentFragment extends ListCommentFragment {

    private static final int EDIT_COMMENT_REQUEST_CODE = 1;

    //    @Extra("auditObjectId")
    Long auditObjectId;

    //    @Extra("attachmentDirectory")
    String attachmentDirectory;
//    String title;
//    String subTitle;

//    protected AuditObjectEntity entity;

//    @Bean(ModelManagerImpl.class)
//    ModelManager modelManager;

    @ViewById(R.id.comment_list)
    RecyclerView commentListView;

    @ViewById(R.id.comment_list_empty)
    View emptyView;

    @ViewById(buttonbar_left_button)
    Button leftButton;

    @ViewById(R.id.buttonbar_right_button)
    Button rightButton;

    //    private AuditCommentsRVAdapter commentListAdapter;
    ListAuditObjectCommentViewModel commentViewModel;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_list_comment, container, false);

        commentListView = v.findViewById(R.id.comment_list);
        emptyView = v.findViewById(R.id.comment_list_empty);
        leftButton = v.findViewById(R.id.buttonbar_left_button);
        rightButton = v.findViewById(R.id.buttonbar_right_button);


        attachmentDirectory = ListAuditObjectCommentFragment_Args.fromBundle(getArguments()).getAttachmentDirectory();
        auditObjectId = ListAuditObjectCommentFragment_Args.fromBundle(getArguments()).getAuditObjectId();

        commentViewModel = new ViewModelProvider(this, new ListCommentsViewModelFactory(getActivity(), auditObjectId)).get(ListAuditObjectCommentViewModel.class);
        initView();
//        commentViewModel.commentsLiveData.observe(getViewLifecycleOwner(), commentListUpdateObserver);
        updateLogo(commentViewModel.getEntityIcon());


        MutableLiveData liveData = NavHostFragment.findNavController(this).getCurrentBackStackEntry()
                .getSavedStateHandle()
                .getLiveData("commentId");
        liveData.observe(getViewLifecycleOwner(), new Observer() {
            @Override
            public void onChanged(Object commentId) {
                commentViewModel.updateComment((Long) commentId);
            }
        });

        return v;
    }

//    @OnActivityResult(EDIT_COMMENT_REQUEST_CODE)
//    void onResult(int resultCode, @OnActivityResult.Extra(value = "commentId") Long commentId) {
//
//        if (resultCode == RESULT_OK) {
//            updateComment(commentId);
//        }
//    }

    @OptionsItem(R.id.delete_all_comments)
    void onDeleteAllComments() {
        AlertDialog confirmDialog = new OcaraDialogBuilder(getActivity())
                .setTitle(R.string.comment_list_delete_all_comment_message_title)
                .setMessage(getString(R.string.comment_list_delete_all_comment_object_message))
                .setPositiveButton(R.string.action_remove, (dialog, which) -> commentViewModel.deleteAllComments())
                .setNegativeButton(R.string.action_cancel, null)
                .create();
        confirmDialog.show();
    }

    public void showNewComment(CommentEntity.Type type) {
//        new EditCommentIntentBuilder(getActivity(), type, attachmentDirectory)
//                .auditId(commentViewModel.getAuditId())
//                .title(commentViewModel.getEntityName())
//                .subtitle(commentViewModel.getAuditObjectCharacteristicsTitle())
//                .startIntent(EDIT_COMMENT_REQUEST_CODE);



        Pair<Integer, Bundle> pair = new EditCommentActionBuilder(getContext(), type, attachmentDirectory)
                .auditId(commentViewModel.getAuditId())
                .title(commentViewModel.getEntityName())
                .subtitle(commentViewModel.getAuditObjectCharacteristicsTitle())
                .getAction();
        NavHostFragment.findNavController(this).navigate(pair.first, pair.second);

    }

    public void showComment(CommentEntity c) {
//        new EditCommentIntentBuilder(getActivity(), c.getType(), attachmentDirectory)
//                .commentId(c.getId())
//                .auditId(commentViewModel.getAuditId())
//                .title(commentViewModel.getEntityName())
//                .subtitle(commentViewModel.getAuditObjectCharacteristicsTitle())
//                .startIntent(EDIT_COMMENT_REQUEST_CODE);


//        MutableLiveData liveData = NavHostFragment.findNavController(this).getCurrentBackStackEntry()
//                .getSavedStateHandle()
//                .getLiveData("commentId");
//        liveData.observe(getViewLifecycleOwner(), new Observer() {
//            @Override
//            public void onChanged(Object commentId) {
//                commentViewModel.updateComment((Long) commentId);
//            }
//        });
        Pair<Integer, Bundle> pair = new EditCommentActionBuilder(getContext(), c.getType(), attachmentDirectory)
                .commentId(c.getId())
                .auditId(commentViewModel.getAuditId())
                .title(commentViewModel.getEntityName())
                .subtitle(commentViewModel.getAuditObjectCharacteristicsTitle())
                .getAction();
        NavHostFragment.findNavController(this).navigate(pair.first, pair.second);
    }

    @Override
    public ListCommentViewModel getCommentsViewModel() {

        return commentViewModel;
    }

    @Override
    public String getTitle() {
        return commentViewModel.getEntityName();
    }

    public RecyclerView getCommentListView() {
        return commentListView;
    }

    public View getEmptyView() {
        return emptyView;
    }

    public Button getLeftButton() {
        return leftButton;
    }

    public Button getRightButton() {
        return rightButton;
    }
}
