package com.orange.ocara.ui.fragment;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.orange.ocara.R;
import com.orange.ocara.business.binding.ErrorBundle;
import com.orange.ocara.business.interactor.AcceptTermsOfUseTask;
import com.orange.ocara.business.interactor.DeclineTermsOfUseTask;
import com.orange.ocara.business.interactor.UseCase;
import com.orange.ocara.ui.viewmodel.CreateSiteViewModel;
import com.orange.ocara.ui.viewmodel.CreateSiteViewModelFactory;
import com.orange.ocara.ui.viewmodel.OcaraViewModelFactory;
import com.orange.ocara.ui.viewmodel.TermsOfUseSelectionViewModel;

import static android.widget.Toast.LENGTH_SHORT;
import static android.widget.Toast.makeText;

public class TermsOfUseSelectionFragment extends Fragment {
    TermsOfUseSelectionViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView_ = inflater.inflate(R.layout.fragment_terms_select, container, false);
        viewModel = new ViewModelProvider(this, new OcaraViewModelFactory(getContext())).get(TermsOfUseSelectionViewModel.class);

        container.findViewById(R.id.terms_of_use_refuse_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refuseTerms();
            }
        });
        container.findViewById(R.id.terms_of_use_accept_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptTerms();
            }
        });


        return contentView_;
    }

    private void refuseTerms() {
        viewModel.declineTerms(new UseCase.UseCaseCallback<DeclineTermsOfUseTask.DeclineTermsOfUseResponse>() {
            @Override
            public void onComplete(DeclineTermsOfUseTask.DeclineTermsOfUseResponse declineTermsOfUseResponse) {
                showTermsDeclined();
            }

            @Override
            public void onError(ErrorBundle errors) {
                showError(errors.getMessage());
            }
        });
    }

    private void acceptTerms() {
        viewModel.acceptTerms(new UseCase.UseCaseCallback<AcceptTermsOfUseTask.AcceptTermsOfUseResponse>() {
            @Override
            public void onComplete(AcceptTermsOfUseTask.AcceptTermsOfUseResponse acceptTermsOfUseResponse) {
                showTermsAccepted();
            }

            @Override
            public void onError(ErrorBundle errors) {
                showError(errors.getMessage());
            }
        });
    }

    public void showError(String message) {
        makeText(getContext(), R.string.terms_error, LENGTH_SHORT)
                .show();
    }

    public void showTermsAccepted() {
        makeText(getContext(), R.string.terms_accepted, LENGTH_SHORT)
                .show();
        finish();
    }

    public void showTermsDeclined() {

        makeText(getContext(), R.string.terms_declined, LENGTH_SHORT)
                .show();

        // Terms refused. We leave the app.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().finishAndRemoveTask();
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            getActivity().finishAffinity();
        } else {
            finish();
        }
    }
    public void finish() {
        NavHostFragment.findNavController(this).navigateUp();
    }

}
