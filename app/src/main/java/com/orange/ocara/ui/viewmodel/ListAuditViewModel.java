package com.orange.ocara.ui.viewmodel;

import android.content.Context;
import android.widget.Filter;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.orange.ocara.business.BizConfig_;
import com.orange.ocara.business.binding.ErrorBundle;
import com.orange.ocara.business.interactor.RedoAuditTask;
import com.orange.ocara.business.interactor.UseCase;
import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.db.ModelManagerImpl_;
import com.orange.ocara.data.cache.model.AuditEntity;
import com.orange.ocara.data.cache.model.SortCriteria;

import java.util.Collection;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;

import static com.orange.ocara.tools.ListUtils.emptyList;

public class ListAuditViewModel extends OcaraViewModel {

    public MutableLiveData<List<AuditEntity>> auditsLiveData;
//    List<AuditEntity> auditsList;

//    Context context;
    private Filter filter;
    ModelManager modelManager;
    private final UseCase<RedoAuditTask.RedoAuditRequest, RedoAuditTask.RedoAuditResponse> redoAuditTask;

    private String queryFilter = "";
    private SortCriteria sortCriteria = SortCriteria.Type.STATUS.build();

    public ListAuditViewModel(Context context) {
        super(context);
//        this.context = context;
        redoAuditTask = BizConfig_.getInstance_(context).redoAuditTask();
        modelManager = ModelManagerImpl_.getInstance_(context);
        auditsLiveData = new MutableLiveData<>();
    }


    public Single createNewAudit(AuditEntity itemToCopy) {
        RedoAuditTask.RedoAuditRequest request = new RedoAuditTask.RedoAuditRequest(itemToCopy);
        return Single.create(new SingleOnSubscribe<AuditEntity>() {
            @Override
            public void subscribe(SingleEmitter<AuditEntity> emitter) throws Exception {
                redoAuditTask.executeUseCase(request, new UseCase.UseCaseCallback<RedoAuditTask.RedoAuditResponse>() {
                    @Override
                    public void onComplete(RedoAuditTask.RedoAuditResponse response) {
                        emitter.onSuccess(response.getAudit());
                    }

                    @Override
                    public void onError(ErrorBundle errors) {
                        emitter.onError(null);
                    }
                });
            }
        });
    }

    public void deleteAudit(AuditEntity audit) {
        Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter emitter) throws Exception {
                modelManager.deleteAudit(audit.getId());
//                auditsList = modelManager.getAllAudits(queryFilter, sortCriteria);
                auditsLiveData.postValue(modelManager.getAllAudits(queryFilter, sortCriteria));
                emitter.onComplete();
            }
        }).subscribe();
    }

    public SortCriteria getSortCriteria() {
        return sortCriteria;
    }

    /**
     * updates the content of the adapter with default parameters
     */
    public void loadAudits() {
        performSortAndSearch(queryFilter, sortCriteria);
    }

    /**
     * updates the content of the adapter
     *
     * @param sortCriteria a sorting attribute
     */
    public void sortAudits(SortCriteria sortCriteria) {
        performSortAndSearch(queryFilter, sortCriteria);
    }

    /**
     * updates the content of the adapter
     *
     * @param query a filtering string
     */
    public void searchAudits(String query) {
        performSortAndSearch(query, sortCriteria);
    }

    private Filter getFilter() {
        if (filter == null) {
            filter = new AuditFilter();
        }

        return filter;
    }

    /**
     * updates the content of the adapter
     *
     * @param queryFilter  a filtering string
     * @param sortCriteria a sorting attribute
     */
    private void performSortAndSearch(String queryFilter, SortCriteria sortCriteria) {
        this.queryFilter = queryFilter;
        this.sortCriteria = sortCriteria;

        getFilter().filter(queryFilter);

//        mAuditListAdapterListener.refreshAuditList(objects);
    }

    private class AuditFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null) {
                List<AuditEntity> audits = modelManager.getAllAudits(constraint.toString(), sortCriteria);

                results.values = audits;
                results.count = audits.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            final List<AuditEntity> values = (List<AuditEntity>) results.values;

            auditsLiveData.setValue(values != null ? values : emptyList());
//            update(values != null ? values : emptyList());
//            if (results.count == 0) {
//                notifyDataSetChanged();
//            }
        }
    }
}

