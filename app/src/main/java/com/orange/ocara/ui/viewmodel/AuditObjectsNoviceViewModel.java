/*
 * Copyright (c) 2019 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 * distribute, sublicense, create a derivative work, and/or sell copies of the
 * Software in any work that is designed, intended, or marketed for pedagogical or
 * instructional purposes related to programming, coding, application development,
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works,
 * or sale is expressly withheld.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.orange.ocara.ui.viewmodel;

import android.content.Context;
import android.util.Pair;

import androidx.lifecycle.MutableLiveData;

import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.db.ModelManagerImpl_;
import com.orange.ocara.data.cache.model.AuditObjectEntity;
import com.orange.ocara.data.cache.model.QuestionAnswerEntity;
import com.orange.ocara.data.cache.model.ResponseModel;
import com.orange.ocara.data.cache.model.RuleAnswerEntity;
import com.orange.ocara.data.net.model.EquipmentEntity;
import com.orange.ocara.ui.tools.RefreshStrategy;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.UiThread;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import timber.log.Timber;


public class AuditObjectsNoviceViewModel extends AuditObjectsViewModel {


    private int currentQuestionIdx = 0;

    public AuditObjectsNoviceViewModel(Context context, long[] selectedObjects) {
        super(context, selectedObjects);
        computeQuestions();
    }

    List<QuestionAnswerEntity> getQuestionsOfObject(int position) {
        List<QuestionAnswerEntity> questionAnswers = new ArrayList<>();

        AuditObjectEntity selectedAuditObject = modelManager.getAuditObject(selectedObjects[position]);
        modelManager.refresh(selectedAuditObject, RULE_REFRESH_STRATEGY);


        final List<QuestionAnswerEntity> questionAnswers1 = selectedAuditObject.getQuestionAnswers();
        questionAnswers.addAll(questionAnswers1);

        // add all characteristics questions as well
        for (AuditObjectEntity characteristic : selectedAuditObject.getChildren()) {
            questionAnswers.addAll(characteristic.getQuestionAnswers());
        }
        return questionAnswers;
    }

    private void computeQuestions() {
        Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter emitter) throws Exception {
                computeQuestionsBackGround();
                emitter.onComplete();
            }
        }).subscribe(new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                questionsComputed();
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }
        });
    }

    //background
    private List<QuestionAnswerEntity> computeQuestionsBackGround() {

        for (Long id : selectedObjects) {
            AuditObjectEntity auditObject = modelManager.getAuditObject(id);
            System.err.println("OCARA : AUDIT OBJ " + id + " " +auditObject.getName());

            modelManager.refresh(auditObject, RULE_REFRESH_STRATEGY);

            final List<QuestionAnswerEntity> questionAnswers1 = auditObject.getQuestionAnswers();
            System.err.println("OCARA : AUDIT OBJ " + id + " " + " " +auditObject.getName() +questionAnswers1.size());

            for (QuestionAnswerEntity questionAnswer : questionAnswers1) {
                questionAnswer.save();
            }

            // add all characteristics questions as well
            for (AuditObjectEntity characteristic : auditObject.getChildren()) {
                for (QuestionAnswerEntity questionAnswer : characteristic.getQuestionAnswers()) {
                    questionAnswer.save();
                }
            }
        }

        List<QuestionAnswerEntity> questionAnswers;
        questionAnswers = new ArrayList<>();
        for (int i = 0; i < selectedObjects.length; i++) {
            questionAnswers.addAll(getQuestionsOfObject(i));
        }


        Timber.d("computeQuestions Finish");
        allQuestions.clear();
        allQuestions.addAll(questionAnswers);
        return questionAnswers;
    }


    //    @UiThread(propagation = UiThread.Propagation.REUSE)
    void questionsComputed() {

        selectQuestion(currentQuestionIdx);
    }

    /**
     * To select a QuestionAnswer from its index.
     *
     * @param questionIndex question index
     */
    public void selectQuestion(int questionIndex) {
        if (currentQuestionIdx != questionIndex) {
            //TODO if livedata post value
            this.currentQuestionIdx = questionIndex;
        }
//        updatedRules.clear();
        List<QuestionAnswerEntity> questionAnswers = new ArrayList<>();
        if (!allQuestions.isEmpty()) {
            QuestionAnswerEntity questionAnswer = allQuestions.get(currentQuestionIdx);
            questionAnswers.add(questionAnswer);
        }

        for (QuestionAnswerEntity questionAnswer : questionAnswers) {
            // pre-set all rules to 'doubt' value
            ResponseModel response = questionAnswer.getResponse();
            if (ResponseModel.NO_ANSWER.equals(response)) {
                for (RuleAnswerEntity ruleAnswer : questionAnswer.getRuleAnswers()) {
                    updateRuleAnswer(ruleAnswer, ResponseModel.DOUBT);
                }
            }
        }
//        questionListLiveData.getValue().clear();
//        questionListLiveData.getValue().addAll(questionAnswers);

        questionListLiveData.setValue(questionAnswers);
    }


    public AuditObjectEntity getNextAuditObject() {
        if (allQuestions.isEmpty()) return null;

        QuestionAnswerEntity question = allQuestions.get(currentQuestionIdx + 1);
        return getQuestionAuditObject(question);
    }

    @Override
    public AuditObjectEntity getCurrentAuditObject() {
        if (allQuestions.isEmpty()) return null;

        QuestionAnswerEntity question = allQuestions.get(currentQuestionIdx);
        return getQuestionAuditObject(question);
    }

    private AuditObjectEntity getQuestionAuditObject(QuestionAnswerEntity currentQuestionAnswer) {
        AuditObjectEntity questionAnswerAuditObject = currentQuestionAnswer != null ? currentQuestionAnswer.getAuditObject() : null;
        if (questionAnswerAuditObject != null) {
            while (questionAnswerAuditObject.getParent() != null) {
                questionAnswerAuditObject = questionAnswerAuditObject.getParent();
            }
        }
        return questionAnswerAuditObject;
    }


//    @Background
//    void removeQuestionAnswer() {
//
//        updatedRules.clear();
//        List<QuestionAnswerEntity> questionAnswers = new ArrayList<>();
//        if (!allQuestions.isEmpty()) {
//            QuestionAnswerEntity questionAnswer = allQuestions.get(currentQuestionIdx);
//            questionAnswers.add(questionAnswer);
//        }
//
//        for (QuestionAnswerEntity questionAnswer : questionAnswers) {
//            // pre-set all rules to 'doubt' value
//            for (RuleAnswerEntity ruleAnswer : questionAnswer.getRuleAnswers()) {
//                updateRuleAnswer(ruleAnswer, ResponseModel.NO_ANSWER);
//            }
//        }
//
//        finishActivity();
//    }


    public void resetAndSelectAuditObject(AuditObjectEntity auditObject) {

        // reset all ruleAnswer to 'NO_ANSWER'
        List<RuleAnswerEntity> ruleAnswers = auditObject.computeAllRuleAnswers();
        for (RuleAnswerEntity r : ruleAnswers) {
            r.updateResponse(ResponseModel.NO_ANSWER);
        }
        // now select the first question of this object
        int indexOfFirstQuestion = 0;
        while (indexOfFirstQuestion < allQuestions.size()) {
            QuestionAnswerEntity qa = allQuestions.get(indexOfFirstQuestion);
            AuditObjectEntity questionAuditObject = getQuestionAuditObject(qa);
            if (auditObject.equals(questionAuditObject)) {
                break;
            }
            indexOfFirstQuestion++;
        }
        if (indexOfFirstQuestion == allQuestions.size()) {
            indexOfFirstQuestion = 0;
        }
        selectQuestion(indexOfFirstQuestion);
    }

//    public void moveToQuestion(int questionIdx) {
//        currentQuestionIdx = questionIdx;
//    }

//    @Background
//    void saveAuditObject(AuditObjectEntity auditObject) {
//        modelManager.refresh(auditObject, QUESTION_REFRESH_STRATEGY);
//        modelManager.updateAuditObject(auditObject, updatedRules);
//        auditObjectSaved(auditObject);
//    }

    public boolean isLastItemSelected() {
        return (currentQuestionIdx >= (allQuestions.size() - 1));
    }

    public boolean isFirstItemSelected() {
        return (currentQuestionIdx <= 0);
    }


    public int getCurrentQuestionIdx() {
        return currentQuestionIdx;
    }

    public int getNumberOfQuestions() {
        return allQuestions.size();
    }


}
