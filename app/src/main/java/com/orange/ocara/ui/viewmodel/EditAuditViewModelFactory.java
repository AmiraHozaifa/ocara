package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class EditAuditViewModelFactory implements ViewModelProvider.Factory {

    private Long auditId;
    private Context context;

    public EditAuditViewModelFactory(Context context, Long auditId) {
        this.context = context;
        this.auditId = auditId;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new EditAuditViewModel(context, auditId);
    }
}
