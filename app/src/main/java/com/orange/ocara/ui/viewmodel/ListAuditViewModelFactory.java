package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.orange.ocara.data.cache.db.ModelManager;

import java.lang.reflect.InvocationTargetException;

public class ListAuditViewModelFactory implements ViewModelProvider.Factory {

    private Context context;


    public ListAuditViewModelFactory(Context context) {
        this.context = context;
    }


    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (OcaraViewModel.class.isAssignableFrom(modelClass)) {
            try {
                return modelClass.getConstructor(Context.class).newInstance(context);
            } catch (NoSuchMethodException e) {

            } catch (IllegalAccessException e) {

            } catch (InstantiationException e) {

            } catch (InvocationTargetException e) {

            }
        }
        return null;
//        else {
//            throw IllegalStateException("ViewModel must extend MajesticViewModel");
//        }
    }


}