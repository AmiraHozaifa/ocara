package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;

import com.activeandroid.Model;
import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.db.ModelManagerImpl_;
import com.orange.ocara.data.cache.model.AuditEntity;
import com.orange.ocara.data.cache.model.CommentEntity;
import com.orange.ocara.ui.tools.RefreshStrategy;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;

public abstract class ListCommentViewModel extends OcaraViewModel {

    public MutableLiveData<List<CommentEntity>> commentsLiveData;
    protected ModelManager modelManager;
    protected long entityId;

    public ListCommentViewModel(Context context, long entityId) {
        super(context);
        this.entityId = entityId;

        modelManager = ModelManagerImpl_.getInstance_(context);
        commentsLiveData = new MutableLiveData<>();
    }


    public abstract String getEntityName();

    public abstract Long getAuditId();

    public abstract void deleteComment(CommentEntity comment);

    public abstract void deleteAllComments();

    public abstract void updateComment(Long commentId);
    public abstract void loadComments();

}

