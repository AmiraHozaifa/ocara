package com.orange.ocara.ui.viewmodel;

import androidx.lifecycle.ViewModel;

import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.db.ModelManagerImpl;
import com.orange.ocara.data.cache.model.SiteEntity;
import com.orange.ocara.tools.StringUtils;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreateSiteViewModel extends ViewModel {

    private static final Pattern SITE_CODE_POSTAL_PATTERN = Pattern.compile("^(\\d{5}$)");


    public String siteNoImmo = "";
    public String siteName = "";
    public String siteStreet = "";
    public String sitePostalCode = "";
    public String siteCity = "";

    ModelManager modelManager;
    SiteEntity site;

    public CreateSiteViewModel(ModelManager modelManager) {
        this.modelManager = modelManager;
    }

    protected boolean checkSitePostalCode(String sitePostalCode) {
        if (!StringUtils.isBlank(sitePostalCode)) {
            Matcher matcher = SITE_CODE_POSTAL_PATTERN.matcher(sitePostalCode);
            return matcher.matches();
        } else {
            return true;
        }
    }

    protected String getSiteName() {
        return StringUtils.trim(siteName);
    }

    protected String getSiteStreet() {
        return StringUtils.trim(siteStreet);
    }

    protected String getSitePostalCode() {
        return StringUtils.trim(sitePostalCode);
    }

    protected String getSiteCity() {
        return StringUtils.trim(siteCity);
    }


    protected String getSiteNoImmo() {
        return StringUtils.trim(siteNoImmo);
    }


//    public void setSiteNoImmo(String noImmo) {
//        this.siteNoImmo = noImmo;4
////        notifyPropertyChanged(BR.celsius);
//    }

    private boolean checkSiteNoImmo() {
        return !modelManager.checkSiteExistenceByNoImmo(getSiteNoImmo());
    }

    private boolean checkSiteName() {
        return !modelManager.checkSiteExistenceByName(getSiteName());
    }

    /**
     * Checks siteCompleteView fields.
     *
     * @return true if siteCompleteView mandatory fields are set, false otherwise
     */
    public boolean checkSiteFields() {
        return !(StringUtils.isBlank(getSiteNoImmo()) || StringUtils.isBlank(getSiteName())) && (getSitePostalCode().isEmpty() || checkSitePostalCode(getSitePostalCode()));
    }

    public Long validateSite() {
        if (!checkSiteFields()) {
            return null;
        }

        if (!checkSiteNoImmo()) {
//            displayErrorBox(com.orange.ocara.R.string.create_site_error_title, com.orange.ocara.R.string.create_site_duplicate_site_noImmo);
//            this.siteNoImmo.requestFocus();
            return null;
        }

        if (!checkSiteName()) {
//            displayErrorBox(com.orange.ocara.R.string.create_site_error_title, com.orange.ocara.R.string.create_site_duplicate_site_name);
//            this.siteName.requestFocus();
            return null;
        }

        site = new SiteEntity();
        site.setNoImmo(getSiteNoImmo());
        site.setName(getSiteName());
        site.setStreet(getSiteStreet());
        if (!getSitePostalCode().isEmpty()) {
            site.setCode(Integer.parseInt(getSitePostalCode()));
        }
        site.setCity(getSiteCity());
        site.save();

        return site.getId();
    }


}
