package com.orange.ocara.ui.fragment;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.orange.ocara.R;
import com.orange.ocara.business.binding.ErrorBundle;
import com.orange.ocara.business.interactor.AcceptTermsOfUseTask;
import com.orange.ocara.business.interactor.DeclineTermsOfUseTask;
import com.orange.ocara.business.interactor.UseCase;
import com.orange.ocara.ui.viewmodel.OcaraViewModelFactory;
import com.orange.ocara.ui.viewmodel.TermsOfUseSelectionViewModel;

import static android.widget.Toast.LENGTH_SHORT;
import static android.widget.Toast.makeText;

public class TermsOfUseSelectionActivity extends AppCompatActivity {
    TermsOfUseSelectionViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_terms_select);
        viewModel = new ViewModelProvider(this, new OcaraViewModelFactory(this))
                .get(TermsOfUseSelectionViewModel.class);

        findViewById(R.id.terms_of_use_refuse_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refuseTerms();
            }
        });
        findViewById(R.id.terms_of_use_accept_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptTerms();
            }
        });

        handleFoldables();

    }

    private void handleFoldables() {
        findViewById(R.id.gen_info_title_txt).setOnClickListener(new onTextClickListener(findViewById(R.id.gen_info_content_txt)));
        findViewById(R.id.item1_title_txt).setOnClickListener(new onTextClickListener(findViewById(R.id.item1_content_txt)));
        findViewById(R.id.item2_title_txt).setOnClickListener(new onTextClickListener(findViewById(R.id.item2_content_txt)));
        findViewById(R.id.item3_title_txt).setOnClickListener(new onTextClickListener(findViewById(R.id.item3_content_txt)));
        findViewById(R.id.item4_title_txt).setOnClickListener(new onTextClickListener(findViewById(R.id.item4_content_txt)));
        findViewById(R.id.item5_title_txt).setOnClickListener(new onTextClickListener(findViewById(R.id.item5_content_txt)));
        findViewById(R.id.item6_title_txt).setOnClickListener(new onTextClickListener(findViewById(R.id.item6_content_txt)));
        findViewById(R.id.item7_title_txt).setOnClickListener(new onTextClickListener(findViewById(R.id.item7_content_txt)));
        findViewById(R.id.item8_title_txt).setOnClickListener(new onTextClickListener(findViewById(R.id.item8_content_txt)));
        findViewById(R.id.item9_title_txt).setOnClickListener(new onTextClickListener(findViewById(R.id.item9_content_txt)));
        findViewById(R.id.item10_title_txt).setOnClickListener(new onTextClickListener(findViewById(R.id.item10_content_txt)));
        findViewById(R.id.item11_title_txt).setOnClickListener(new onTextClickListener(findViewById(R.id.item11_content_txt)));

    }

    class onTextClickListener implements View.OnClickListener {
        View viewToToggleVisibility;

        public onTextClickListener(View viewToToggleVisibility) {
            this.viewToToggleVisibility = viewToToggleVisibility;
        }

        @Override
        public void onClick(View v) {
            if (viewToToggleVisibility.getVisibility() == View.VISIBLE) {
                viewToToggleVisibility.setVisibility(View.GONE);
                ((TextView) v).setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_chevron_right_small, 0);
            } else {
                viewToToggleVisibility.setVisibility(View.VISIBLE);
                ((TextView) v).setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_chevron_down_small, 0);
            }

        }
    }

    private void refuseTerms() {
        viewModel.declineTerms(new UseCase.UseCaseCallback<DeclineTermsOfUseTask.DeclineTermsOfUseResponse>() {
            @Override
            public void onComplete(DeclineTermsOfUseTask.DeclineTermsOfUseResponse declineTermsOfUseResponse) {
                showTermsDeclined();
            }

            @Override
            public void onError(ErrorBundle errors) {
                showError(errors.getMessage());
            }
        });
    }

    private void acceptTerms() {
        viewModel.acceptTerms(new UseCase.UseCaseCallback<AcceptTermsOfUseTask.AcceptTermsOfUseResponse>() {
            @Override
            public void onComplete(AcceptTermsOfUseTask.AcceptTermsOfUseResponse acceptTermsOfUseResponse) {
                showTermsAccepted();
            }

            @Override
            public void onError(ErrorBundle errors) {
                showError(errors.getMessage());
            }
        });
    }

    public void showError(String message) {
        makeText(this, R.string.terms_error, LENGTH_SHORT)
                .show();
    }

    public void showTermsAccepted() {
        makeText(this, R.string.terms_accepted, LENGTH_SHORT)
                .show();
        setResult(RESULT_OK);
        finish();
    }

    public void showTermsDeclined() {

        makeText(this, R.string.terms_declined, LENGTH_SHORT)
                .show();

        // Terms refused. We leave the app.
        setResult(-10);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAndRemoveTask();
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            finishAffinity();
        } else {
            finish();
        }
    }

}
