/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import com.orange.ocara.R;
import com.orange.ocara.business.BizConfig;
import com.orange.ocara.business.binding.ErrorBundle;
import com.orange.ocara.business.interactor.CheckTermsOfUseTask;
import com.orange.ocara.business.interactor.UseCase.UseCaseCallback;
import com.orange.ocara.business.model.RuleSetStat;
import com.orange.ocara.business.model.RulesetModel;
import com.orange.ocara.business.model.UserModel;
import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.db.ModelManagerImpl;
import com.orange.ocara.data.cache.model.AuditEntity;
import com.orange.ocara.data.cache.model.AuditorEntity;
import com.orange.ocara.data.cache.model.SiteEntity;
import com.orange.ocara.data.cache.model.SortCriteria;
import com.orange.ocara.data.net.model.Ruleset;
import com.orange.ocara.databinding.FragmentCreateAuditBinding;
import com.orange.ocara.ui.CreateAuditUiConfig;
import com.orange.ocara.ui.TermsCheckingUiConfig;
import com.orange.ocara.ui.adapter.AuditorItemListAdapter;
import com.orange.ocara.ui.adapter.RulesetStatusAdapter;
import com.orange.ocara.ui.adapter.SiteItemListAdapter;
import com.orange.ocara.ui.contract.CreateAuditContract;
import com.orange.ocara.ui.contract.TermsOfUseDisplayContract;
import com.orange.ocara.ui.dialog.OcaraDialogBuilder;
import com.orange.ocara.ui.model.AuditFormUiModel;
import com.orange.ocara.ui.view.Switch;
import com.orange.ocara.ui.viewmodel.CreateAuditViewModel;
import com.orange.ocara.ui.viewmodel.CreateAuditViewModelFactory;
import com.orange.ocara.ui.widget.AuditorAutoCompleteView;
import com.orange.ocara.ui.widget.SiteAutoCompleteView;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.UiThread;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.CompletableObserver;
import io.reactivex.disposables.Disposable;
import lombok.RequiredArgsConstructor;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.LENGTH_SHORT;
import static android.widget.Toast.makeText;
import static androidx.navigation.Navigation.findNavController;
import static com.microsoft.appcenter.utils.HandlerUtils.runOnUiThread;
import static com.orange.ocara.business.interactor.CheckTermsOfUseTask.CheckTermsOfUseResponse;
import static com.orange.ocara.tools.StringUtils.isBlank;
import static com.orange.ocara.tools.StringUtils.trim;
import static com.orange.ocara.ui.widget.SiteAutoCompleteView.format;
import static timber.log.Timber.d;
import static timber.log.Timber.e;
import static timber.log.Timber.i;
import static timber.log.Timber.v;
import static timber.log.Timber.w;

/**
 * View that displays a form which aims on creating an audit
 */
@EFragment(R.layout.fragment_create_audit)
public class CreateAuditFragment
        extends BaseFragment
        implements AuditFormUiModel, TermsOfUseDisplayContract.TermsOfUseDisplayView {

    @Bean(BizConfig.class)
    BizConfig bizConfig;

    @Bean(TermsCheckingUiConfig.class)
    TermsCheckingUiConfig termsUiConfig;

    @Bean(ModelManagerImpl.class)
    ModelManager modelManager;


    //    // adapters for auto-complete
    @Bean
    SiteItemListAdapter siteItemListAdapter;
    @Bean
    AuditorItemListAdapter authorItemListAdapter;


    FragmentCreateAuditBinding binding;
    CreateAuditViewModel createAuditViewModel;

    //    RulesetModel selectedRuleset;
    public static final Pattern AUTHOR_PATTERN = Pattern.compile("^.{3,}+$");
    private static final int CREATE_SITE_REQUEST_CODE = 1;

    protected TermsOfUseDisplayContract.TermsOfUseCheckingUserActionsListener touActionsListener;

    //    CreateAuditUiConfig uiConfig = new CreateAuditUiConfig();
    //
//    private CreateAuditContract.CreateAuditUserActionsListener actionsListener;
    //
    private RulesetStatusAdapter rulesetStatusAdapter;

    private SiteEntity currentSite = null;

    private ProgressDialog mInitPageDialog;

    @AfterInject
    void afterInject() {
        System.err.println("OCARA CREATE AUDIT FRAGMENT AFTER INJECT");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    Observer<List<RulesetModel>> ruleSetObserver = new Observer<List<RulesetModel>>() {
        @Override
        public void onChanged(List<RulesetModel> rulesetModels) {
            hideProgressDialog();
            if (rulesetStatusAdapter == null) {
                showRulesetList(rulesetModels);
                enableRulesInfo();
            } else {
//                rulesetStatusAdapter.setRulesets(rulesetModels);
//                rulesetStatusAdapter.notifyDataSetChanged();
                showRulesetList(rulesetModels);
                enableRulesInfo();
                binding.rulesetType.setSelection(rulesetStatusAdapter.getPosition(createAuditViewModel.getSelectedRulSet()));
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_create_audit, container, false);
        View rootView = binding.getRoot();

        System.err.println("OCARA : AUDIT onCreateView");
//        ViewModelProvider provider = ;
        createAuditViewModel = new ViewModelProvider(this, new CreateAuditViewModelFactory(getContext())).get(CreateAuditViewModel.class);
        createAuditViewModel.rulesLiveData.observe(getViewLifecycleOwner(), ruleSetObserver);

        binding.setCreateAuditViewModel(createAuditViewModel);
        binding.editAuditLayout.setOnClickListener(view -> hideSoftKeyboard());
        binding.rulesInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                RulesetModel selectedRuleset = rulesetStatusAdapter.getItem(binding.rulesetType.getSelectedItemPosition());
//                RulesetInfoActivity_
//                        .intent(getActivity())
//                        .ruleset(selectedRuleset)
//                        .rulesetUpgradable(true)
//                        .start();
//                Toast.makeText(CreateAuditFragment.this.getActivity(), "Hell", LENGTH_LONG).show();
                navigateToRuleSet(createAuditViewModel.getSelectedRulSet());
            }
        });

        if (!createAuditViewModel.hasRules()) {
            showProgressDialog();
        }
        createAuditViewModel.retrieveRuleSet();

        return rootView;

    }

    private void navigateToRuleSet(RulesetModel selectedRuleset) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("ruleSet", selectedRuleset);
        bundle.putBoolean("ruleSetUpgradable", true);
        NavHostFragment.findNavController(this).navigate(R.id.action_ruleset_info, bundle);

    }

    public void initTermsOfUse() {
        d("ActivityMessage=Initializing the terms of use");

        touActionsListener = termsUiConfig.readingListener(this);
        touActionsListener.checkTerms(new CreateAuditActivityCheckTermsOfUseCallback(this));

    }


    @Override
    public void onResume() {
        super.onResume();

        initAuthor();
        initSite();
        initAuditType();
        initLevel();
//        initTermsOfUse();
//        showProgressDialog();
//        if (createAuditViewModel.hasRules()) {
//            showRulesetList(createAuditViewModel.getRules());
//            enableRulesInfo();
//        } else
//            showRuleset();
    }

    private void resetUi() {
        binding.authorCompleteView.setText("");
        binding.site.setText("");
        binding.auditName.setText("");
        binding.level.setChecked(true);
    }

    private void initAuthor() {
        authorItemListAdapter.setModelManager(modelManager);
        binding.authorCompleteView.setAdapter(authorItemListAdapter);
    }

    private void initSite() {
        d("Setting up the siteCompleteView");
        siteItemListAdapter.setModelManager(modelManager);
        binding.site.setAdapter(siteItemListAdapter);
        binding.site.setThreshold(1);
        binding.site.setValidator(new AutoCompleteTextView.Validator() {
            @Override
            public boolean isValid(CharSequence text) {
                v("Check is Valid : " + text + " current " + format(currentSite).toString());
                return text.toString().equals(format(currentSite).toString());
            }

            @Override
            public CharSequence fixText(CharSequence invalidText) {
                v("Returning fixed text");
                displayErrorBox(getString(R.string.edit_audit_site_unknown_title), getString(R.string.edit_audit_site_unknown));
                if (createAuditViewModel.audit != null && createAuditViewModel.audit.getSite() != null) {
                    setSite(createAuditViewModel.audit.getSite());
                    return format(createAuditViewModel.audit.getSite()).toString();
                } else {
                    return "";
                }
            }
        });

        binding.site.setOnFocusChangeListener((v, hasFocus) -> {
            if (v.equals(binding.site) && !hasFocus && v instanceof AutoCompleteTextView) {
                ((AutoCompleteTextView) v).performValidation();
            }
        });

        NavController navController = NavHostFragment.findNavController(this);
        // We use a String here, but any type that can be put in a Bundle is supported
        MutableLiveData liveData = navController.getCurrentBackStackEntry()
                .getSavedStateHandle()
                .getLiveData("siteId");
        liveData.observe(getViewLifecycleOwner(), new Observer() {
            @Override
            public void onChanged(Object res) {
                SiteEntity siteFromLiveData = modelManager.getSite((Long) res);
                if (siteFromLiveData == null) return;
                currentSite = siteFromLiveData;
                setSite(currentSite);
                binding.site.requestFocus();

            }
        });

        binding.createSite.setOnClickListener(v -> {
            findNavController(v).navigate(CreateAuditFragment_Directions.actionCreateSite());

        });
    }

    private void initAuditType() {
        d("Setting up the audit type");
        //init Adapter ruleset Info
//        showProgressDialog();
        initRuleset();

        binding.rulesetType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                runOnUiThread(() -> {
                    d("ActivityMessage=setting preferred ruleset");
//                    actionsListener.savePreferredRuleset(rulesetStatusAdapter.getItem(position));
//                    createAuditViewModel.setSelectedRulSet(binding.rulesetType.getSelectedItemPosition());
                    savePreferredRuleset(rulesetStatusAdapter.getItem(position));
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // do nothing yet
            }
        });
    }

    void savePreferredRuleset(RulesetModel ruleset) {
//        createAuditViewModel.setSelectedRulSet(ruleset);
        createAuditViewModel.savePreferredRuleset(ruleset).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onComplete() {
                hideProgressDialog();
                validateFields();
            }

            @Override
            public void onError(Throwable e) {
                hideProgressDialog();
            }
        });
    }

    private void initLevel() {

        binding.level.setChecked(createAuditViewModel.audit == null || AuditEntity.Level.EXPERT.equals(createAuditViewModel.audit.getLevel()));
        binding.level.requestFocus();
        binding.level.setOnCheckedChangeListener((compoundButton, b) -> validateAudit());
    }

    private void validateAudit() {
        if (createAuditViewModel.validateAudit(this)) {
            enableSaveButton();
        } else disableSaveButton();
    }

    private void initRuleset() {
        disableRulesInfo();
    }

    @Override
    public RulesetModel getActualRuleset() {
        if (rulesetStatusAdapter == null)
            return null;
        return rulesetStatusAdapter.getItem(binding.rulesetType.getSelectedItemPosition());
    }


    @TextChange(R.id.author_complete_view)
    void onAuthorTextChange(CharSequence text) {
        validateAudit();
        authorItemListAdapter.getFilter().filter(text);
    }

    @TextChange(R.id.site)
    void onSiteTextChange(CharSequence text) {
        final String[] split = text.toString().split(" -- ");
        d("ActivityMessage=on siteCompleteView text change;Text=%s", text.toString());
        if (split.length == 2
                && modelManager.checkSiteExistenceByNoImmo(split[0].trim())
                && modelManager.checkSiteExistenceByName(split[1].trim())) {
            currentSite = modelManager.searchSites(split[1].trim()).get(0);
            validateAudit();
            siteItemListAdapter.getFilter().filter(text);
            validateFields();
        } else {
            disableSaveButton();
        }
    }

    //    @Override
    public void showRulesetList(List<RulesetModel> rulesets) {
        rulesetStatusAdapter = new RulesetStatusAdapter(getActivity(), rulesets);
        initRuleSetInfoAdapter();
        if (!rulesets.isEmpty()) {
            showRulesetIcons();
        } else {
            hideRulesetIcons();
        }
    }

//    @UiThread
//    void initPosition(final RulesetModel item) {
//        if (item != null) {
//            final int position = rulesetStatusAdapter.getPosition(item);
//            binding.rulesetType.setSelection(position);
//        }
//    }

    //    @Override
    @UiThread
    public void showRulesetIcons() {
        binding.rulesInfo.setVisibility(VISIBLE);
    }

    //    @Override
    @UiThread
    public void hideRulesetIcons() {
        binding.rulesInfo.setVisibility(INVISIBLE);
    }

    @UiThread
    void initRuleSetInfoAdapter() {
        binding.rulesetType.setAdapter(rulesetStatusAdapter);
        if (mInitPageDialog != null) {
            mInitPageDialog.dismiss();
        }
    }

    @TextChange(R.id.name)
    void onNameTextChange() {
        validateAudit();
    }

//    @Click(R.id.rules_info)
//    void showRulesInfoClicked() {
//        d("Show RulesetInfoActivity...");
//        final RulesetModel selectedRuleset = rulesetStatusAdapter.getItem(binding.rulesetType.getSelectedItemPosition());
//        RulesetInfoActivity_
//                .intent(this)
//                .ruleset(selectedRuleset)
//                .rulesetUpgradable(true)
//                .start();


//        CreateAuditFragment_Directions.ActionRulesetInfo action = CreateAuditFragment_Directions.actionRulesetInfo();
//        action.setRuleSet(selectedRuleset);
//        action.setRuleSetUpgradable(true);
//        Navigation.findNavController(view).navigate(action);
//    }

    @OnActivityResult(CREATE_SITE_REQUEST_CODE)
    void onResult(int resultCode, @OnActivityResult.Extra(value = "siteId") Long siteId) {
        d("On activity result, we do some stuff...");
        if (resultCode == Activity.RESULT_OK) {
            v("Site accepted");
            currentSite = modelManager.getSite(siteId);
            setSite(currentSite);
            binding.site.requestFocus();
        }
    }

    private AuditorEntity makeAuthor() {
        AuditorEntity newAuthor = new AuditorEntity();

        Matcher matcher = AUTHOR_PATTERN.matcher(getUserName());
        if (matcher.find()) {
            newAuthor.setUserName(trim(matcher.group(0)));
        }

        return newAuthor;
    }

    @Override
    public UserModel getActualAuthor() {
        return AuditorEntity
                .builder()
                .userName(getUserName())
                .build();
    }

    @Override
    public SiteEntity getActualSite() {
        return currentSite;
    }

    private void setSite(SiteEntity site) {
        binding.site.setCurrentSite(site);
        createAuditViewModel.currentSite = site;
//        binding.site.setText(site.getName());
        currentSite = site; // Order is very important because setCurrentSite will call textWatcher which will set currentSite to null
    }

    private String getName() {
        return trim(binding.auditName.getText().toString());
    }

    private String getUserName() {
        return trim(binding.authorCompleteView.getText().toString());
    }

    public RulesetModel getRuleSet() {
        final int selectedItemPosition = binding.rulesetType.getSelectedItemPosition();
        if (rulesetStatusAdapter == null || selectedItemPosition == rulesetStatusAdapter.getCount() || selectedItemPosition < 0) {
            return null;
        }

        return rulesetStatusAdapter.getItem(selectedItemPosition);
    }

    @Click(R.id.start_audit_button)
    void onAuditStartClicked() {
        i("ActivityMessage=Start button has been clicked...");
        acceptTerms();
//        touActionsListener.checkTerms(new CreateAuditActivityCheckTermsOfUseCallback(this));
    }

    /**
     * Check if fields are valid and deals with consequences by modifying the view elements
     */
    public void validateFields() {
        if (!checkAuditFields()) {
            disableSaveButton();
        } else {
            enableSaveButton();
        }
    }

    private boolean auditIsValid() {
        if (!checkAuditFields()) {
            return false;
        }

        if (!isAuditNameAndVersionValid()) {
            displayErrorBox(getString(R.string.edit_audit_error_title), getString(R.string.edit_audit_duplicate_audit));
            binding.auditName.requestFocus();
            return false;
        }

        return true;
    }

    private boolean isAuditNameAndVersionValid() {
        final String oldAuditName = createAuditViewModel.audit.getName();
        final String newAuditName = getName();
        final SiteEntity oldSite = createAuditViewModel.audit.getSite();
        final SiteEntity newSite = currentSite;

        // Check that name changed
        if ((oldAuditName != null && oldAuditName.equalsIgnoreCase(newAuditName)) &&
                oldSite.equals(newSite)) {
            return true;
        }

        return !modelManager.checkAuditExistenceByNameAndVersion(newAuditName, newSite, createAuditViewModel.audit.getVersion());
    }

    /**
     * Checks audit fields.
     *
     * @return true if audit mandatory fields are set, false otherwise
     */
    private boolean checkAuditFields() {
        boolean userNameIsValid = AUTHOR_PATTERN.matcher(getUserName()).find();
        boolean siteIsNotNull = currentSite != null;
        boolean rulesetIsValid = getRuleSet() != null && RuleSetStat.OFFLINE.equals(getRuleSet().getStat());
        boolean auditNameIsValid = !isBlank(getName());

        return userNameIsValid && siteIsNotNull && rulesetIsValid && auditNameIsValid;
    }


    @Override
    public AuditEntity getActualAudit() {
        return createAuditViewModel.audit;
    }

    /**
     * mandatory function. Inherits from {@link BaseActivity}
     * @return always false
     */
//    @Override
//    protected boolean isChildActivity() {
//        return false;
//    }

    /**
     * Convenient function, so as to use the UI Thread
     *
     * @param audit an {@link AuditEntity}
     */
    @Background
    void createAudit(AuditEntity audit) {
        i("ActivityMessage=Start audit creation... ");
        AuditEntity newAudit = modelManager.createAudit(getRuleSet(), audit.getSite(), audit.getName(), audit.getAuthor(), audit.getLevel());
        i("ActivityMessage=Audit created;AuditName=%s;AuditId=%d", newAudit.getName(), newAudit.getId());
        auditCreated(newAudit);
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    void auditCreated(AuditEntity audit) {
        i("ActivityMessage=Audit has been created. Starting Setup...");
        createAuditViewModel.audit = audit;
//        SetupAuditPathActivity_
//                .intent(this)
//                .fromListAudit(false)
//                .flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                .auditId(audit.getId())
//                .start();

        Bundle bundle = new Bundle();
        bundle.putLong("auditId", audit.getId());
        bundle.putBoolean("fromListAudit", false);

        NavHostFragment.findNavController(this).navigate(R.id.action_setup_path, bundle);
        resetUi();
    }

    @LongClick(R.id.rules_info)
    public void showRulesInfoLabel() {
        makeText(getActivity(), R.string.edit_audit_show_rules_information, LENGTH_SHORT).show();
    }

    //    @Override
    @UiThread
    public void showDownloadError() {
        makeText(getActivity(), "Erreur de téléchargement", LENGTH_LONG).show();
    }

    /**
     * hides a {@link ProgressDialog}, if it exists
     */
//    @Override
    public void hideProgressDialog() {
        if (mInitPageDialog != null) {
            mInitPageDialog.dismiss();
        }
    }

    /**
     * displays a {@link ProgressDialog}
     */
//    @Override
    @UiThread
    public void showProgressDialog() {
        mInitPageDialog = new ProgressDialog(getActivity());
        mInitPageDialog.setMessage(getString(R.string.init_page));
        mInitPageDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mInitPageDialog.setIndeterminate(true);
        mInitPageDialog.setCanceledOnTouchOutside(false);
        mInitPageDialog.setCancelable(false);
        mInitPageDialog.show();
    }

    //    @Override
    public void disableRulesInfo() {
        binding.rulesInfo.setEnabled(false);
    }

    @UiThread
//    @Override
    public void enableRulesInfo() {
        binding.rulesInfo.setEnabled(true);
    }


//    @Background
//    void showRuleset() {
//        d("ActivityMessage=retrieving the rulesets list");
////        actionsListener.loadRulesets();
//        showProgressDialog();
////        try {
////            Thread.sleep(3000);
////        } catch (Exception e) {
////
////        }
//        createAuditViewModel.retrieveRulesetsObservable.subscribe(new io.reactivex.Observer<List<RulesetModel>>() {
//            @Override
//            public void onSubscribe(Disposable d) {
//
//            }
//
//            @Override
//            public void onNext(List<RulesetModel> rules) {
//                showRulesetList(rules);
//                enableRulesInfo();
//                hideProgressDialog();
//            }
//
//            @Override
//            public void onError(Throwable e) {
//                hideProgressDialog();
//                showDownloadError();
//
//            }
//
//            @Override
//            public void onComplete() {
//
//            }
//        });
//    }

    //    @Override
    public void enableSaveButton() {
        binding.startAuditButton.setEnabled(true);
    }

    //    @Override
    public void disableSaveButton() {
        binding.startAuditButton.setEnabled(false);
    }

    @Override
    public AuditEntity getInitialAudit() {
        // we are creating new audits, so there is no initial audit to compare with.
        // maybe this function is not necessary...
        return null;
    }

    /**
     * implements {@link TermsOfUseDisplayContract.TermsOfUseDisplayView#showTerms()}
     */
//    @Override
    public void acceptTerms() {

        if (auditIsValid()) {
            i("ActivityMessage=Validation succeeded, after start button has been clicked...");
            createAuditViewModel.audit.setAuthor(makeAuthor());
            createAuditViewModel.audit.setSite(currentSite);

            final String referenceString = getString(R.string.audit_item_name_format, createAuditViewModel.audit.getName(), createAuditViewModel.audit.getVersion());
            String currentAuditName = getName();
            if (!currentAuditName.equals(referenceString)) {
                createAuditViewModel.audit.setName(currentAuditName);
            }
            createAuditViewModel.audit.setLevel(binding.level.isChecked() ? AuditEntity.Level.EXPERT : AuditEntity.Level.BEGINNER);
            createAudit(createAuditViewModel.audit);
        } else {
            w("ActivityMessage=Validation failed after start button has been clicked.");
        }
    }

    @Override
    public void showTerms() {
        d("ActivityMessage=Showing terms");
//        TermsOfUseAcceptanceFragment_
//                .intent(getActivity())
//                .start();
        NavHostFragment.findNavController(this).navigate(R.id.action_show_terms);

    }

    @Override
    public void showError(String message) {
        makeText(getActivity().getApplicationContext(), "Error while handling terms", LENGTH_SHORT)
                .show();
    }

    /**
     * To display an error box.
     *
     * @param title   title
     * @param message message
     */
    private AlertDialog errorDialog = null;

    void displayErrorBox(String title, String message) {

        if (errorDialog != null && errorDialog.isShowing()) {
            errorDialog.dismiss();
        }

        errorDialog = new OcaraDialogBuilder(getActivity())
                .setTitle(title) // title
                .setMessage(message) // message
                .setPositiveButton(R.string.action_ok, null)
                .create();

        errorDialog.show();
    }

    /**
     * Callback class for {@link CheckTermsOfUseTask} dedicated to the current activity
     */
    @RequiredArgsConstructor
    public static class CreateAuditActivityCheckTermsOfUseCallback implements UseCaseCallback<CheckTermsOfUseResponse> {

        private final CreateAuditFragment view;

        @Override
        public void onComplete(CheckTermsOfUseResponse terms) {

            if (terms.isAccepted()) {
                d("Message=Terms already checked and accepted");
                view.acceptTerms();
            } else {
                d("Message=Terms not checked yet, or already refused");
                view.showTerms();
            }
        }

        @Override
        public void onError(ErrorBundle errors) {
            e(errors.getCause(), "Message=Item upgrading failed;Error=%s", errors.getMessage());
            view.showError(errors.getMessage());
        }
    }
}
