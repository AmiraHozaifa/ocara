package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import com.activeandroid.Model;
import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.db.ModelManagerImpl_;
import com.orange.ocara.data.cache.model.AuditEntity;
import com.orange.ocara.data.cache.model.CommentEntity;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;

public class EditCommentViewModel extends OcaraViewModel {


    ModelManager modelManager;

    public EditCommentViewModel(Context context) {
        super(context);
        modelManager = ModelManagerImpl_.getInstance_(context);
    }

    public Single<String> getAuditName(long auditId) {

        return Single.create(new SingleOnSubscribe<String>() {
            @Override
            public void subscribe(SingleEmitter<String> emitter) throws Exception {

                emitter.onSuccess(modelManager.getAudit(auditId).getName());
            }
        });

    }

    public CommentEntity getComment(long commentId) {
        return Model.load(CommentEntity.class, commentId);

    }
//    public Single<CommentEntity> getComment(long commentId) {

//        return Single.create(new SingleOnSubscribe<CommentEntity>() {
//            @Override
//            public void subscribe(SingleEmitter<CommentEntity> emitter) throws Exception {
//
//                emitter.onSuccess(Model.load(CommentEntity.class, commentId));
//            }
//        });

//    }
}
