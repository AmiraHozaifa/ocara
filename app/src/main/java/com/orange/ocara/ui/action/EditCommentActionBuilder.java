package com.orange.ocara.ui.action;

import android.content.Context;
import android.os.Bundle;
import android.util.Pair;

import com.orange.ocara.R;
import com.orange.ocara.data.cache.model.AuditEntity;
import com.orange.ocara.data.cache.model.CommentEntity;
import com.orange.ocara.ui.activity.EditCommentAudioActivity_;
import com.orange.ocara.ui.activity.EditCommentFileActivity_;
import com.orange.ocara.ui.activity.EditCommentPhotoActivity_;
import com.orange.ocara.ui.activity.EditCommentTextActivity_;
import com.orange.ocara.ui.intent.EditCommentIntentBuilder;

import org.androidannotations.api.builder.ActivityIntentBuilder;

public class EditCommentActionBuilder {


    /**
     * a context
     */
    private final Context context;

    /**
     * a {@link CommentEntity.Type}
     */
    private final CommentEntity.Type commentType;

    /**
     * a location for the file linked to the comment
     */
    private final String attachmentDirectory;

    /**
     * an identifier for a {@link CommentEntity}
     */
    private Long commentId = null;

    /**
     * an identifier for an {@link AuditEntity}
     */
    private Long auditId = null;

    /**
     * a {@link String}
     */
    private String title = null;

    /**
     * a {@link String}
     */
    private String subtitle = null;

    /**
     * Instantiates
     *
     * @param context   a {@link Context}
     * @param type      a {@link CommentEntity.Type}
     * @param directory a location for files
     */
    public EditCommentActionBuilder(Context context, CommentEntity.Type type, String directory) {
        this.context = context;
        this.commentType = type;
        this.attachmentDirectory = directory;
    }

    /**
     * @param commentId a {@link Long}
     * @return an instance of {@link EditCommentIntentBuilder}
     */
    public EditCommentActionBuilder commentId(Long commentId) {
        this.commentId = commentId;
        return this;
    }

    /**
     * @param auditId a {@link Long}
     * @return an instance of {@link EditCommentIntentBuilder}
     */
    public EditCommentActionBuilder auditId(Long auditId) {
        this.auditId = auditId;
        return this;
    }

    /**
     * @param title a {@link String} that replaces the default title of the view
     * @return an instance of {@link EditCommentIntentBuilder}
     */
    public EditCommentActionBuilder title(String title) {
        this.title = title;
        return this;
    }

    /**
     * @param subtitle a {@link String} that replaces the default subtitle of the view
     * @return an instance of {@link EditCommentIntentBuilder}
     */
    public EditCommentActionBuilder subtitle(String subtitle) {
        this.subtitle = subtitle;
        return this;
    }

    /**
     * Builds and displays the {@link android.content.Intent}
     * <p>
     * //     * @param resultCode an {@link Integer}
     */
    public Pair<Integer, Bundle> getAction() {

//        ActivityIntentBuilder builder;
        Integer action = -1;
        Bundle bundle = null;
        switch (commentType) {
            case TEXT:
                action = R.id.action_edit_comment_txt;
                bundle = new Bundle();
                if (commentId != null) bundle.putLong("commentId", commentId);
                bundle.putInt("commentType", CommentEntity.getIntFromTypeFrom(CommentEntity.Type.TEXT));
                if (attachmentDirectory != null)
                    bundle.putString("attachmentDirectory", attachmentDirectory);
                if (auditId != null) bundle.putLong("auditId", auditId);
//                EditCommentTextActivity_.IntentBuilder_ txtTmpBuilder = EditCommentTextActivity_
//                        .intent(context)
//                        .commentId(commentId)
//                        .commentType(CommentEntity.Type.TEXT)
//                        .attachmentDirectory(attachmentDirectory)
//                        .auditId(auditId);

                if (title != null) {
//                    txtTmpBuilder.title(title);
                    bundle.putString("title", title);
                }

                if (subtitle != null) {
//                    txtTmpBuilder.subTitle(subtitle);
                    bundle.putString("subtitle", subtitle);
                }

//                builder = txtTmpBuilder;
                break;
            case PHOTO:
                action = R.id.action_edit_comment_photo;
                bundle = new Bundle();
                if (commentId != null) bundle.putLong("commentId", commentId);
                bundle.putInt("commentType", CommentEntity.getIntFromTypeFrom(CommentEntity.Type.PHOTO));
                if (attachmentDirectory != null)
                    bundle.putString("attachmentDirectory", attachmentDirectory);
                if (auditId != null) bundle.putLong("auditId", auditId);
                if (title != null) bundle.putString("title", title);
                if (subtitle != null) bundle.putString("subtitle", subtitle);

//                EditCommentPhotoActivity_.IntentBuilder_ photoTmpBuilder = EditCommentPhotoActivity_
//                        .intent(context)
//                        .commentId(commentId)
//                        .commentType(CommentEntity.Type.PHOTO)
//                        .attachmentDirectory(attachmentDirectory)
//                        .auditId(auditId)
//                        .title(title)
//                        .subTitle(subtitle);

                if (title != null) {
                    bundle.putString("title", title);
//                    photoTmpBuilder.title(title);
                }

                if (subtitle != null) {
                    bundle.putString("title", title);
//                    photoTmpBuilder.subTitle(subtitle);
                }

//                builder = photoTmpBuilder;
                break;
            case AUDIO:
                action = R.id.action_edit_comment_audio;
                bundle = new Bundle();
                if (commentId != null) bundle.putLong("commentId", commentId);
                bundle.putSerializable("commentType", CommentEntity.getIntFromTypeFrom(CommentEntity.Type.AUDIO));
                if (attachmentDirectory != null)
                    bundle.putString("attachmentDirectory", attachmentDirectory);
                if (auditId != null) bundle.putLong("auditId", auditId);
                if (title != null) bundle.putString("title", title);
                if (subtitle != null) bundle.putString("subtitle", subtitle);
//                EditCommentAudioActivity_.IntentBuilder_ audioTmpBuilder = EditCommentAudioActivity_
//                        .intent(context)
//                        .commentId(commentId)
//                        .commentType(CommentEntity.Type.AUDIO)
//                        .attachmentDirectory(attachmentDirectory)
//                        .auditId(auditId)
//                        .title(title)
//                        .subTitle(subtitle);

//                if (title != null) {
//                    audioTmpBuilder.title(title);
//                }
//
//                if (subtitle != null) {
//                    audioTmpBuilder.subTitle(subtitle);
//                }
//
//                builder = audioTmpBuilder;
                break;
            case FILE:
                action = R.id.action_edit_comment_file;
                bundle = new Bundle();
                if (commentId != null) bundle.putLong("commentId", commentId);
                bundle.putSerializable("commentType", CommentEntity.getIntFromTypeFrom(CommentEntity.Type.FILE));
                if (attachmentDirectory != null)
                    bundle.putString("attachmentDirectory", attachmentDirectory);
                if (auditId != null) bundle.putLong("auditId", auditId);
                if (title != null) bundle.putString("title", title);
                if (subtitle != null) bundle.putString("subtitle", subtitle);
//                EditCommentFileActivity_.IntentBuilder_ fileTmpBuilder = EditCommentFileActivity_
//                        .intent(context)
//                        .commentId(commentId)
//                        .commentType(CommentEntity.Type.FILE)
//                        .attachmentDirectory(attachmentDirectory)
//                        .auditId(auditId)
//                        .title(title)
//                        .subTitle(subtitle);
//
//                if (title != null) {
//                    fileTmpBuilder.title(title);
//                }
//
//                if (subtitle != null) {
//                    fileTmpBuilder.subTitle(subtitle);
//                }
//
//                builder = fileTmpBuilder;
                break;
            default:
                throw new RuntimeException("Error to be defined");
        }
        return new Pair<>(action, bundle);
//        if (builder != null) {
//            builder.startForResult(resultCode);
//        }
    }
}
