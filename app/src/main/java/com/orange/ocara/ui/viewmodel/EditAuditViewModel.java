package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import com.orange.ocara.business.model.AuditModel;
import com.orange.ocara.business.model.RulesetModel;
import com.orange.ocara.business.model.SiteModel;
import com.orange.ocara.business.model.UserModel;
import com.orange.ocara.business.service.RuleSetService;
import com.orange.ocara.business.service.impl.RuleSetServiceImpl_;
import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.db.ModelManagerImpl_;
import com.orange.ocara.data.cache.model.AuditEntity;
import com.orange.ocara.data.cache.model.AuditorEntity;
import com.orange.ocara.data.net.model.RulesetEntity;
import com.orange.ocara.ui.binding.AuditNameDoesNotExistValidator;
import com.orange.ocara.ui.binding.AuthorNamePatternValidator;
import com.orange.ocara.ui.binding.BindingResult;
import com.orange.ocara.ui.binding.UniqueReviewValidator;
import com.orange.ocara.ui.binding.UserNamePatternValidator;
import com.orange.ocara.ui.binding.Validator;
import com.orange.ocara.ui.model.AuditFormUiModel;
import com.orange.ocara.ui.model.AuditUiModel;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;

import static com.orange.ocara.tools.ListUtils.newArrayList;

public class EditAuditViewModel extends OcaraViewModel {

    ModelManager modelManager;
//    /**
//     * Current value of the {@link AuditorEntity} of the audit
//     */
//    private AuditorEntity currentAuthor;
//
//    /**
//     * Current value of the {@link AuditEntity.Level} of the audit
//     */
//    private AuditEntity.Level currentAuditLevel;
//
//    /**
//     * Current value of the {@link AuditEntity#getName()}
//     */
//    private String currentAuditName;

    public AuditEntity databaseAudit;

//    long auditId;

    //    public MutableLiveData<AuditUiModel> uiAudit;
    public MutableLiveData<String> uiAuditName = new MutableLiveData<>();
    public AuditEntity.Level uiAuditLevel;
    //    public MutableLiveData<Integer> uiAuditVersion = new MutableLiveData<>();
    public MutableLiveData<String> uiAuditAuthorName = new MutableLiveData<>();


//    public MediatorLiveData<String> uiAuditNameMliveData = new MediatorLiveData<>();
//    public MutableLiveData<Integer> uiAuditVersionMliveData  = new MediatorLiveData<>();
//    public MutableLiveData<String> uiAuditAuthorNameMliveData  = new MediatorLiveData<>();

    public MutableLiveData<Boolean> isAuditValid = new MutableLiveData<>();

    public EditAuditViewModel(Context context, long auditId) {

        super(context);
        modelManager = ModelManagerImpl_.getInstance_(context);
        databaseAudit = modelManager.getAudit(auditId);

        uiAuditName.setValue(databaseAudit.getName());
        uiAuditLevel = databaseAudit.getLevel();
//        uiAuditVersion.setValue(databaseAudit.getVersion());
        uiAuditAuthorName.setValue(databaseAudit.getAuthorName());
        isAuditValid.setValue(false);
//        uiAuditNameMliveData.addSource(uiAuditAuthorName, mediatorObs);


        validateUiAudit();
    }

//    Observer mediatorObs = new Observer() {
//        @Override
//        public void onChanged(Object o) {
//            validateUiAudit();
//        }
//    };

    public void validateUiAudit() {
        isAuditValid.setValue(false);
        BindingResult errors = new BindingResult();

        Validator<UserModel> authorNamePatternValidator = new UserNamePatternValidator();
        UserModel user = AuditorEntity.builder()
                .userName(uiAuditAuthorName.getValue()).build();
        authorNamePatternValidator.validate(user, errors);

        Validator<AuditModel> auditNameDoesNotExistValidator = new UniqueReviewValidator(modelManager);
        auditNameDoesNotExistValidator.validate(AuditUiModel
                .builder().id(databaseAudit.getId()).name(uiAuditName.getValue())
                .level(uiAuditLevel).authorName(uiAuditAuthorName.getValue())
                .build(), errors);

        if (!errors.hasErrors()) {
//            isAuditValid = true;
            isAuditValid.setValue(true);

        }
    }

    /**
     * We'd rather use some value object (ie {@link AuditUiModel}), and define the function equals()
     *
     * @return true if the two audits are different.
     */
    private boolean auditChanged() {
        boolean sameLevel = databaseAudit.getLevel() == uiAuditLevel;
        boolean sameAuditName = uiAuditName.getValue().equalsIgnoreCase(databaseAudit.getName());
        boolean sameAuthor = uiAuditAuthorName.getValue().equals(databaseAudit.getAuthorName());

        return !sameAuthor || !sameAuditName || !sameLevel;
    }

    public ModelManager getModelManager() {
        return modelManager;
    }

    public AuditEntity getDatabaseAudit() {
        return databaseAudit;
    }

    public Completable updateAudit() {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter emitter) throws Exception {
                if (auditChanged()) {
                    databaseAudit.setAuthor(makeAuditor(uiAuditAuthorName.getValue()));
                    databaseAudit.setName(uiAuditName.getValue());
                    databaseAudit.setLevel(uiAuditLevel);
                    modelManager.updateAudit(databaseAudit);
                }
                emitter.onComplete();

            }
        });
    }

    /**
     * Helper function that creates a new {@link AuditorEntity}
     * <p>
     * This function has been created, so as to make the unit test working. Without that,
     * ActiveAndroid prevents developers from creating a new {@link android.graphics.ColorSpace.Model}
     * in any unit test, without creating the whole {@link android.content.Context}.
     *
     * @param authorName a {@link String}
     * @return a new {@link AuditorEntity}
     */
    AuditorEntity makeAuditor(String authorName) {
        return AuditorEntity.builder().userName(authorName).build();
    }

    public void setUiAuditLevel(AuditEntity.Level uiAuditLevel) {
        this.uiAuditLevel = uiAuditLevel;
    }

    public RulesetModel getRuleSet() {
        RuleSetService ruleSetService = RuleSetServiceImpl_.getInstance_(context);
        RulesetEntity in = ruleSetService.getRuleSet(databaseAudit.getRuleSetRef(), databaseAudit.getRuleSetVersion(), false);
        return RulesetModel
                .builder().author(in.getAuthorName()).comment(in.getComment()).date(in.getDate())
                .reference(in.getReference()).ruleCategoryName(in.getRuleCategoryName())
                .stat(in.getStat()).type(in.getType()).version(in.getVersion()).id(in.getId())
                .build();

    }
}
