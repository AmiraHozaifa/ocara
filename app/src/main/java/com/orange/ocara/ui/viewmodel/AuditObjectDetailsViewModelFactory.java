package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import java.lang.reflect.InvocationTargetException;

public class AuditObjectDetailsViewModelFactory implements ViewModelProvider.Factory {


    private Long auditObjId;
    private Context context;

    public AuditObjectDetailsViewModelFactory(Context context, Long auditObjId) {
        this.context = context;
        this.auditObjId = auditObjId;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new AuditObjectDetailsViewModel(context, auditObjId);
    }

}