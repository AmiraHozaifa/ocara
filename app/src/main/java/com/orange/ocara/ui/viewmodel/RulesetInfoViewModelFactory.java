package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.orange.ocara.business.model.RulesetModel;
import com.orange.ocara.data.cache.db.ModelManager;

public class RulesetInfoViewModelFactory implements ViewModelProvider.Factory {

    private RulesetModel ruleset;
    private boolean rulesetUpgradable;
    private Context context;

    public RulesetInfoViewModelFactory(Context context, RulesetModel ruleset, boolean rulesetUpgradable) {
        this.context = context;
        this.ruleset = ruleset;
        this.rulesetUpgradable = rulesetUpgradable;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new RulesetInfoViewModel(context,ruleset, rulesetUpgradable);
    }

}