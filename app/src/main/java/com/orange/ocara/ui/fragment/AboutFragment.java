package com.orange.ocara.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.orange.ocara.BuildConfig;
import com.orange.ocara.R;

public class AboutFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView_ = inflater.inflate(R.layout.fragment_about, container, false);

        ((TextView)contentView_.findViewById(R.id.version)).setText(getString(R.string.about_version, BuildConfig.VERSION_NAME));

        contentView_.findViewById(R.id.item1_title).setOnClickListener(new AboutFragment.onTextClickListener
                (contentView_.findViewById(R.id.item1_content_txt),  contentView_.findViewById(R.id.item1_content_txt1)
                        ,contentView_.findViewById(R.id.image1), contentView_.findViewById(R.id.image2)));

        contentView_.findViewById(R.id.item2_title).setOnClickListener(new AboutFragment.onTextClickListener
                (contentView_.findViewById(R.id.item2_content_txt)));


        contentView_.findViewById(R.id.item3_title).setOnClickListener(new AboutFragment.onTextClickListener
                (contentView_.findViewById(R.id.item3_content_txt)));



        contentView_.findViewById(R.id.item4_title).setOnClickListener(new AboutFragment.onTextClickListener
                (contentView_.findViewById(R.id.item4_content_txt)));



        contentView_.findViewById(R.id.item5_title).setOnClickListener(new AboutFragment.onTextClickListener
                (contentView_.findViewById(R.id.item5_content_txt)));

        setHasOptionsMenu(true);

        return contentView_;
    }

    class onTextClickListener implements View.OnClickListener {
        View[] viewsToToggleVisibility;

        public onTextClickListener(View... viewToToggleVisibility) {
            this.viewsToToggleVisibility = viewToToggleVisibility;
        }

        @Override
        public void onClick(View v) {
            if (viewsToToggleVisibility[0].getVisibility() == View.VISIBLE) {
                for (View viewToToggleVisibility : viewsToToggleVisibility) {
                    viewToToggleVisibility.setVisibility(View.GONE);
                }
                ((TextView) v).setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_chevron_right_small, 0);
            } else {
                for (View viewToToggleVisibility : viewsToToggleVisibility) {
                    viewToToggleVisibility.setVisibility(View.VISIBLE);
                }
                ((TextView) v).setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_chevron_down_small, 0);
            }

        }
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        MenuItem item = menu.findItem(R.id.action_help);
        if (item != null) {
            item.setVisible(false);
        }

        MenuItem item2 = menu.findItem(R.id.action_about);
        if (item2 != null) {
            item2.setVisible(false);
        }
    }
}