/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.fragment;

import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.orange.ocara.R;
import com.orange.ocara.data.cache.model.CommentEntity;
import com.orange.ocara.ui.action.EditCommentActionBuilder;
import com.orange.ocara.ui.viewmodel.ListAuditCommentViewModel;
import com.orange.ocara.ui.viewmodel.ListCommentsViewModelFactory;
import com.orange.ocara.ui.viewmodel.ListCommentViewModel;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import static com.orange.ocara.R.id.buttonbar_left_button;

/**
 * Activity dedicated to the display of a list of {@link CommentEntity}s
 */
@EFragment(R.layout.fragment_list_comment)
@OptionsMenu(R.menu.comment_list)
public class ListAuditCommentFragment extends ListCommentFragment {

    private static final int EDIT_COMMENT_REQUEST_CODE = 1;

    //    @Extra("auditId")
    Long auditId;

    //    @Extra("attachmentDirectory")
    String attachmentDirectory;

    ListAuditCommentViewModel commentViewModel;


    @ViewById(R.id.comment_list)
    RecyclerView commentListView;

    @ViewById(R.id.comment_list_empty)
    View emptyView;

    @ViewById(buttonbar_left_button)
    Button leftButton;

    @ViewById(R.id.buttonbar_right_button)
    Button rightButton;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list_comment, container, false);

        commentListView = v.findViewById(R.id.comment_list);
        emptyView = v.findViewById(R.id.comment_list_empty);
        leftButton = v.findViewById(R.id.buttonbar_left_button);
        rightButton = v.findViewById(R.id.buttonbar_right_button);
        attachmentDirectory = ListAuditCommentFragment_Args.fromBundle(getArguments()).getAttachmentDirectory();
        auditId = ListAuditCommentFragment_Args.fromBundle(getArguments()).getAuditId();

        commentViewModel = new ViewModelProvider(this, new ListCommentsViewModelFactory(getActivity(), auditId)).get(ListAuditCommentViewModel.class);
//        commentViewModel.commentsLiveData.observe(getViewLifecycleOwner(), commentListUpdateObserver);

        MutableLiveData liveData = NavHostFragment.findNavController(this).getCurrentBackStackEntry()
                .getSavedStateHandle()
                .getLiveData("commentId");
        liveData.observe(getViewLifecycleOwner(), new Observer() {
            @Override
            public void onChanged(Object commentId) {
                commentViewModel.updateComment((Long) commentId);
            }
        });

        initView();
        return v;
    }

    public void showNewComment(CommentEntity.Type type) {
//        new EditCommentIntentBuilder(getActivity(), type, attachmentDirectory)
//                .auditId(auditId)
//                .getAction(EDIT_COMMENT_REQUEST_CODE);

//        MutableLiveData liveData = NavHostFragment.findNavController(this).getCurrentBackStackEntry()
//                .getSavedStateHandle()
//                .getLiveData("commentId");
//        liveData.observe(getViewLifecycleOwner(), new Observer() {
//            @Override
//            public void onChanged(Object commentId) {
//                commentViewModel.updateComment((Long) commentId);
//            }
//        });


        Pair<Integer, Bundle> pair = new EditCommentActionBuilder(getContext(), type, attachmentDirectory).auditId(auditId)
                .getAction();

        NavHostFragment.findNavController(this).navigate(pair.first, pair.second);

    }

    public void showComment(CommentEntity c) {
//        new EditCommentIntentBuilder(getActivity(), c.getType(), attachmentDirectory)
//                .commentId(c.getId())
//                .auditId(auditId)
//                .startIntent(EDIT_COMMENT_REQUEST_CODE);


        Pair<Integer, Bundle> pair = new EditCommentActionBuilder(getContext(), c.getType(), attachmentDirectory).commentId(c.getId())
                .auditId(auditId)
                .getAction();
        NavHostFragment.findNavController(this).navigate(pair.first, pair.second);

    }

    @Override
    public ListCommentViewModel getCommentsViewModel() {
        return commentViewModel;
    }

    @Override
    public String getTitle() {
        String auditName = commentViewModel.getEntityName();
        return getString(R.string.list_comment_title, auditName);
    }


    public RecyclerView getCommentListView() {
        return commentListView;
    }

    public View getEmptyView() {
        return emptyView;
    }

    public Button getLeftButton() {
        return leftButton;
    }

    public Button getRightButton() {
        return rightButton;
    }
}
