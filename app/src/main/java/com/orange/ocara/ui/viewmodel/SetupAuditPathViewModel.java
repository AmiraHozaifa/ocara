package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.orange.ocara.business.service.RuleSetService;
import com.orange.ocara.business.service.impl.RuleSetServiceImpl_;
import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.db.ModelManagerImpl_;
import com.orange.ocara.data.cache.model.AuditEntity;
import com.orange.ocara.data.cache.model.AuditObjectEntity;
import com.orange.ocara.data.cache.model.CommentEntity;
import com.orange.ocara.data.cache.model.QuestionAnswerEntity;
import com.orange.ocara.data.cache.model.ResponseModel;
import com.orange.ocara.data.cache.model.RuleAnswerEntity;
import com.orange.ocara.data.net.model.EquipmentCategoryEntity;
import com.orange.ocara.data.net.model.EquipmentEntity;
import com.orange.ocara.data.net.model.RulesetEntity;
import com.orange.ocara.ui.tools.RefreshStrategy;

import org.androidannotations.annotations.Background;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import timber.log.Timber;

public class SetupAuditPathViewModel extends OcaraViewModel {
    private static final RefreshStrategy STRATEGY = RefreshStrategy.builder().depth(RefreshStrategy.DependencyDepth.AUDIT_OBJECT).commentsNeeded(true).build();
    protected static final RefreshStrategy RULE_REFRESH_STRATEGY = RefreshStrategy.ruleAnswerRefreshStrategy();

    private ModelManager modelManager;
    private RuleSetService mRuleSetService;
    public MutableLiveData<AuditEntity> auditLiveData;
    public MutableLiveData<List<AuditObjectEntity>> auditObjectsLiveData;
    public MutableLiveData<EquipmentEntity> selectedEquipmentEntityLiveData;
    private Long auditId;

    public SetupAuditPathViewModel(Context context, Long auditId) {
        super(context);
        this.auditId = auditId;
        mRuleSetService = RuleSetServiceImpl_.getInstance_(context);
        modelManager = ModelManagerImpl_.getInstance_(context);
        this.auditLiveData = new MutableLiveData<>();

        refreshAudit();

        selectedEquipmentEntityLiveData = new MutableLiveData<>();
    }


    public void refreshAudit() {
        AuditEntity auditEntity = modelManager.getAudit(auditId);
        modelManager.refresh(auditEntity, STRATEGY);
        modelManager.refresh(auditEntity, RULE_REFRESH_STRATEGY);

        this.auditLiveData.setValue(auditEntity);

    }

    public void setSelectedEquipment(EquipmentEntity equipment) {
        selectedEquipmentEntityLiveData.setValue(equipment);
    }

    public ModelManager getModelManager() {
        return modelManager;
    }

    public Single createAuditObject(/*AuditEntity audit,*/  EquipmentEntity objectDescription, List<EquipmentEntity> children) {
        return Single.create(new SingleOnSubscribe<AuditObjectEntity>() {
            @Override
            public void subscribe(SingleEmitter<AuditObjectEntity> emitter) throws Exception {

                AuditObjectEntity auditObject = modelManager.createAuditObject(auditLiveData.getValue(), objectDescription);
                for (EquipmentEntity child : children) {
                    modelManager.createChildAuditObject(auditObject, child);
                }
                Timber.d("ActivityMessage=Created audit object named %s, related to the ruleset named %s", auditObject.getName(), auditObject.getRuleSet().getType());
                auditObject.refresh(STRATEGY);
                modelManager.refresh(auditLiveData.getValue(), STRATEGY);
                modelManager.refresh(auditLiveData.getValue(), RULE_REFRESH_STRATEGY);

                auditLiveData.postValue(auditLiveData.getValue());
                emitter.onSuccess(auditObject);
            }
        });
    }

    public List<EquipmentCategoryEntity> getEquipmentCategories() {
        RulesetEntity ruleSet = mRuleSetService.getRuleSet(auditLiveData.getValue().getRuleSetRef(),
                auditLiveData.getValue().getRuleSetVersion(), false);
        return mRuleSetService.getRulsetObjects(ruleSet);

    }

    public Completable deleteAuditObject(AuditObjectEntity auditObject) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter emitter) throws Exception {
                modelManager.deleteAuditObject(auditObject);
//                selectedIndex = selectedIndex - 1;
//                refreshAudit();
                modelManager.refresh(auditLiveData.getValue(), STRATEGY);
                modelManager.refresh(auditLiveData.getValue(), RULE_REFRESH_STRATEGY);

                auditLiveData.postValue(auditLiveData.getValue());
                emitter.onComplete();
            }
        });

    }


    public Completable deleteAllAuditObjects() {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter emitter) throws Exception {
                modelManager.deleteAllAuditObjects(auditLiveData.getValue());
//                selectedIndex = -1;
//        refreshAudit();
                modelManager.refresh(auditLiveData.getValue(), STRATEGY);
                modelManager.refresh(auditLiveData.getValue(), RULE_REFRESH_STRATEGY);

                auditLiveData.postValue(auditLiveData.getValue());
                emitter.onComplete();
            }
        });

    }

    public EquipmentEntity getAuditObjectDescription(AuditObjectEntity auditObject) {
        return mRuleSetService.getObjectDescriptionFromRef(auditObject.getAudit().getRuleSetRef(), auditObject.getAudit().getRuleSetVersion(), auditObject.getObjectDescriptionId());
    }

    public EquipmentEntity getAuditObjectDescriptionFromRef(String ref) {
        return mRuleSetService.getObjectDescriptionFromRef(auditLiveData.getValue().getRuleSetRef(), auditLiveData.getValue().getRuleSetVersion(), ref);
    }

    public List<CommentEntity> getAuditComments() {
        modelManager.refresh(auditLiveData.getValue(), RefreshStrategy.builder().commentsNeeded(true).build());
        return auditLiveData.getValue().getComments();
    }

    public Completable updateAuditObjName(AuditObjectEntity auditObject, String name) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter emitter) throws Exception {
                auditObject.setName(name);
                auditObject.save();
                modelManager.refresh(auditLiveData.getValue(), STRATEGY);
                modelManager.refresh(auditLiveData.getValue(), RULE_REFRESH_STRATEGY);

                auditLiveData.postValue(auditLiveData.getValue());
                emitter.onComplete();
            }
        });
    }

    public void updateAuditObjectChildren(final AuditObjectEntity parent, List<EquipmentEntity> childrenToCreate, List<AuditObjectEntity> childrenToRemove) {
        Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter emitter) throws Exception {
                modelManager.updateAuditObjectChildren(parent, childrenToCreate, childrenToRemove);
                modelManager.refresh(auditLiveData.getValue(), STRATEGY);
                modelManager.refresh(auditLiveData.getValue(), RULE_REFRESH_STRATEGY);

                auditLiveData.postValue(auditLiveData.getValue());
                emitter.onComplete();
            }
        }).subscribe();
    }


    public void resetAuditObject(AuditObjectEntity auditObject) {

        // reset all ruleAnswer to 'NO_ANSWER'
        List<RuleAnswerEntity> ruleAnswers = auditObject.computeAllRuleAnswers();
        for (RuleAnswerEntity r : ruleAnswers) {
            r.updateResponse(ResponseModel.NO_ANSWER);
        }
    }

    public void deleteAudit() {
        Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter emitter) throws Exception {
                modelManager.deleteAudit(auditId);
//                auditsList = modelManager.getAllAudits(queryFilter, sortCriteria);
                emitter.onComplete();
            }
        }).subscribe();
    }

}
