/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.orange.ocara.R;
import com.orange.ocara.data.cache.model.CommentEntity;
import com.orange.ocara.ui.adapter.AuditCommentsRVAdapter;
import com.orange.ocara.ui.dialog.OcaraDialogBuilder;
import com.orange.ocara.ui.intent.EditCommentIntentBuilder;
import com.orange.ocara.ui.viewmodel.ListCommentViewModel;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import static com.orange.ocara.R.id.buttonbar_left_button;

/**
 * Activity dedicated to the display of a list of {@link CommentEntity}s
 */
@EFragment(R.layout.fragment_list_comment)
public abstract class ListCommentFragment extends BaseFragment implements AuditCommentsRVAdapter.AuditCommentsRVAdapterListener {

    private static final int EDIT_COMMENT_REQUEST_CODE = 1;

//    @Extra("auditId")
//    Long auditId;

//    protected AuditEntity entity;

//    @Bean(ModelManagerImpl.class)
//    ModelManager modelManager;

//    @Bean(ListAuditCommentsUiConfig.class)
//    ListAuditCommentsUiConfig uiConfig;

//    @Extra("attachmentDirectory")
//    String attachmentDirectory;


    /**
     * Presenter dedicated to the view
     */
//    private ListAuditCommentsUserActionsListener userActionsListener;

    /**
     * Adapter that manages the content of the list.
     */
    private AuditCommentsRVAdapter commentListAdapter;
//    @AfterExtras
//    void initActionListener() {

//        userActionsListener = uiConfig.actionsListener(ListAuditCommentFragment.this);
//    }

    protected Observer<List<CommentEntity>> commentListUpdateObserver = new Observer<List<CommentEntity>>() {
        @Override
        public void onChanged(List<CommentEntity> commentEntities) {
            if (commentEntities.isEmpty()) {
                getCommentListView().setVisibility(View.GONE);
                getEmptyView().setVisibility(View.VISIBLE);
            } else {
                getCommentListView().setVisibility(View.VISIBLE);
                getEmptyView().setVisibility(View.GONE);
            }
            commentListAdapter.update(commentEntities);
        }
    };

    protected void initView (){
        getCommentsViewModel().commentsLiveData.observe(getViewLifecycleOwner(), commentListUpdateObserver);
        commentListAdapter = new AuditCommentsRVAdapter(getActivity(), this);


        getCommentListView().setLayoutManager(new LinearLayoutManager(getActivity()));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getCommentListView().getContext(),
                DividerItemDecoration.VERTICAL);
        getCommentListView().addItemDecoration(dividerItemDecoration);
        getCommentListView().setAdapter(commentListAdapter);

        initTitle();
    }
    @Override
    public void onResume() {
        super.onResume();
        getCommentsViewModel().loadComments();
    }
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        loadEntityBackground();
//    }

//    @Background
//    void loadEntityBackground() {
//        this.entity = modelManager.getAudit(auditId);
//        initView();
//        refreshComments();
//    }

    //    @UiThread
    void initTitle() {
//        String auditName = getCommentsViewModel().getEntityName();
//        String title = getString(R.string.list_comment_title, auditName);
        String title = getTitle();
        setTitle(title);
    }

    @OptionsItem(R.id.delete_all_comments)
    void onDeleteAllComments() {
        AlertDialog confirmDialog = new OcaraDialogBuilder(getContext())
                .setTitle(R.string.comment_list_delete_all_comment_message_title) // title
                .setMessage(getString(R.string.comment_list_delete_all_comment_audit_message)) // message
                .setPositiveButton(R.string.action_remove, (dialog, which) -> deleteAllComments())
                .setNegativeButton(R.string.action_cancel, null)
                .create();
        confirmDialog.show();
    }

//    @AfterViews
//    void setUpCommentList() {
//        commentListView.setEmptyView(emptyView);
//        commentListView.setAdapter(commentListAdapter);
//
//        registerForContextMenu(commentListView);
//    }

    @AfterViews
    void setUpButtonBar() {
        getLeftButton().setText(R.string.action_back);
        getRightButton().setVisibility(View.GONE);
    }

//    @ItemClick(R.id.comment_list)
//    void commentListItemClicked(CommentEntity comment) {
//        showComment(comment);
//    }

//    @Override
//    public void onCreateContextMenu(ContextMenu menu, View v,
//                                    ContextMenu.ContextMenuInfo menuInfo) {
//        super.onCreateContextMenu(menu, v, menuInfo);
//
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.comment_context_menu, menu);
//    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//
//        if (entity != null) {
//            setLoading(true);
//            refreshComments();
//        }
//    }

    @Click(R.id.buttonbar_left_button)
    void onLeftButtonClicked() {
        finish();
    }

    @OptionsItem(R.id.action_comment_text)
    void onCreateTextComment() {
        createComment(CommentEntity.Type.TEXT);
    }

    @OptionsItem(R.id.action_comment_audio)
    void onCreateAudioComment() {
        createComment(CommentEntity.Type.AUDIO);
    }

    @OptionsItem(R.id.action_comment_photo)
    void onCreatePhotoComment() {
        createComment(CommentEntity.Type.PHOTO);
    }

    @OptionsItem(R.id.action_comment_file)
    void onCreateFileComment() {
        createComment(CommentEntity.Type.FILE);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.action_remove) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
//            setLoading(true);
            getCommentsViewModel().deleteComment(commentListAdapter.getItem(info.position));
            return true;
        } else {
            return super.onContextItemSelected(item);
        }
    }

    /**
     * To create a new comment.
     *
     * @param type comment type
     */
    private void createComment(CommentEntity.Type type) {
        showNewComment(type);
    }

//    @OnActivityResult(EDIT_COMMENT_REQUEST_CODE)
//    void onResult(int resultCode, @OnActivityResult.Extra(value = "commentId") Long commentId) {
//
//        if (resultCode == RESULT_OK) {
//            updateComment(commentId);
//        }
//    }

//    @Background(serial = "serial")
//    public void refreshComments() {
//        modelManager.refresh(entity, RefreshStrategy.builder().commentsNeeded(true).build());
//        List<CommentEntity> comments = entity.getComments();
//        showComments(comments);
//    }

//    @UiThread(propagation = UiThread.Propagation.REUSE)
//    public void showComments(List<CommentEntity> comments) {
//        setLoading(false);
//        commentListAdapter.update(comments);
//    }

//    @Background(serial = "serial")
//    void updateComment(Long commentId) {
//        CommentEntity comment = Model.load(CommentEntity.class, commentId);
//        entity.attachComment(comment);
//        comment.save();
//    }

//    @Background
    protected void deleteAllComments() {
//        modelManager.deleteAllComments(comments);
//        refreshComments();
        getCommentsViewModel().deleteAllComments();
    }

//    @Background
    protected void deleteComment(CommentEntity comment) {
//        modelManager.deleteComment(comment);
//        refreshComments();
        getCommentsViewModel().deleteComment(comment);
    }

    @Override
    public void onDestroy() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setLogo(R.drawable.ic_app);
        super.onDestroy();
    }
//    public void showNewComment(CommentEntity.Type type) {
////        new EditCommentIntentBuilder(getActivity(), type, attachmentDirectory)
////                .auditId(auditId)
////                .getAction(EDIT_COMMENT_REQUEST_CODE);
//
//        new EditCommentIntentBuilder(getActivity(), type, attachmentDirectory)
//                .auditId(getCommentsViewModel().getAuditId())
//                .getAction(EDIT_COMMENT_REQUEST_CODE);
//    }
//
//    public void showComment(CommentEntity c) {
////        new EditCommentIntentBuilder(getActivity(), c.getType(), attachmentDirectory)
////                .commentId(c.getId())
////                .auditId(auditId)
////                .getAction(EDIT_COMMENT_REQUEST_CODE);
//        new EditCommentIntentBuilder(getActivity(), c.getType(), attachmentDirectory)
//                .commentId(c.getId())
//                .auditId(getCommentsViewModel().getAuditId())
//                .getAction(EDIT_COMMENT_REQUEST_CODE);
//
//    }

    @Override
    public void onCommentListItemClicked(CommentEntity commentEntity) {
        showComment(commentEntity);
    }

    @Override
    public void onDeleteCommentListItemClicked(CommentEntity commentEntity) {
        deleteComment(commentEntity);
    }

    public abstract void showNewComment(CommentEntity.Type type);

    public abstract void showComment(CommentEntity c);

    public abstract ListCommentViewModel getCommentsViewModel();

    public abstract String getTitle();

    public abstract RecyclerView getCommentListView();

    public abstract View getEmptyView();

    public abstract Button getLeftButton();

    public abstract Button getRightButton();
}
