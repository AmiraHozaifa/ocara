package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.orange.ocara.business.BizConfig;
import com.orange.ocara.data.cache.db.ModelManager;

public class CreateAuditViewModelFactory implements ViewModelProvider.Factory {
//    private BizConfig bizConfig;
//    private ModelManager modelManager;

    Context context;

    public CreateAuditViewModelFactory(Context context) {
//        this.bizConfig = bizConfig;
//        this.modelManager = modelManager;
        this.context = context;
    }


    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new CreateAuditViewModel(context);
    }
}