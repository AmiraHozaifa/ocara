package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import com.orange.ocara.business.BizConfig;
import com.orange.ocara.business.BizConfig_;
import com.orange.ocara.business.interactor.CheckOnboardingStatusTask;
import com.orange.ocara.business.interactor.CheckRemoteStatusTask;
import com.orange.ocara.business.interactor.CheckTermsStatusTask;
import com.orange.ocara.business.interactor.InitTask;
import com.orange.ocara.business.interactor.UseCase;
import com.orange.ocara.business.interactor.UseCaseHandler;
import com.orange.ocara.ui.IntroUiConfig_;
import com.orange.ocara.ui.contract.IntroContract;

public class SplashActivityViewModel extends OcaraViewModel {
//    private IntroContract.IntroUserActionsListener actionsListener;



    private final UseCaseHandler useCaseHandler;

    /** see {@link InitTask} */
    private final UseCase<InitTask.InitRequest, InitTask.InitResponse> initTask;

    /** see {@link CheckTermsStatusTask} */
    private final UseCase<CheckTermsStatusTask.CheckTermsStatusRequest, CheckTermsStatusTask.CheckTermsStatusResponse> checkTermsStatusTask;

    /** see {@link CheckOnboardingStatusTask}*/
    private final UseCase<CheckOnboardingStatusTask.CheckOnboardingStatusRequest, CheckOnboardingStatusTask.CheckOnboardingStatusResponse> checkOnboardingStatusTask;

    /** see {@link CheckRemoteStatusTask} */
    private final UseCase<CheckRemoteStatusTask.CheckRemoteStatusRequest, CheckRemoteStatusTask.CheckRemoteStatusResponse> checkRemoteStatusTask;


    public SplashActivityViewModel(Context context) {
        super(context);

        BizConfig bizConfig = BizConfig_.getInstance_(context);
        this.useCaseHandler = BizConfig.USE_CASE_HANDLER;
        this.initTask = bizConfig.initTask();
        this.checkTermsStatusTask = bizConfig.checkTermsStatusTask();
        this.checkOnboardingStatusTask = bizConfig.checkOnboardingStatusTask();
        this.checkRemoteStatusTask = bizConfig.checkRemoteStatusTask();
    }

    public void checkInit(UseCase.UseCaseCallback<InitTask.InitResponse> callback) {
        useCaseHandler.execute(initTask, new InitTask.InitRequest(), callback);
    }

    public void checkTerms(UseCase.UseCaseCallback<CheckTermsStatusTask.CheckTermsStatusResponse> callback) {

        useCaseHandler.execute(checkTermsStatusTask, new CheckTermsStatusTask.CheckTermsStatusRequest(), callback);
    }

    public void checkTutorial(UseCase.UseCaseCallback<CheckOnboardingStatusTask.CheckOnboardingStatusResponse> callback) {

        useCaseHandler.execute(checkOnboardingStatusTask, new CheckOnboardingStatusTask.CheckOnboardingStatusRequest(), callback);
    }

    public void checkRemote(UseCase.UseCaseCallback<CheckRemoteStatusTask.CheckRemoteStatusResponse> callback) {
        useCaseHandler.execute(checkRemoteStatusTask, new CheckRemoteStatusTask.CheckRemoteStatusRequest(), callback);
    }
}
