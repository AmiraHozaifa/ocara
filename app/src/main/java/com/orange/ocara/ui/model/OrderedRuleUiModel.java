/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.Html;

import com.orange.ocara.business.model.RuleModel;

/**
 * a UiModel for rules
 */
public class OrderedRuleUiModel implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public OrderedRuleUiModel createFromParcel(Parcel in) {
            return new OrderedRuleUiModel(in);
        }

        public OrderedRuleUiModel[] newArray(int size) {
            return new OrderedRuleUiModel[size];
        }
    };

    private final CharSequence ruleText;

    private final long ruleId;

    private final int order;

    private final boolean illustrated;

    public OrderedRuleUiModel(Parcel in) {

        this.ruleText = in.readString();
        this.ruleId = in.readLong();
        this.order = in.readInt();
        this.illustrated = in.readInt() == 1;
    }

    public OrderedRuleUiModel(RuleModel model) {

        ruleId = model.getId();
        ruleText = Html.fromHtml(model.getLabel());
        this.order = model.getIndex();
        this.illustrated = model.isIllustrated();
    }

    public CharSequence getRuleText() {
        return ruleText;
    }

    public int getOrder() {
        return order;
    }

    public long getRuleId() {
        return ruleId;
    }

    public boolean isIllustrated() {
        return illustrated;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(String.valueOf(this.ruleText));
        dest.writeLong(this.ruleId);
        dest.writeInt(this.order);
        dest.writeInt(this.illustrated ? 1 : 0);
    }


}