/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.orange.ocara.R;
import com.orange.ocara.ui.activity.MainActivity;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;

import java.io.File;

@EFragment
/*package.*/ public abstract class BaseFragment extends Fragment {


    @Override
    public void onResume() {
        super.onResume();
        Bundle bundle = new Bundle();
        bundle.putString("screen_class", getClass().getName());
        bundle.putString("screen_name", getClass().getName());
        FirebaseAnalytics.getInstance(getActivity().getApplicationContext()).logEvent("screen_view", bundle);

    }

    @AfterViews
    public void initBase() {
        hideSoftKeyboard();
    }

    protected void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        View focusedView = getActivity().getCurrentFocus();
        if (focusedView != null) {
            inputMethodManager.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
        }
    }

    public void setLoading(Boolean loading) {
        ((MainActivity) getActivity()).setLoading(loading);
    }

    public void updateLogo(String iconName) {
        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                ((AppCompatActivity) getActivity()).getSupportActionBar().setLogo(new BitmapDrawable(getResources(), bitmap));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                // no-op
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                // no-op
            }
        };
        final int maxSize = getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_height_material);
        final String path = getActivity().getExternalCacheDir() + File.separator + iconName;
        File icon = new File(path);
        Picasso.with(getActivity()).load(icon).placeholder(android.R.color.black).resize(maxSize, maxSize).into(target);
    }

    public void setTitle(CharSequence title) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
    }

    public void setSubtitle(CharSequence title) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(title);
    }

    @Override
    public void onDestroy() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(null);
        super.onDestroy();
    }

    public void finish() {
        NavHostFragment.findNavController(this).navigateUp();
    }

    public int getHelpPage() {
        return 0;
    }
}
