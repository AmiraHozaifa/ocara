/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.orange.ocara.R;
import com.orange.ocara.data.cache.model.AuditObjectEntity;
import com.orange.ocara.data.cache.model.QuestionAnswerEntity;
import com.orange.ocara.data.cache.model.ResponseModel;
import com.orange.ocara.ui.viewmodel.AuditObjectsExpertViewModel;
import com.orange.ocara.ui.viewmodel.AuditObjectsViewModelFactory;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OptionsMenu;

import java.util.List;

import javax.annotation.Nullable;

import timber.log.Timber;

/**
 * Activity dedicated to managing an {@link AuditObjectEntity}
 */
@EFragment(R.layout.fragment_audit_object)
@OptionsMenu(R.menu.audit_objects)
public class AuditObjectsExpertFragment extends AuditObjectsFragment {

    AuditObjectsExpertViewModel viewModel;

    Observer<List<QuestionAnswerEntity>> questionListObserver = new Observer<List<QuestionAnswerEntity>>() {
        @Override
        public void onChanged(List<QuestionAnswerEntity> QuestionList) {
            refreshQuestionList(QuestionList);
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = super.onCreateView(inflater, container, savedInstanceState);
        editMode = AuditObjectsExpertFragment_Args.fromBundle(getArguments()).getEditMode();
        auditId = AuditObjectsExpertFragment_Args.fromBundle(getArguments()).getAuditId();
        selectedObjects = AuditObjectsExpertFragment_Args.fromBundle(getArguments()).getSelectedObjects();


        viewModel = new ViewModelProvider(this, new AuditObjectsViewModelFactory(getActivity(), selectedObjects))
                .get(AuditObjectsExpertViewModel.class);
        viewModel.questionListLiveData.observe(getViewLifecycleOwner(), questionListObserver);

//        Toast.makeText(getActivity(), "IN FRAG", Toast.LENGTH_LONG).show();

        return v;

    }

    @Override
    protected String getTitle() {
        return getString(R.string.audit_object_expert_mode_title, getAuditObjectName(viewModel.getCurrentAuditObject()), viewModel.getCurrentObjectIdx() + 1, selectedObjects.length);
    }


    @Click(R.id.buttonbar_left_button)
    void onLeftButton() {
        Timber.d("Clicking on the left button...");
        if (viewModel.isLastItemSelected()) {
            onBack();
        } else {
            moveToObject(viewModel.getCurrentObjectIdx() - 1);
        }
    }

    @Click(R.id.buttonbar_right_button)
    void onRightButton() {

        Timber.d("Clicking on the right button...");
        if (viewModel.isLastItemSelected()) {
            Timber.d("Checking failed...");
            finish();
            return; // blindage
        }

        Timber.d("Checking succeeded...");

        moveToObject(viewModel.getCurrentObjectIdx() + 1);
//        final AuditObjectEntity currentAuditObject = viewModel.getCurrentAuditObject();
//        moveToAuditObject(viewModel.getCurrentQuestionIdx() + 1);
//
//        if (viewModel.getCurrentQuestionIdx() < viewModel.getNumberOfQuestions()
//                && viewModel.getCurrentAuditObject() != currentAuditObject) {
//            // object has changed ==> display a progress status for user
//            showAuditObjectProgress(currentAuditObject, false);
//            return;
//        }

    }


    void moveToObject(final int newCurrentSelectedObject) {

        final AuditObjectEntity currentAuditObject = viewModel.getCurrentAuditObject();

        // check that audioObject can be saved

        if (currentAuditObject != null && currentAuditObject.getResponse().equals(ResponseModel.NOT_APPLICABLE)) {

            Timber.d("Could not move to next question %d", newCurrentSelectedObject);

            // cannot save an object with this status.
            // User MUST set at least one piece of information

            AlertDialog errorDialog = new AlertDialog.Builder(getContext())
                    .setTitle(R.string.audit_object_badglobaleresponse_title) // title
                    .setMessage(R.string.audit_object_badglobaleresponse_message) // message
                    .setPositiveButton(R.string.action_ok, null)
                    .create();

            errorDialog.show();
            viewModel.resetAuditObject();
        } else {
//            currentSelectedQuestion = newCurrentSelectedQuestion;

            // save audit object
            Timber.d("Save audit and move to next question %d", newCurrentSelectedObject);
//            saveAuditObject(currentAuditObject);
//            viewModel.selectQuestion(newCurrentSelectedQuestion);

            if (viewModel.getCurrentObjectIdx() < selectedObjects.length) {
                showAuditObjectProgress(currentAuditObject, false);
                viewModel.selectObject(newCurrentSelectedObject);
            } else {
                // show progress and terminate
                showAuditObjectProgress(currentAuditObject, true);
            }
        }

    }

    @Override
    public AuditObjectsExpertViewModel getViewModel() {
        return viewModel;
    }

}
