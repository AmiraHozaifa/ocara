package com.orange.ocara.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.lifecycle.ViewModelProvider;

import com.orange.ocara.R;
import com.orange.ocara.data.net.model.RulesetEntity;
import com.orange.ocara.ui.ListRulesUiConfig;
import com.orange.ocara.ui.adapter.RulesetSpinnerAdapter;
import com.orange.ocara.ui.contract.ListRulesetsContract;
import com.orange.ocara.ui.viewmodel.BrowseRuleSetsViewModel;
import com.orange.ocara.ui.viewmodel.BrowseRulesetsViewModelFactory;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ItemSelect;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

@EFragment(R.layout.fragment_view_refrential)
public class BrowseRulesetsFragment extends BaseFragment {


    private static final String RULESET = "RULESET";

    //    @Extra
    String rulesetReference;

    //    @Extra
    Integer ruleSetVersion;

    //    @Extra
    String defaultEquipmentReference;

    //    @ViewById(R.id.rule_spinner)
    AppCompatSpinner rulesetSpinner;

    //    @ViewById(R.id.rule_layout)
    FrameLayout equipmentFragment;

    @Bean
    RulesetSpinnerAdapter rulesetAdapter;

    private boolean init = false;

//    @Bean(ListRulesUiConfig.class)
//    ListRulesUiConfig uiConfig;

    ListRulesetsContract.ListRulesUserActionsListener actionListener;
    BrowseRuleSetsViewModel viewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.err.println("OCARA : AUDIT  ONCREATE BrowseRulesetsFragment");

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View v = super.onCreateView(inflater, container, savedInstanceState);
//        System.err.println("OCARA : AUDIT BrowseRuleSets onCreateView");

        View v = inflater.inflate(R.layout.fragment_view_refrential, container, false);
        rulesetSpinner = v.findViewById(R.id.rule_spinner);
        equipmentFragment = v.findViewById(R.id.rule_layout);
        return v;
    }

    @AfterViews
    void initView() {
//        actionListener = uiConfig.actionsListener(this);
//
//        actionListener.loadRulesets();

        rulesetReference = BrowseRulesetsFragment_Args.fromBundle(getArguments()).getRulesetReference();
        ruleSetVersion = BrowseRulesetsFragment_Args.fromBundle(getArguments()).getRuleSetVersion();
        defaultEquipmentReference = BrowseRulesetsFragment_Args.fromBundle(getArguments()).getDefaultEquipmentReference();

        System.err.println("OCARA : AUDIT BrowseRuleSets onCreateView ARGS " + rulesetReference + " " + ruleSetVersion + " " + defaultEquipmentReference);

        viewModel = new ViewModelProvider(getActivity(), new BrowseRulesetsViewModelFactory(getActivity())).get(BrowseRuleSetsViewModel.class);
        viewModel.ruleSets.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new DisposableSingleObserver<List<RulesetEntity>>() {
            @Override
            public void onSuccess(List<RulesetEntity> rulesetEntities) {
                showRulesets(rulesetEntities);
                dispose();
            }

            @Override
            public void onError(Throwable e) {

            }
        });
    }

    //    @Override
    public void showRulesets(List<RulesetEntity> list) {

        Timber.d("Message=Initializing the list of rulesets;RulesetsCount=%d", list.size());
        if (!list.isEmpty()) {
            rulesetAdapter.update(list);
            rulesetSpinner.setAdapter(rulesetAdapter);
            int position = 0;
            if (rulesetReference == null && ruleSetVersion == null) {
                position = rulesetSpinner.getSelectedItemPosition();
            } else if (ruleSetVersion != null) {
                init = true;
                RulesetEntity cursor;
                for (int i = 0; i < list.size(); i++) {
                    cursor = list.get(i);
                    if (cursor != null
                            && cursor.getReference().equals(rulesetReference)
                            && cursor.getVersion() != null
                            && Integer.parseInt(cursor.getVersion()) == ruleSetVersion) {
                        position = i;
                        break;
                    }
                    if (cursor != null && rulesetReference == null && viewModel.getSelectedRuleset() != null) {
                        if (cursor.getReference().equals(viewModel.getSelectedRuleset().getReference())
                                && cursor.getVersion() != null
                                && Integer.parseInt(cursor.getVersion()) == Integer.parseInt(viewModel.getSelectedRuleset().getVersion())) {
                            position = i;
                            break;
                        }
                    }


                }
            }
            rulesetSpinner.setSelection(position);
        }
    }

    @ItemSelect(R.id.rule_spinner)
    public void onRulesetSelected(boolean selected, int position) {
        Timber.d("Message=Selecting a ruleset;RulesetIndex=%d;IsInitialized=%b", position, init);
        RulesetEntity entity = rulesetAdapter.getItem(position);
        viewModel.setSelectedRuleset(entity);
        updateFragment(entity);
    }

    /**
     * triggers the display of the {@link BrowseEquipmentsFragment}
     */
    private void updateFragment(final @NonNull RulesetEntity ruleset) {

        Timber.d("Message=Updating fragment;RulesetId=%d;EquipmentRef=%s", ruleset.getId(), defaultEquipmentReference);


        if ((getChildFragmentManager().findFragmentById(equipmentFragment.getId())) == null) {
            BrowseEquipmentsFragment fragment = BrowseEquipmentsFragment_
                    .builder()
                    .rulesetId(ruleset.getId())
                    .defaultEquipmentReference(defaultEquipmentReference)
                    .build();

            System.err.println("OCARA : AUDIT NEW NEW FRAG " + rulesetReference + " " + ruleSetVersion + " " + defaultEquipmentReference);
            fragment.setRetainInstance(true);

            getChildFragmentManager()
                    .beginTransaction()
                    .replace(equipmentFragment.getId(), fragment, RULESET)
                    .commit();
            return;
        } else {
            System.err.println("OCARA : AUDIT UPDATE FRAG " + rulesetReference + " " + ruleSetVersion + " " + defaultEquipmentReference);
            ((BrowseEquipmentsFragment) getChildFragmentManager().findFragmentById(equipmentFragment.getId()))
                    .update(defaultEquipmentReference, ruleset.getId());
        }


//        getActivity().
    }

    @Override
    public void onDestroy() {
        viewModel.setSelectedRuleIdx(-1);
        viewModel.setSelectedEquipment(-1L);
        viewModel.setSelectedRuleset(null);
        super.onDestroy();
        System.err.println("OCARA : AUDIT  DESTROY");
    }
}
