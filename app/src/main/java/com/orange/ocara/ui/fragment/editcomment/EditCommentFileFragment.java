/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.fragment.editcomment;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.orange.ocara.R;
import com.orange.ocara.data.cache.model.CommentEntity;
import com.orange.ocara.ui.viewmodel.EditCommentViewModel;
import com.orange.ocara.ui.viewmodel.OcaraViewModelFactory;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.io.IOException;

import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

@EFragment(R.layout.activity_edit_comment_file)
public class EditCommentFileFragment extends EditCommentFragment{

    protected static final int REQUEST_PICK_SINGLE_FILE = 1;

    @ViewById(R.id.editCommentLayout)
    LinearLayout editCommentLayout;

    @ViewById(R.id.photo)
    ImageView imageView;

    private File tempAttachment;

    private String pictureFilename;

    @Click(R.id.change_image)
    void onChangeImage() {
        createAttachment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.activity_edit_comment_file, container, false);
        commentId = EditCommentFileFragment_Args.fromBundle(getArguments()).getCommentId();
        commentType = CommentEntity.getTypeFromInt(EditCommentFileFragment_Args.fromBundle(getArguments()).getCommentType());
        attachmentDirectory = EditCommentFileFragment_Args.fromBundle(getArguments()).getAttachmentDirectory();
        imageName = EditCommentFileFragment_Args.fromBundle(getArguments()).getImageName();
        name = EditCommentFileFragment_Args.fromBundle(getArguments()).getName();
        auditId = EditCommentFileFragment_Args.fromBundle(getArguments()).getAuditId();
        title = EditCommentFileFragment_Args.fromBundle(getArguments()).getTitle();
        subTitle = EditCommentFileFragment_Args.fromBundle(getArguments()).getSubTitle();

        editCommentViewModel = new ViewModelProvider(this, new OcaraViewModelFactory(getActivity()))
                .get(EditCommentViewModel.class);

        rootview.findViewById(R.id.editCommentLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard();
            }
        });
        loadComment();
//        setUpComment();

        return rootview;
    }


    @Override
    void onValidateComment() {
        if (comment.getAttachment() == null && tempAttachment != null) {
            try {
                pictureFilename = copyAsAttachment(tempAttachment);
                pictureFilename = "file:" + pictureFilename;

                // Save a file: path for use with ACTION_VIEW intents
                comment.setAttachment(pictureFilename);
            } catch (IOException e) {
                Timber.e(e, "Cannot store attachment %s to dir %s", tempAttachment, attachmentDirectory);
            }
        }

        // terminate
        super.onValidateComment();
    }

    @Override
    protected void createAttachment() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_PICK_SINGLE_FILE);
    }

    @OnActivityResult(REQUEST_PICK_SINGLE_FILE)
    void onImageSelected(int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            if(data != null) {
                Uri selectedImageUri = data.getData();

                // Get the path from the Uri
                final String path = getPathFromURI(selectedImageUri);
                if (path != null) {
                    tempAttachment = new File(path);
                    selectedImageUri = Uri.fromFile(tempAttachment);

                    try {
                        pictureFilename = copyAsAttachment(tempAttachment);
                        pictureFilename = "file:" + pictureFilename;

                        // Save a file: path for use with ACTION_VIEW intents
                        comment.setAttachment(pictureFilename);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                // Set the image in ImageView
                imageView.setImageURI(selectedImageUri);

                Timber.v("Message=On choosing image result;Path=%s;", pictureFilename);
                handleAttachment();
            }
        } else {
            Timber.e("Message=Failed on choosing image;ResultCode=%d;", resultCode);
            finish();
        }
    }

    @Override
    protected void handleAttachment() {
        imageView.setVisibility(View.VISIBLE);

        if (comment.getAttachment() != null) {
            // override pictureFilename value with those from comment
            pictureFilename = comment.getAttachment();
        }

        Uri uri = Uri.parse(pictureFilename);
        Timber.v("Message=Trying to load image;Icon=%s", pictureFilename);
        Picasso
                .with(getContext())
                .load(uri)
                .placeholder(R.color.black50)
                .fit()
                .into(imageView);

        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(uri);
        getContext().sendBroadcast(mediaScanIntent);
    }

    String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContext().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }
}
