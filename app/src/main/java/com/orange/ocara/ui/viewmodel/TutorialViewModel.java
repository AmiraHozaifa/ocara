package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import com.orange.ocara.business.BizConfig;
import com.orange.ocara.business.BizConfig_;
import com.orange.ocara.business.binding.ErrorBundle;
import com.orange.ocara.business.interactor.ChangeOnboardingStepTask;
import com.orange.ocara.business.interactor.CompleteOnboardingTask;
import com.orange.ocara.business.interactor.LoadOnboardingItemsTask;
import com.orange.ocara.business.interactor.SkipOnboardingTask;
import com.orange.ocara.business.interactor.UseCase;
import com.orange.ocara.business.interactor.UseCaseHandler;
import com.orange.ocara.business.model.OnboardingItemModel;

import java.util.Collections;
import java.util.List;

import timber.log.Timber;

public class TutorialViewModel extends OcaraViewModel {


    private final UseCaseHandler useCaseHandler;

    /**
     * see {@link CompleteOnboardingTask}
     */
    private final UseCase<CompleteOnboardingTask.CompleteOnboardingRequest, CompleteOnboardingTask.CompleteOnboardingResponse> completeUseCase;

    /**
     * see {@link LoadOnboardingItemsTask}
     */
    private final UseCase<LoadOnboardingItemsTask.LoadOnboardingItemsRequest, LoadOnboardingItemsTask.LoadOnboardingItemsResponse> loadUseCase;

    /**
     * see {@link SkipOnboardingTask}
     */
    private final UseCase<SkipOnboardingTask.SkipOnboardingRequest, SkipOnboardingTask.SkipOnboardingResponse> skipUseCase;

    /**
     * see {@link ChangeOnboardingStepTask}
     */
    private final UseCase<ChangeOnboardingStepTask.ChangeOnboardingStepRequest, ChangeOnboardingStepTask.ChangeOnboardingStepResponse> changeStepUseCase;

    /**
     * index of the initial item to display from the pager
     */
    private int currentPosition = 0;

    /**
     * the items of the tutorial
     */
    private List<OnboardingItemModel> list = Collections.emptyList();

    public TutorialViewModel(Context context) {
        super(context);
        BizConfig bizConfig = BizConfig_.getInstance_(context);
        this.useCaseHandler = BizConfig.USE_CASE_HANDLER;
        this.completeUseCase = bizConfig.completeOnboardingTask();
        this.loadUseCase = bizConfig.loadOnboardingItemsTask();
        this.skipUseCase = bizConfig.skipOnboardingTask();
        this.changeStepUseCase = bizConfig.changeOnboardingItemTask();
    }

    //    @Override
    public void completeOnboarding(UseCase.UseCaseCallback<CompleteOnboardingTask.CompleteOnboardingResponse> callback) {

        useCaseHandler.execute(completeUseCase, new CompleteOnboardingTask.CompleteOnboardingRequest(), callback);
    }

    //    @Override
    public void loadOnboarding(UseCase.UseCaseCallback<LoadOnboardingItemsTask.LoadOnboardingItemsResponse> callback) {

        useCaseHandler.execute(
                loadUseCase,
                new LoadOnboardingItemsTask.LoadOnboardingItemsRequest(),
                new UseCase.UseCaseCallback<LoadOnboardingItemsTask.LoadOnboardingItemsResponse>() {

                    @Override
                    public void onComplete(LoadOnboardingItemsTask.LoadOnboardingItemsResponse response) {
                        list = response.getItems();
                        currentPosition = 0;
                        callback.onComplete(response);
                    }

                    @Override
                    public void onError(ErrorBundle errors) {
                        list = Collections.emptyList();
                        currentPosition = 0;
                        callback.onError(errors);
                    }
                });
    }

    public void skipOnboarding(UseCase.UseCaseCallback<SkipOnboardingTask.SkipOnboardingResponse> callback) {

        useCaseHandler.execute(skipUseCase, new SkipOnboardingTask.SkipOnboardingRequest(), callback);
    }

    public void openNextStep(UseCase.UseCaseCallback<ChangeOnboardingStepTask.ChangeOnboardingStepResponse> callback) {

        useCaseHandler.execute(
                changeStepUseCase,
                new ChangeOnboardingStepTask.ChangeOnboardingStepRequest(currentPosition, currentPosition + 1, list.size() - 1),
                new UseCase.UseCaseCallback<ChangeOnboardingStepTask.ChangeOnboardingStepResponse>() {

                    @Override
                    public void onComplete(ChangeOnboardingStepTask.ChangeOnboardingStepResponse response) {

                        Timber.d("PresenterMessage=Changing step completed;NewIndex=%d;OldIndex=%d", response.getTargetPosition(), currentPosition);
                        currentPosition = response.getTargetPosition();
                        callback.onComplete(response);
                    }

                    @Override
                    public void onError(ErrorBundle errors) {

                        callback.onError(errors);
                    }
                });
    }

    public void notifyStepChanged(int newPosition, UseCase.UseCaseCallback<ChangeOnboardingStepTask.ChangeOnboardingStepResponse> callback) {

        useCaseHandler.execute(
                changeStepUseCase,
                new ChangeOnboardingStepTask.ChangeOnboardingStepRequest(currentPosition, newPosition, list.size() - 1),
                new UseCase.UseCaseCallback<ChangeOnboardingStepTask.ChangeOnboardingStepResponse>() {

                    @Override
                    public void onComplete(ChangeOnboardingStepTask.ChangeOnboardingStepResponse response) {

                        Timber.d("PresenterMessage=Changing step completed;NewIndex=%d;OldIndex=%d", response.getTargetPosition(), currentPosition);
                        currentPosition = response.getTargetPosition();
                        callback.onComplete(response);
                    }

                    @Override
                    public void onError(ErrorBundle errors) {

                        callback.onError(errors);
                    }
                });
    }

}
