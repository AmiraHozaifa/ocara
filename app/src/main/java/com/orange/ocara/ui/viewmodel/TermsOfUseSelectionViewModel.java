package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import com.orange.ocara.business.BizConfig;
import com.orange.ocara.business.BizConfig_;
import com.orange.ocara.business.interactor.AcceptTermsOfUseTask;
import com.orange.ocara.business.interactor.DeclineTermsOfUseTask;
import com.orange.ocara.business.interactor.UseCase;
import com.orange.ocara.business.interactor.UseCaseHandler;

public class TermsOfUseSelectionViewModel extends OcaraViewModel {

    private final UseCaseHandler useCaseHandler;

    private final UseCase<AcceptTermsOfUseTask.AcceptTermsOfUseRequest, AcceptTermsOfUseTask.AcceptTermsOfUseResponse> acceptanceTask;

    private final UseCase<DeclineTermsOfUseTask.DeclineTermsOfUseRequest, DeclineTermsOfUseTask.DeclineTermsOfUseResponse> declineTask;

    public TermsOfUseSelectionViewModel(Context context) {
        super(context);

        BizConfig bizConfig = BizConfig_.getInstance_(context);
        this.useCaseHandler = BizConfig.USE_CASE_HANDLER;
        acceptanceTask = bizConfig.acceptTermsOfUseTask();
        declineTask = bizConfig.declineTermsOfUseTask();
    }


    public void declineTerms(UseCase.UseCaseCallback<DeclineTermsOfUseTask.DeclineTermsOfUseResponse> callback) {
        useCaseHandler.execute(declineTask, new DeclineTermsOfUseTask.DeclineTermsOfUseRequest(), callback);
    }


    public void acceptTerms(UseCase.UseCaseCallback<AcceptTermsOfUseTask.AcceptTermsOfUseResponse> callback) {
        useCaseHandler.execute(acceptanceTask, new AcceptTermsOfUseTask.AcceptTermsOfUseRequest(), callback);
    }
}
