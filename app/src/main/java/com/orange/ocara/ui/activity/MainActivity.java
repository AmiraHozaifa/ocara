package com.orange.ocara.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.orange.ocara.BuildConfig;
import com.orange.ocara.R;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;

import timber.log.Timber;

import static androidx.navigation.Navigation.findNavController;
import static androidx.navigation.ui.NavigationUI.navigateUp;
import static androidx.navigation.ui.NavigationUI.setupActionBarWithNavController;
import static androidx.navigation.ui.NavigationUI.setupWithNavController;
import static timber.log.Timber.e;
import static timber.log.Timber.v;

public class MainActivity extends AppCompatActivity {
    DrawerLayout drawerLayout;
    NavigationView navigationView;

    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = findViewById(R.id.ocara_drawerLayout);
        navigationView = findViewById(R.id.navigationView);

//        ocaraNavController = new DestinationController();
        progressBar = findViewById(R.id.progress);
        setSupportActionBar(findViewById(R.id.toolbar));
        getSupportActionBar().setLogo(R.drawable.ic_app);
        setupNavigation();

    }

    public void setLoading(boolean isLoading) {
        progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        boolean b = super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.global_options_menu, menu);

        return b;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.action_about) {
//            AboutActivity_.intent(this).start();
            openAbout();
            return true;
        } else if (i == R.id.action_help) {
//            final String activityName = this.getClass().getName();
//            String pdfName = activityName.substring(29, activityName.length()) + ".pdf";
//            String pdfName = activityName + ".pdf";
//            final String pdfName = "Guide_de_formation_V18.0.0.pdf";
//            showHelp(pdfName);
            openHelpForChild();
            return true;
        } else if (i == R.id.action_terms) {
//            TermsOfUseReadingActivity_
//                    .intent(MainActivity.this)
//                    .start();
            openTermsViewOnly();
            return true;
        } else if (i == R.id.settingsFragment) {
//            SettingsActivity_.intent(this).start();
            return NavigationUI.onNavDestinationSelected(item, findNavController(this, R.id.nav_host_fragment)) ||
                    super.onOptionsItemSelected(item);
//            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onSupportNavigateUp() {

        return navigateUp(findNavController(this, R.id.nav_host_fragment), drawerLayout);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void setupNavigation() {
        NavController navController = findNavController(this, R.id.nav_host_fragment);

        // Update action bar to reflect navigation
        setupActionBarWithNavController(this, navController, drawerLayout);

        navigationView.setNavigationItemSelectedListener(
                item -> {
                    drawerLayout.closeDrawer(GravityCompat.START);
                    switch (item.getItemId()) {
                        case R.id.nav_new_audit_menu_item:
                            return true;
//                        case R.id.nav_list_audit_item:
//                            onAllAuditItemClicked();
//                            return true;
//                        case R.id.nav_show_rules_item:
//                            onAllRuleSetItemClicked();
//                            return true;
//                        case R.id.nav_show_help_item:
//                            final String pdfName = "Guide_de_formation_V18.0.0.pdf";
//                            showHelp(pdfName);
//                            openHelpFromDrawer();
//                            return true;
                        default:
                            return false;
                    }
                });


        setupWithNavController(navigationView, navController);
    }

    /**
     * New audit item clicked.
     */
//    protected void onNewAuditItemClicked() {
//        ocaraNavController.navigateToHome(this);
//    }
    protected void openHelpForChild() {
        String name = getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment).getChildFragmentManager()
                .getFragments().get(0).getClass().getName();
//        System.err.println("NAME =  " + name);
        openHelp(name);
    }

    protected void openHelpFromDrawer() {
        openHelp("");
    }

    protected void openHelp(String subject) {
        Bundle subjectBundle = new Bundle();
        subjectBundle.putString("subject", subject);
        findNavController(this, R.id.nav_host_fragment).navigate(R.id.helpFragment_, subjectBundle);
    }

    protected void openTermsViewOnly() {
        findNavController(this, R.id.nav_host_fragment).navigate(R.id.TermsOfUseViewFragment);
    }


    protected void openAbout() {
        findNavController(this, R.id.nav_host_fragment).navigate(R.id.AboutFragment);
    }

    /**
     * Show help item clicked.
     */
//    protected void showHelp(String pdfName) {
//
//        String locale = Locale.getDefault().getLanguage();
//        String folder;
//        File externalCacheDir = getExternalCacheDir();
//        if (locale.equals("en")) {
//            folder = "pdf/";
//        } else {
//            folder = "pdf-" + locale + "/";
//        }
//        v("ext " + externalCacheDir.getAbsolutePath() + " locale " + locale + " folder " + folder);
//
//        try (InputStream in = getAssets().open(folder + pdfName)) {
//
//            File file = new File(externalCacheDir, pdfName);
//            BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file));
//
//            copyFile(in, out);
//            in.close();
//            out.flush();
//            out.close();
//
//            Uri uri;
//            Intent intent = new Intent(Intent.ACTION_VIEW);
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && file.exists()) {
//                Timber.i("Message=Viewing a pdf file;FileAbsolutePath=%s;IsFile=%b;IsReadable=%b", file.getAbsolutePath(), file.isFile(), file.canRead());
//                uri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", file);
//                intent.setDataAndType(uri, "application/pdf");
//
//                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_NO_HISTORY);
//                startActivity(intent);
//            } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
//                uri = Uri.parse("file://" + externalCacheDir + File.separator + pdfName);
//                intent.setDataAndType(uri, "application/pdf");
//                startActivity(intent);
//            } else {
//                Toast.makeText(this.getApplicationContext(), "The file does not exist... ", Toast.LENGTH_SHORT).show();
//            }
//        } catch (Exception e) {
//            e(e, "Error message");
//        }
//    }
    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

}
