package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.orange.ocara.business.BizConfig;
import com.orange.ocara.business.BizConfig_;
import com.orange.ocara.business.binding.ErrorBundle;
import com.orange.ocara.business.interactor.RetrieveRulesetsTask;
import com.orange.ocara.business.interactor.SavePreferredRulesetTask;
import com.orange.ocara.business.interactor.UseCase;
import com.orange.ocara.business.model.RulesetModel;
import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.db.ModelManagerImpl_;
import com.orange.ocara.data.cache.model.AuditEntity;
import com.orange.ocara.data.cache.model.SiteEntity;
import com.orange.ocara.data.net.model.Ruleset;
import com.orange.ocara.ui.binding.AuditValidator;
import com.orange.ocara.ui.binding.AuthorNamePatternValidator;
import com.orange.ocara.ui.binding.BindingResult;
import com.orange.ocara.ui.binding.RulesetStateValidator;
import com.orange.ocara.ui.binding.SiteExistsValidator;
import com.orange.ocara.ui.binding.Validator;
import com.orange.ocara.ui.model.AuditFormUiModel;


import io.reactivex.Completable;

import java.util.List;

import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.orange.ocara.tools.ListUtils.newArrayList;

//@EBean(scope = EBean.Scope.Singleton)
public class CreateAuditViewModel extends ViewModel
//        implements CreateAuditContract.CreateAuditUserActionsListener
{


    private BizConfig bizConfig;
    private ModelManager modelManager;


    public SiteEntity currentSite = null;
//    public int expertiseLevel;

    public String auditName = "";
    public String authorName = "";

    public AuditEntity audit;
    public boolean isAuditValid = false;

    public MutableLiveData<List<RulesetModel>> rulesLiveData;
    /**
     * a {@link UseCase} for the retrieval of a {@link List} of {@link Ruleset}s
     */
    private UseCase<RetrieveRulesetsTask.RetrieveRulesetsRequest, RetrieveRulesetsTask.RetrieveRulesetsResponse> mRetrieveRulesetsTask;

    /**
     * a {@link UseCase} for saving a {@link Ruleset} preference
     */
    private final UseCase<SavePreferredRulesetTask.SavePreferredRulesetRequest, SavePreferredRulesetTask.SavePreferredRulesetResponse> mSavePreferredRulesetTask;

    /**
     * a bunch of {@link AuditValidator}s for the validation of an {@link AuditFormUiModel}
     */
    private final List<Validator<AuditFormUiModel>> auditValidators = newArrayList(new AuthorNamePatternValidator(), new RulesetStateValidator(), new SiteExistsValidator());


    public CreateAuditViewModel(Context context) {
        System.err.println("OCARA : AUDIT VIEW MODEL CONST");
        rulesLiveData = new MutableLiveData<>();
        this.bizConfig = BizConfig_.getInstance_(context);
        this.modelManager = ModelManagerImpl_.getInstance_(context);

        mRetrieveRulesetsTask = bizConfig.retrieveRulesetsTask();
        mSavePreferredRulesetTask = bizConfig.savePreferredRulesetTask();
        audit = new AuditEntity();
    }


    public ModelManager getModelManager() {
        return modelManager;
    }

    //    public void retrieveAudit() {
//        // creating new audit : retrieve last audits
//        List<AuditEntity> lastAudits = modelManager.getAllAudits("", SortCriteria.Type.DATE.build());
//
//        if (lastAudits.isEmpty()) {
//            this.audit = new AuditEntity();
//        } else {
//            this.audit = lastAudits.get(0);
//            this.audit.setName("");
//        }
//    }

    public boolean hasRules() {
        return (rulesLiveData.getValue() != null);
    }

    //    public List<RulesetModel> getRules() {
//        return rules;
//    }
    public void retrieveRuleSet() {

        Single.create(new SingleOnSubscribe<List<RulesetModel>>() {
            @Override
            public void subscribe(SingleEmitter<List<RulesetModel>> emitter) throws Exception {
                RetrieveRulesetsTask.RetrieveRulesetsRequest request = new RetrieveRulesetsTask.RetrieveRulesetsRequest();
                mRetrieveRulesetsTask.executeUseCase(request, new UseCase.UseCaseCallback<RetrieveRulesetsTask.RetrieveRulesetsResponse>() {
                    @Override
                    public void onComplete(RetrieveRulesetsTask.RetrieveRulesetsResponse response) {
                        Timber.d("ActivityMessage=Rulesets retrieval is successful;RulesetsCount=%d", response.getCount());
                        emitter.onSuccess(response.getItems());
//                        view.showRulesetList(response.getItems());
//                        view.enableRulesInfo();
//                        view.hideProgressDialog();
                    }

                    //                @Background
                    @Override
                    public void onError(ErrorBundle errors) {
                        Timber.d("ActivityMessage=Rulesets retrieval failed;ErrorMessage=%s", errors.getMessage());
                        emitter.onError(null);
//                        view.hideProgressDialog();
//                        view.showDownloadError();
                    }
                });
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).
                subscribe(RulesetEntities -> {
                    rulesLiveData.postValue(RulesetEntities);

                });

    }

    private Observable retrieveRulesetsObservable = Observable.create(new ObservableOnSubscribe<List<RulesetModel>>() {
        @Override
        public void subscribe(ObservableEmitter emitter) {

            RetrieveRulesetsTask.RetrieveRulesetsRequest request = new RetrieveRulesetsTask.RetrieveRulesetsRequest();
            mRetrieveRulesetsTask.executeUseCase(request, new UseCase.UseCaseCallback<RetrieveRulesetsTask.RetrieveRulesetsResponse>() {
                @Override
                public void onComplete(RetrieveRulesetsTask.RetrieveRulesetsResponse response) {
                    Timber.d("ActivityMessage=Rulesets retrieval is successful;RulesetsCount=%d", response.getCount());
                    rulesLiveData.postValue(response.getItems());
                    emitter.onNext(response.getItems());
                    emitter.onComplete();

//                        view.showRulesetList(response.getItems());
//                        view.enableRulesInfo();
//                        view.hideProgressDialog();
                }

                //                @Background
                @Override
                public void onError(ErrorBundle errors) {
                    Timber.d("ActivityMessage=Rulesets retrieval failed;ErrorMessage=%s", errors.getMessage());
                    emitter.onError(null);
//                        view.hideProgressDialog();
//                        view.showDownloadError();
                }
            });
        }
    });
    RulesetModel selectedRulSet;

    public RulesetModel getSelectedRulSet() {
        return selectedRulSet;
    }

    public Completable savePreferredRuleset(final RulesetModel selectedRulSet) {
        this.selectedRulSet = selectedRulSet;
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter emitter) throws Exception {

                mSavePreferredRulesetTask.executeUseCase(
                        new SavePreferredRulesetTask.SavePreferredRulesetRequest(selectedRulSet),
                        new UseCase.UseCaseCallback<SavePreferredRulesetTask.SavePreferredRulesetResponse>() {

                            @Override
                            public void onComplete(SavePreferredRulesetTask.SavePreferredRulesetResponse responseValue) {
                                Timber.d("Item saved as preference");
//                            view.hideProgressDialog();
//                            view.validateFields();
                                emitter.onComplete();
                            }

                            @Override
                            public void onError(ErrorBundle errors) {
                                Timber.e("Item could not be saved as preference");
//                            view.hideProgressDialog();
                                emitter.onError(null);
                            }
                        });
            }
        });
    }

    //    @Override
    public boolean validateAudit(AuditFormUiModel audit) {
        BindingResult errors = new BindingResult();
        for (Validator validator : auditValidators) {
            validator.validate(audit, errors);
        }

        if (errors.hasErrors()) {
            isAuditValid = false;
        } else {
            isAuditValid = true;
        }
        return isAuditValid;
    }
}
