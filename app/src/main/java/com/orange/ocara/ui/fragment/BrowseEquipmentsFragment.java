/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.orange.ocara.R;
import com.orange.ocara.business.binding.ErrorBundle;
import com.orange.ocara.business.interactor.LoadRulesTask;
import com.orange.ocara.business.interactor.UseCase;
import com.orange.ocara.business.model.RuleGroupModel;
import com.orange.ocara.business.model.RuleModel;
import com.orange.ocara.business.service.EquipmentService;
import com.orange.ocara.business.service.impl.EquipmentServiceImpl;
import com.orange.ocara.data.net.model.Equipment;
import com.orange.ocara.ui.BrowseRulesUiConfig;
//import com.orange.ocara.ui.adapter.RulesExpandableListAdapter;
//import com.orange.ocara.ui.adapter.RulesetEquipmentsAdapter;
import com.orange.ocara.ui.adapter.RulesExpandableRVAdapter;
import com.orange.ocara.ui.adapter.RulesetEquipmentsRvAdapter;
import com.orange.ocara.ui.viewmodel.BrowseRuleSetsViewModel;
import com.orange.ocara.ui.viewmodel.BrowseRulesetsViewModelFactory;
import com.thoughtbot.expandablerecyclerview.listeners.GroupExpandCollapseListener;
import com.thoughtbot.expandablerecyclerview.listeners.OnGroupClickListener;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.orange.ocara.tools.ListUtils.emptyList;

/**
 * {@link BaseFragment} dedicated to the display of equipments
 * and their related rules, according to a given ruleset
 */
@EFragment(R.layout.fragment_object_list_by_rules)
public class BrowseEquipmentsFragment extends BaseFragment
//        implements ListEquipmentsContract.ListEquipmentsView, BrowseRulesContract.BrowseRulesView
{

//    @ViewById(R.id.objects_listview)
//    ListView equipmentsListView;


    @ViewById(R.id.objects_listview)
    RecyclerView equipmentsListView;

    @ViewById(R.id.text_no_question)
    TextView noRuleTextView;

    @ViewById(R.id.rules_listview)
    RecyclerView rulesListView;
//    ExpandableListView rulesListView;

//    @Bean
//    RulesetEquipmentsAdapter equipmentsAdapter;

    @Bean
    RulesetEquipmentsRvAdapter equipmentsAdapter;

//    private RulesExpandableListAdapter rulesAdapter;

    private RulesExpandableRVAdapter rulesAdapter;

//    private BrowseRulesContract.BrowseRulesUserActionsListener browseRulesListener;

//    private ListEquipmentsContract.ListEquipmentsUserActionsListener listEquipmentsListener;

//    @Bean(EquipmentServiceImpl.class)
//    EquipmentService equipmentService;

//    @Bean(BrowseRulesUiConfig.class)
//    BrowseRulesUiConfig browseRulesUiConfig;

    @FragmentArg
    String defaultEquipmentReference;

    @FragmentArg
    Long rulesetId;
    ExpandableGroup expandedGrp = null;
    //    BrowseEquipmentsViewModel browseEquipmentViewModel;
    BrowseRuleSetsViewModel browseRuleSetsViewModel;

    /**
     * Initializing the listeners, aka the presenters
     */
    @AfterViews
    void initData() {

//        listEquipmentsListener = new ListEquipmentsPresenter(equipmentService, this);
//
//        browseRulesListener = browseRulesUiConfig.browseRulesActionsListener();

//        browseEquipmentViewModel = new ViewModelProvider(this, new BrowseEquipmentViewModelFactory(getActivity())).get(BrowseEquipmentsViewModel.class);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View v = super.onCreateView(inflater, container, savedInstanceState);
        System.err.println("OCARA : AUDIT BrowseEquipmentsFragment onCreateView");

        View v = inflater.inflate(R.layout.fragment_object_list_by_rules, container, false);

        if (savedInstanceState != null) {
            //probably orientation change
            expandedGrp = (ExpandableGroup) savedInstanceState.getParcelable("Grp");
        }

        equipmentsListView = v.findViewById(R.id.objects_listview);
        noRuleTextView = v.findViewById(R.id.text_no_question);
        rulesListView = v.findViewById(R.id.rules_listview);

//        browseEquipmentViewModel = new ViewModelProvider(this, new BrowseEquipmentViewModelFactory(getActivity())).get(BrowseEquipmentsViewModel.class);
        browseRuleSetsViewModel = new ViewModelProvider(getActivity(), new BrowseRulesetsViewModelFactory(getActivity())).get(BrowseRuleSetsViewModel.class);

        Timber.d("Message=Initializing the view;");

        // initializing the view that displays equipments
        equipmentsAdapter.setOnItemClickListener(equipmentClickListener());
        equipmentsListView.setLayoutManager(new LinearLayoutManager(getActivity()));

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(equipmentsListView.getContext(),
                DividerItemDecoration.VERTICAL);
        equipmentsListView.addItemDecoration(dividerItemDecoration);
        equipmentsListView.setAdapter(equipmentsAdapter);

        // initializing the view that has an expandable list of rules
//        rulesListView.setEmptyView(noRuleTextView);


        // initializing the content of the view
        showEquipments(browseRuleSetsViewModel.loadAllEquipmentsByRulesetId(rulesetId));

        return v;
    }

//    public void update(String defaultEquipmentReference, Long rulesetId) {
//        this.defaultEquipmentReference = defaultEquipmentReference;
//        this.rulesetId = rulesetId;
//        showEquipments(browseRuleSetsViewModel.loadAllEquipmentsByRulesetId(rulesetId));
//    }


    public void update(String defaultEquipmentReference, Long rulesetId) {
        this.defaultEquipmentReference = defaultEquipmentReference;
        this.rulesetId = rulesetId;
        showEquipments(browseRuleSetsViewModel.loadAllEquipmentsByRulesetId(rulesetId));
    }

    @AfterViews
    void initView() {

//        Timber.d("Message=Initializing the view;");
//
//        // initializing the view that displays equipments
//        equipmentsAdapter.setOnItemClickListener(equipmentClickListener());
//        equipmentsListView.setLayoutManager(new LinearLayoutManager(getActivity()));
//
//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(equipmentsListView.getContext(),
//                DividerItemDecoration.VERTICAL);
//        equipmentsListView.addItemDecoration(dividerItemDecoration);
//        equipmentsListView.setAdapter(equipmentsAdapter);
//
//        // initializing the view that has an expandable list of rules
////        rulesListView.setEmptyView(noRuleTextView);
//
//        // initializing the content of the view
//        showEquipments(browseEquipmentViewModel.loadAllEquipmentsByRulesetId(rulesetId));
    }

    public void showEquipments(List<Equipment> equipments) {
        if (equipments.isEmpty()) {
            showNoEquipments();
            return;
        }
        Timber.d("Message=Showing a list of equipments;DefaultObjectRef=%s;ObjectsCount=%d;", defaultEquipmentReference, equipments.size());

        // Updating the adapter
        equipmentsAdapter.update(equipments);

        // Selecting the current item
        int currentPosition = equipmentsAdapter.getPositionByReference(defaultEquipmentReference);

        if (currentPosition < 0) {
            currentPosition = equipmentsAdapter.getPositionById(browseRuleSetsViewModel.getSelectedEquipment());
            System.err.println("OCARA : AUDIT BrowseEquipmentsFragment equipmet " + browseRuleSetsViewModel.getSelectedEquipment() + " " + currentPosition);
            if (currentPosition < 0)
                currentPosition = 0;
        }

        equipmentsAdapter.setSelection(currentPosition);
//        equipmentsListView.setItemChecked(currentPosition, true);
        equipmentsListView.getLayoutManager().scrollToPosition(currentPosition);


        // Refreshing the subsequent questions
        Equipment equipment = equipmentsAdapter.getItem(currentPosition);
        Timber.d(
                "Message=Retrieving questions for selected equipment;ObjectDescriptionRef=%s;ObjectDescriptionName=%s",
                equipment.getReference(), equipment.getName());

        loadRules(equipment.getId());
    }

    public void showRules(List<RuleGroupModel> groups, List<RuleModel> rules) {
        Timber.d("Message=Showing a list of questions;ObjectRef=%s;QuestionsCount=%d;", defaultEquipmentReference, groups.size());
//        List<OrderedRuleGroupUiModel> groups = new ArrayList<>();

        rulesAdapter = RulesExpandableRVAdapter.build(groups, rules, this);
        rulesAdapter.setOnGroupClickListener(new OnGroupClickListener() {
            @Override
            public boolean onGroupClick(int flatPos) {
                System.err.println("OCARA : AUDIT GRPOPEN " + flatPos);
                if (!rulesAdapter.isGroupExpanded(flatPos)) {
                    System.err.println("OCARA : AUDIT GRPOPEN EXP" + flatPos);
                    browseRuleSetsViewModel.setSelectedRuleIdx(flatPos);
                } else {
                    System.err.println("OCARA : AUDIT GRPOPEN NOT EXP" + flatPos);
                    browseRuleSetsViewModel.setSelectedRuleIdx(-1);
                }
                return true;
            }
        });
//        rulesAdapter.setOnGroupExpandCollapseListener(new GroupExpandCollapseListener() {
//            @Override
//            public void onGroupExpanded(ExpandableGroup group) {
//                expandedGrp = group;
//                System.err.println("OCARA : AUDIT GRPOPEN "+ group.getTitle());
//            }
//
//            @Override
//            public void onGroupCollapsed(ExpandableGroup group) {
//                expandedGrp = null;
//            }
//        });
        rulesListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        rulesListView.setAdapter(rulesAdapter);
        if (browseRuleSetsViewModel.getSelectedRuleIdx() != -1) {
            System.err.println("OCARA : AUDIT GRPOPEN EXPAND" + browseRuleSetsViewModel.getSelectedRuleIdx());
            rulesAdapter.toggleGroup(browseRuleSetsViewModel.getSelectedRuleIdx());
        } else {
            System.err.println("OCARA : AUDIT GRPOPEN DONTTT EXPAND" + browseRuleSetsViewModel.getSelectedRuleIdx());
        }

    }

//    public void showNoRules() {
//        Timber.d("Message=Showing no rules;ObjectRef=%s;", defaultEquipmentReference);
//        rulesAdapter.reset();
//    }

    public void showNoEquipments() {
        Timber.d("Message=Showing no equipment;ObjectRef=%s;", defaultEquipmentReference);
        equipmentsAdapter.update(emptyList());
    }

    /**
     * retrieves a callback dedicated to the selection of an item in a list of equipments
     *
     * @return an instance of {@link AdapterView.OnItemClickListener}
     * //
     */

    private View.OnClickListener equipmentClickListener() {

        return (clickedView) -> {
//            Timber.d("Message=Clicked on an item;Position=%d;Index=%d", positionOfTheClickedView, positionOfTheItem);

            Equipment equipment = (Equipment) equipmentsAdapter.getItem((int) clickedView.getTag());
            browseRuleSetsViewModel.setSelectedRuleIdx(-1);
            loadRules(equipment.getId());
        };
    }
//    private AdapterView.OnItemClickListener equipmentClickListener() {
//
//        return (parentView, clickedView, positionOfTheClickedView, positionOfTheItem) -> {
//            Timber.d("Message=Clicked on an item;Position=%d;Index=%d", positionOfTheClickedView, positionOfTheItem);
//
//            Equipment equipment = (Equipment) parentView.getItemAtPosition(positionOfTheClickedView);
//            loadRules(equipment.getId());
//        };
//    }

    /**
     * triggers the loading of the content related to the given identifier
     *
     * @param equipmentId an identifier for an Equipment (aka ObjectDescription)
     */
    private void loadRules(long equipmentId) {
//        browseEquipmentViewModel.loadAllRulesByEquipmentId(this.rulesetId, equipmentId);
        browseRuleSetsViewModel.setSelectedEquipment(equipmentId);
        browseRuleSetsViewModel.loadRules(this.rulesetId, equipmentId).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new DisposableSingleObserver<LoadRulesTask.LoadRulesResponse>() {
            @Override
            public void onSuccess(LoadRulesTask.LoadRulesResponse loadRulesResponse) {
                showRules(loadRulesResponse.getGroups(), loadRulesResponse.getRules());
                this.dispose();
            }

            @Override
            public void onError(Throwable e) {
                this.dispose();
            }
        });
//        browseEquipmentViewModel.loadRulesResponse.subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread()).subscribe(new DisposableSingleObserver<LoadRulesTask.LoadRulesResponse>() {
//            @Override
//            public void onSuccess(LoadRulesTask.LoadRulesResponse loadRulesResponse) {
//                showRules(loadRulesResponse.getGroups(), loadRulesResponse.getRules());
//                this.dispose();
//            }
//
//            @Override
//            public void onError(Throwable e) {
//                this.dispose();
//            }
//        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            //probably orientation change
            expandedGrp = (ExpandableGroup) savedInstanceState.getParcelable("Grp");
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("Grp", expandedGrp);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
