/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */
package com.orange.ocara.ui.fragment.editcomment;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.orange.ocara.R;
import com.orange.ocara.data.cache.model.CommentEntity;
import com.orange.ocara.ui.activity.EditCommentActivity;
import com.orange.ocara.ui.dialog.AudioRecorderDialog;
import com.orange.ocara.ui.fragment.AudioPlayerFragment;
import com.orange.ocara.ui.fragment.AudioPlayerFragment_;
import com.orange.ocara.ui.viewmodel.EditCommentViewModel;
import com.orange.ocara.ui.viewmodel.OcaraViewModelFactory;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentById;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.io.IOException;

import timber.log.Timber;

import static android.app.Activity.RESULT_OK;


@EFragment(R.layout.activity_edit_comment_audio)
public class EditCommentAudioFragment extends EditCommentFragment implements AudioRecorderDialog.AudioRecorderDialogListener {


    private static final int RECORD_AUDIO_REQUEST_CODE = 2;
    @ViewById(R.id.editCommentLayout)
    LinearLayout editCommentLayout;
    @ViewById(R.id.validate_comment)
    Button validateComment;
    @ViewById(R.id.start_playing_recording)
    Button startPlayingRecordingButton;
//    @FragmentById(R.id.comment_audio_player)
//    AudioPlayerFragment audioPlayerFragment;
    /**
     * The file recorded
     */
    private File tempAttachment;
    /**
     * the file to play (from recorder or attachment)
     */
    private String audioFilename;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.activity_edit_comment_audio, container, false);

        validateComment = rootview.findViewById(R.id.validate_comment);
        startPlayingRecordingButton = rootview.findViewById(R.id.start_playing_recording);
//        audioPlayerFragment = (AudioPlayerFragment_) getChildFragmentManager().findFragmentById(R.id.comment_audio_player);

        commentId = EditCommentAudioFragment_Args.fromBundle(getArguments()).getCommentId();
        commentType = CommentEntity.getTypeFromInt(EditCommentAudioFragment_Args.fromBundle(getArguments()).getCommentType());
        attachmentDirectory = EditCommentAudioFragment_Args.fromBundle(getArguments()).getAttachmentDirectory();
        imageName = EditCommentAudioFragment_Args.fromBundle(getArguments()).getImageName();
        name = EditCommentAudioFragment_Args.fromBundle(getArguments()).getName();
        auditId = EditCommentAudioFragment_Args.fromBundle(getArguments()).getAuditId();
        title = EditCommentAudioFragment_Args.fromBundle(getArguments()).getTitle();
        subTitle = EditCommentAudioFragment_Args.fromBundle(getArguments()).getSubTitle();

        editCommentViewModel = new ViewModelProvider(this, new OcaraViewModelFactory(getActivity()))
                .get(EditCommentViewModel.class);

        rootview.findViewById(R.id.editCommentLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard();
            }
        });
        loadComment();
//        setUpComment();
        return rootview;
    }

    @Click(resName = "start_playing_recording")
    void startPlayingRecording() {
        launchRecorderCustom();
    }

    @Click(resName = "validate_comment")
    void onValidateComment() {

        if (!hasAttachment() && tempAttachment != null) {

            // copy recorded attachment in correct audit folder

            String attachmentFileName = null;
            try {

                String shortFilename = makeTimestampedFilename("REC") + ".amr";
                attachmentFileName = copyAsAttachment(tempAttachment, shortFilename);
                attachmentFileName = "file:" + attachmentFileName;
                tempAttachment.delete();

            } catch (IOException e) {
                Timber.e(e, "Cannot store attachment %s to dir %s", tempAttachment, attachmentDirectory);
            }
            comment.setAttachment(attachmentFileName);
        }
        // terminate
        super.onValidateComment();
    }

    @Override
    protected void createAttachment() {

        androidx.fragment.app.FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.hide(getChildFragmentManager().findFragmentById(R.id.comment_audio_player));
        transaction.commit();
        validateComment.setEnabled(false);

        // new :
        launchRecorder();

    }


    @Override
    protected void handleAttachment() {

        if (comment.getAttachment() != null) {
            // override recordFilename value with those from comment
            audioFilename = comment.getAttachment();
        }
        Uri audioAttachment = Uri.parse(audioFilename);
//        audioPlayerFragment.setUri(audioAttachment);
        ((AudioPlayerFragment)getChildFragmentManager().findFragmentById(R.id.comment_audio_player)).setUri(audioAttachment);
        androidx.fragment.app.FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.show(getChildFragmentManager().findFragmentById(R.id.comment_audio_player));
        transaction.commit();
        validateComment.setEnabled(true);

    }


    @OnActivityResult(RECORD_AUDIO_REQUEST_CODE)
    void onAudioRecorded(int resultCode, Intent intent) {
        if (resultCode == RESULT_OK) {
            String sourcePath = getRealPathFromURI(intent.getData());
            tempAttachment = new File(sourcePath);
            audioFilename = sourcePath;
            handleAttachment();
        } else {
            finish();
        }
    }


    /**
     * from:
     * http://stackoverflow.com/questions/3401579/get-filename-and-path-from-uri-from-mediastore
     */
    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void launchRecorder() {

        final Intent intent = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);

        if (intent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivityForResult(intent, RECORD_AUDIO_REQUEST_CODE);
        } else {
            startPlayingRecordingButton.setVisibility(View.VISIBLE);
        }
    }


    private void launchRecorderCustom() {
        try {
            tempAttachment = createAudioFile();
            FragmentManager fm = getChildFragmentManager();
            startPlayingRecordingButton.setVisibility(View.GONE);
            AudioRecorderDialog audioRecorderDialog = AudioRecorderDialog.newInstance(tempAttachment.getAbsolutePath());
            audioRecorderDialog.show(fm, "fragment_edit_name");
        } catch (IOException e) {
            Timber.e(e, "Cannot create audio attachment ");
        }
    }

    @Override
    public void onRecordDialogDismiss(AudioRecorderDialog recordDialog) {
        audioFilename = tempAttachment.getAbsolutePath();
        handleAttachment();

    }

    private File createAudioFile() throws IOException {
        // Create an image file name
        String imageFileName = makeTimestampedFilename("REC");
        File storageDir = Environment.getExternalStorageDirectory();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".amr",         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }

    private boolean hasAttachment() {
        return comment.getAttachment() != null;
    }
}
