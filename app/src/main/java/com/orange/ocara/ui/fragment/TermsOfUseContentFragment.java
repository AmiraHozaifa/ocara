package com.orange.ocara.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.orange.ocara.R;

public class TermsOfUseContentFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView_ = inflater.inflate(R.layout.fragment_terms_of_use_content, container, false);
        contentView_.findViewById(R.id.gen_info_title_txt).setOnClickListener(new TermsOfUseContentFragment.onTextClickListener(contentView_.findViewById(R.id.gen_info_content_txt)));
        contentView_.findViewById(R.id.item1_title_txt).setOnClickListener(new TermsOfUseContentFragment.onTextClickListener(contentView_.findViewById(R.id.item1_content_txt)));
        contentView_.findViewById(R.id.item2_title_txt).setOnClickListener(new TermsOfUseContentFragment.onTextClickListener(contentView_.findViewById(R.id.item2_content_txt)));
        contentView_.findViewById(R.id.item3_title_txt).setOnClickListener(new TermsOfUseContentFragment.onTextClickListener(contentView_.findViewById(R.id.item3_content_txt)));
        contentView_.findViewById(R.id.item4_title_txt).setOnClickListener(new TermsOfUseContentFragment.onTextClickListener(contentView_.findViewById(R.id.item4_content_txt)));
        contentView_.findViewById(R.id.item5_title_txt).setOnClickListener(new TermsOfUseContentFragment.onTextClickListener(contentView_.findViewById(R.id.item5_content_txt)));
        contentView_.findViewById(R.id.item6_title_txt).setOnClickListener(new TermsOfUseContentFragment.onTextClickListener(contentView_.findViewById(R.id.item6_content_txt)));
        contentView_.findViewById(R.id.item7_title_txt).setOnClickListener(new TermsOfUseContentFragment.onTextClickListener(contentView_.findViewById(R.id.item7_content_txt)));
        contentView_.findViewById(R.id.item8_title_txt).setOnClickListener(new TermsOfUseContentFragment.onTextClickListener(contentView_.findViewById(R.id.item8_content_txt)));
        contentView_.findViewById(R.id.item9_title_txt).setOnClickListener(new TermsOfUseContentFragment.onTextClickListener(contentView_.findViewById(R.id.item9_content_txt)));
        contentView_.findViewById(R.id.item10_title_txt).setOnClickListener(new TermsOfUseContentFragment.onTextClickListener(contentView_.findViewById(R.id.item10_content_txt)));
        contentView_.findViewById(R.id.item11_title_txt).setOnClickListener(new TermsOfUseContentFragment.onTextClickListener(contentView_.findViewById(R.id.item11_content_txt)));

        return contentView_;
    }

    class onTextClickListener implements View.OnClickListener {
        View viewToToggleVisibility;

        public onTextClickListener(View viewToToggleVisibility) {
            this.viewToToggleVisibility = viewToToggleVisibility;
        }

        @Override
        public void onClick(View v) {
            if (viewToToggleVisibility.getVisibility() == View.VISIBLE) {
                viewToToggleVisibility.setVisibility(View.GONE);
                ((TextView) v).setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_chevron_right_small, 0);
            } else {
                viewToToggleVisibility.setVisibility(View.VISIBLE);
                ((TextView) v).setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_chevron_down_small, 0);
            }

        }
    }
}