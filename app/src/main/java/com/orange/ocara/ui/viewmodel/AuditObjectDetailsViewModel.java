/*
 * Copyright (c) 2019 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 * distribute, sublicense, create a derivative work, and/or sell copies of the
 * Software in any work that is designed, intended, or marketed for pedagogical or
 * instructional purposes related to programming, coding, application development,
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works,
 * or sale is expressly withheld.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.orange.ocara.business.service.RuleSetService;
import com.orange.ocara.business.service.impl.RuleSetServiceImpl_;
import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.db.ModelManagerImpl_;
import com.orange.ocara.data.cache.model.AuditObjectEntity;
import com.orange.ocara.data.net.model.EquipmentEntity;
import com.orange.ocara.data.net.model.IllustrationEntity;
import com.orange.ocara.ui.tools.RefreshStrategy;

import java.util.List;


public class AuditObjectDetailsViewModel extends OcaraViewModel {
    protected Context context;
    ModelManager modelManager;
    RuleSetService mRuleSetService;


    Long auditObjectId;
    public MutableLiveData<AuditObjectEntity> auditObjectLiveData = new MutableLiveData<>();

    public AuditObjectDetailsViewModel(Context context, Long auditObjectId) {
        super(context);
        modelManager = ModelManagerImpl_.getInstance_(context);
        mRuleSetService = RuleSetServiceImpl_.getInstance_(context);
        this.auditObjectId = auditObjectId;
        refreshAuditObjBackground();
    }

    public AuditObjectEntity getCurrentAuditObject() {
        return auditObjectLiveData.getValue();
    }

    public void updateAuditObjectChildren(final AuditObjectEntity parent, List<EquipmentEntity> childrenToCreate, List<AuditObjectEntity> childrenToRemove) {
        modelManager.updateAuditObjectChildren(parent, childrenToCreate, childrenToRemove);
//        modelManager.refresh(audit, strategy);
//        auditRefreshed();
        refreshAuditObjBackground();
    }
    private static final RefreshStrategy STRATEGY = RefreshStrategy.builder().depth(RefreshStrategy.DependencyDepth.AUDIT_OBJECT).commentsNeeded(true).build();
    protected static final RefreshStrategy RULE_REFRESH_STRATEGY = RefreshStrategy.ruleAnswerRefreshStrategy();

    //@Background
    void refreshAuditObjBackground() {
        AuditObjectEntity audit = modelManager.getAuditObject(auditObjectId);
        modelManager.refresh(audit, RefreshStrategy.builder().commentsNeeded(true).build());
        modelManager.refresh(audit, STRATEGY);
        modelManager.refresh(audit, RULE_REFRESH_STRATEGY);

        auditObjectLiveData.postValue(audit); //  TODO when changed to BE use post
//        refreshAuditFinished();
    }

    public EquipmentEntity getAuditObjectDefinition() {
        return mRuleSetService.getObjectDescriptionFromRef(auditObjectLiveData.getValue().getAudit().getRuleSetRef(),
                auditObjectLiveData.getValue().getAudit().getRuleSetVersion(), auditObjectLiveData.getValue().getObjectDescriptionId());

    }

    public List<IllustrationEntity> getAuditObjectIllustrations() {
        return mRuleSetService.getIllutrationsFormRef(getCurrentAuditObject().getAudit().getRuleSetRef(),
                getCurrentAuditObject().getAudit().getRuleSetVersion(), getAuditObjectDefinition().getIllustration());
    }

    public String getAttachmentDirectory() {
        return modelManager.getAttachmentDirectory(auditObjectId).getAbsolutePath();

    }

}
