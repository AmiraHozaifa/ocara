/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.PopupMenu;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.orange.ocara.R;
import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.model.AuditEntity;
import com.orange.ocara.data.cache.model.SortCriteria;
import com.orange.ocara.ui.activity.ListAuditActivity;
import com.orange.ocara.ui.view.AuditItemView;
import com.orange.ocara.ui.view.AuditItemView_;

import java.util.Collection;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import static com.orange.ocara.tools.ListUtils.emptyList;

/**
 * adapter for a {@link List} of {@link AuditEntity}s
 * <p>
 * Implements {@link ItemListAdapter}
 */
public class AuditRVAdapter
        extends ItemRvAdapter<AuditEntity, AuditRVAdapter.AuditViewHolder> {

    public class AuditViewHolder extends RecyclerView.ViewHolder {

        AuditItemView auditView;

        public AuditViewHolder(@NonNull View itemView) {
            super(itemView);
            auditView = (AuditItemView) itemView;
        }

        public void bind(int position) {
            AuditEntity entity = getItem(position);
            auditView.bind(entity);

            if (entity.getId() == selectedAuditId) {
                auditView.setBackgroundColor(context.getResources().getColor(R.color.orange_light));
            } else {
                auditView.setBackgroundColor(context.getResources().getColor(R.color.transparent));
            }

            auditView.getExportAuditDocx().setOnClickListener(v -> mAuditListAdapterListener.onExportAuditToWord(entity));

            auditView.getMore().setOnClickListener(v -> {
                PopupMenu popupMenu = new PopupMenu(context, v);
                if (AuditEntity.Status.TERMINATED.equals(entity.getStatus())) {
                    popupMenu.inflate(R.menu.list_all_audit_terminated);
                } else {
                    popupMenu.inflate(R.menu.list_all_audit_in_progress);
                }
                popupMenu.setOnMenuItemClickListener(item -> mAuditListAdapterListener.onAuditSubMenuItemClick(item, position));
                popupMenu.show();
            });

            auditView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSelectedAuditId(entity.getId());
                    v.setTag(position);
                    mAuditListAdapterListener.onAuditListItemClicked(entity);
                }
            });
        }
    }

    /**
     * a {@link android.content.Context} where the adapter is applied
     */
//    private ListAuditActivity mListAuditActivity;

    /**
     * a repository
     */
    private LayoutInflater mInflater;
    Context context;
    //    private ModelManager mModelManager;
//    private Filter filter;
    private AuditRVAdapter.AuditListAdapterListener mAuditListAdapterListener;


    private long selectedAuditId = 0;

//    @Getter
//    private String queryFilter = "";
//
//    @Getter
//    private SortCriteria sortCriteria = SortCriteria.Type.STATUS.build();

    /**
     * instantiate
     *
     * @param context                  a {@link android.content.Context}
     *                                 //     * @param modelManager             a repository
     * @param auditListAdapterListener a listener
     */
    public AuditRVAdapter(final Context context /*, ModelManager modelManager*/, AuditListAdapterListener auditListAdapterListener) {
//        mListAuditActivity = listAuditActivity;
        this.context = context;
        mInflater = LayoutInflater.from(context);
//        mModelManager = modelManager;
        mAuditListAdapterListener = auditListAdapterListener;
    }

    @NonNull
    @Override
    public AuditViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View itemView = mInflater.inflate(R.layout.audit_item, parent, false);

        AuditItemView itemView = AuditItemView_.build(parent.getContext());
        itemView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));
        return new AuditViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(AuditViewHolder holder, int position) {
//        AuditEntity audit = getItem(position);
        holder.bind(position);


    }

    public long getSelectedAuditId() {
        return selectedAuditId;
    }

    public void setSelectedAuditId(long selectedAuditId) {
        this.selectedAuditId = selectedAuditId;
    }

/**
 * updates the content of the adapter with default parameters
 */
//    public void refresh() {
//        refresh(queryFilter, sortCriteria);
//    }
//
//    /**
//     * updates the content of the adapter
//     *
//     * @param sortCriteria a sorting attribute
//     */
//    public void refresh(SortCriteria sortCriteria) {
//        refresh(queryFilter, sortCriteria);
//    }
//
//    /**
//     * updates the content of the adapter
//     *
//     * @param query a filtering string
//     */
//    public void refresh(String query) {
//        refresh(query, sortCriteria);
//    }

    /**
     * updates the content of the adapter
     *
     * @param queryFilter  a filtering string
     * @param sortCriteria a sorting attribute
     */
//    public void refresh(String queryFilter, SortCriteria sortCriteria) {
//        this.queryFilter = queryFilter;
//        this.sortCriteria = sortCriteria;
//
//        getFilter().filter(queryFilter);
//
//        mAuditListAdapterListener.refreshAuditList(objects);
//    }

//    private Filter getFilter() {
//        if (filter == null) {
//            filter = new AuditFilter();
//        }
//
//        return filter;
//    }

    /**
     * Behaviour of a listener on the adapter
     */
    public interface AuditListAdapterListener {

        /**
         * Updates the content with the given elements
         *
         * @param audits a {@link List} of {@link AuditEntity}s
         */
        void refreshAuditList(List<AuditEntity> audits);

        void onExportAuditToWord(final AuditEntity auditEntity);

        boolean onAuditSubMenuItemClick(MenuItem item, final int position);

        void onAuditListItemClicked(AuditEntity audit);
    }

    /**
     * Internal user name Filter.
     */
//    private class AuditFilter extends Filter {
//
//        @Override
//        protected FilterResults performFiltering(CharSequence constraint) {
//            FilterResults results = new FilterResults();
//
//            if (constraint != null) {
//                List<AuditEntity> audits = mModelManager.getAllAudits(constraint.toString(), sortCriteria);
//
//                results.values = audits;
//                results.count = audits.size();
//            }
//
//            return results;
//        }
//
//        @Override
//        protected void publishResults(CharSequence constraint, FilterResults results) {
//            final Collection<AuditEntity> values = (Collection<AuditEntity>) results.values;
//            update(values != null ? values : emptyList());
//            if (results.count == 0) {
//                notifyDataSetChanged();
//            }
//        }
//    }

}
