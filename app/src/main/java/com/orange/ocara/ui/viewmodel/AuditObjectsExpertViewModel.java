/*
 * Copyright (c) 2019 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 * distribute, sublicense, create a derivative work, and/or sell copies of the
 * Software in any work that is designed, intended, or marketed for pedagogical or
 * instructional purposes related to programming, coding, application development,
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works,
 * or sale is expressly withheld.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.orange.ocara.ui.viewmodel;


import android.content.Context;
import android.util.Pair;

import androidx.lifecycle.MutableLiveData;

import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.db.ModelManagerImpl_;
import com.orange.ocara.data.cache.model.AuditEntity;
import com.orange.ocara.data.cache.model.AuditObjectEntity;
import com.orange.ocara.data.cache.model.QuestionAnswerEntity;
import com.orange.ocara.data.cache.model.ResponseModel;
import com.orange.ocara.data.cache.model.RuleAnswerEntity;
import com.orange.ocara.ui.tools.RefreshStrategy;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.observers.DisposableCompletableObserver;

public class AuditObjectsExpertViewModel extends AuditObjectsViewModel {


    private int currentObjectIdx;
    AuditObjectEntity currentAuditObject;

    public AuditObjectsExpertViewModel(Context context, long[] selectedObjects) {
        super(context, selectedObjects);
        selectObject(0);
    }

    public void selectObject(int idx) {
        currentObjectIdx = idx;
        currentAuditObject = modelManager.getAuditObject(selectedObjects[currentObjectIdx]);
        computeQuestions();
    }

    private void computeQuestions() {
        Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(CompletableEmitter emitter) throws Exception {
                getQuestionOfCurrentObject();
                emitter.onComplete();
            }
        }).subscribe(new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                questionsComputed();
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }
        });
    }

    void getQuestionOfCurrentObject() {
        List<QuestionAnswerEntity> questionAnswers = getQuestionsOfObject(currentObjectIdx);
        allQuestions.clear();
        allQuestions.addAll(questionAnswers);
        questionListLiveData.setValue(allQuestions);
    }

    List<QuestionAnswerEntity> getQuestionsOfObject(int position) {
        List<QuestionAnswerEntity> questionAnswers = new ArrayList<>();

        AuditObjectEntity selectedAuditObject = modelManager.getAuditObject(selectedObjects[position]);
        modelManager.refresh(selectedAuditObject, RULE_REFRESH_STRATEGY);


        final List<QuestionAnswerEntity> questionAnswers1 = selectedAuditObject.getQuestionAnswers();
        questionAnswers.addAll(questionAnswers1);

        // add all characteristics questions as well
        for (AuditObjectEntity characteristic : selectedAuditObject.getChildren()) {
            questionAnswers.addAll(characteristic.getQuestionAnswers());
        }

        return questionAnswers;
    }

    void questionsComputed() {
//        List<QuestionAnswerEntity> questionAnswers = new ArrayList<>();
//        questionAnswers.addAll(allQuestions);
        for (QuestionAnswerEntity questionAnswer : allQuestions) {
            // pre-set all rules to 'doubt' value
            ResponseModel response = questionAnswer.getResponse();
            if (ResponseModel.NO_ANSWER.equals(response)) {
                for (RuleAnswerEntity ruleAnswer : questionAnswer.getRuleAnswers()) {
                    updateRuleAnswer(ruleAnswer, ResponseModel.OK);
                }
            }
        }
        questionListLiveData.setValue(allQuestions);
    }


    public void resetAuditObject() {

        for (QuestionAnswerEntity questionAnswer : allQuestions) {
            // pre-set all rules to 'OK' value
            ResponseModel response = questionAnswer.getResponse();
            if (ResponseModel.NO_ANSWER.equals(response)) {
                for (RuleAnswerEntity ruleAnswer : questionAnswer.getRuleAnswers()) {
//                    ruleAnswer.updateResponse(ResponseModel.OK);
                    updateRuleAnswer(ruleAnswer, ResponseModel.OK);
                }
            }
        }
        questionListLiveData.setValue(allQuestions);
    }


    private AuditObjectEntity getQuestionAuditObject(QuestionAnswerEntity currentQuestionAnswer) {
        AuditObjectEntity questionAnswerAuditObject = currentQuestionAnswer != null ? currentQuestionAnswer.getAuditObject() : null;
        if (questionAnswerAuditObject != null) {
            while (questionAnswerAuditObject.getParent() != null) {
                questionAnswerAuditObject = questionAnswerAuditObject.getParent();
            }
        }
        return questionAnswerAuditObject;
    }

    @Override
    public AuditObjectEntity getCurrentAuditObject() {
        return currentAuditObject;
    }

    public int getCurrentObjectIdx() {
        return currentObjectIdx;
    }


    public boolean isLastItemSelected() {
        return (currentObjectIdx >= (selectedObjects.length - 1));
    }

    @Override
    public boolean isFirstItemSelected() {
        return (currentObjectIdx <= 0);
    }


}
