/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.viewpager.widget.ViewPager;

import com.orange.ocara.R;
import com.orange.ocara.business.service.RuleSetService;
import com.orange.ocara.business.service.impl.RuleSetServiceImpl;
import com.orange.ocara.data.cache.model.AuditEntity;
import com.orange.ocara.data.cache.model.AuditObjectEntity;
import com.orange.ocara.data.cache.model.CommentEntity;
import com.orange.ocara.data.cache.model.ResponseModel;
import com.orange.ocara.data.net.model.EquipmentCategoryEntity;
import com.orange.ocara.data.net.model.EquipmentEntity;
import com.orange.ocara.data.net.model.RulesetEntity;
import com.orange.ocara.tools.StringUtils;
import com.orange.ocara.ui.adapter.AuditObjectsAdapter;
import com.orange.ocara.ui.adapter.CategoryPagerAdapter;
import com.orange.ocara.ui.dialog.NotificationDialogBuilder;
import com.orange.ocara.ui.dialog.OcaraDialogBuilder;
import com.orange.ocara.ui.dialog.SelectAuditObjectCharacteristicsDialogBuilder;
import com.orange.ocara.ui.routing.DestinationController;
import com.orange.ocara.ui.routing.Navigation;
import com.orange.ocara.ui.tools.RefreshStrategy;
import com.orange.ocara.ui.view.AuditObjectsView;
import com.orange.ocara.ui.view.BadgeView;
import com.orange.ocara.ui.view.PagerSlidingTabStrip;
import com.orange.ocara.ui.viewmodel.OcaraViewModelFactory;
import com.orange.ocara.ui.viewmodel.SetupAuditPathViewModel;
import com.orange.ocara.ui.viewmodel.SetupAuditPathViewModelFactory;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Activity dedicated to the configuration of an audit's content
 */
@EFragment(R.layout.activity_setup_audit_path)
@OptionsMenu(R.menu.audit_setup_path)
public class SetupAuditPathFragment extends BaseFragmentAuditMangment
        implements BrowseEquipmentsByCategoryFragment.OnEquipmentSelectListener,
        SelectAuditObjectCharacteristicsDialogBuilder.UpdateAuditObjectCharacteristicsListener, ViewTreeObserver.OnGlobalLayoutListener {

    private static final int MAX_ITEM_MARGIN = 15;

    private static final int BADGE_VERTICAL_MARGIN = 4;

    private static final int USER_WANTS_TO_AUDIT_OBJECT_NOW = 1;

    private static final int USER_WANTS_TO_AUDIT_OBJECT_LATER = 2;

    private static final int RESULT_AUDIT_ACTIVITY_REQ_CODE = 100;

    /**
     * Refresh strategy.
     */
//    private static final RefreshStrategy STRATEGY = RefreshStrategy.builder().depth(RefreshStrategy.DependencyDepth.AUDIT_OBJECT).commentsNeeded(true).build();

//    @Bean(DestinationController.class)
//    Navigation navController;

    //    @Extra
    boolean fromListAudit;
    //    @Extra

    @ViewById(R.id.pager)
    ViewPager viewPager;

    @ViewById(R.id.tabs)
    PagerSlidingTabStrip tabs;

    @ViewById(R.id.audited_objects)
    AuditObjectsView auditedObjects;

    @ViewById(R.id.buttonbar_left_button)
    Button leftButton;

    @ViewById(R.id.buttonbar_right_button)
    Button rightButton;

    @ViewById(R.id.action_comment)
    View target;
//    @Bean(RuleSetServiceImpl.class)
//    RuleSetService mRuleSetService;

    private int selectedIndex = -1;

    private AuditObjectsAdapter auditObjectsAdapter;

    private BadgeView badge;

//    @Override
//    RefreshStrategy getRefreshStrategy() {
//        return STRATEGY;
//    }

    //    @Override
//    protected boolean isChildActivity() {
//        return true;
//    }
    Observer<AuditEntity> auditObserver = new Observer<AuditEntity>() {
        @Override
        public void onChanged(AuditEntity audit) {
            auditRefreshed(audit);
        }
    };

    Observer<EquipmentEntity> selectedEquipmentObserver = new Observer<EquipmentEntity>() {
        @Override
        public void onChanged(EquipmentEntity eq) {
//            Toast.makeText(getActivity(), "EQ SEL = " + eq.name, Toast.LENGTH_LONG).show();
            if (wasPaused) {
                wasPaused = false;
                return;
            }
            onEquipmentSelected(eq);
        }
    };

    private SetupAuditPathViewModel viewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);

        fromListAudit = SetupAuditPathFragment_Args.fromBundle(getArguments()).getFromListAudit();
        auditId = SetupAuditPathFragment_Args.fromBundle(getArguments()).getAuditId();

        viewModel = new ViewModelProvider(this, new SetupAuditPathViewModelFactory(getActivity(), auditId)).get(SetupAuditPathViewModel.class);
        viewModel.auditLiveData.observe(getViewLifecycleOwner(), auditObserver);
        viewModel.selectedEquipmentEntityLiveData.observe(getViewLifecycleOwner(), selectedEquipmentObserver);
//        target = v.findViewById(R.id.action_comment);

//        Toast.makeText(getActivity(), "IN FRAG", Toast.LENGTH_LONG).show();

        return v;

    }

    boolean wasPaused = false;

    @Override
    public void onPause() {
//        viewModel.selectedEquipmentEntityLiveData.removeObserver(selectedEquipmentObserver);
//        viewModel.auditLiveData.removeObserver(auditObserver);

        wasPaused = true;

        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
//        Toast.makeText(getActivity(), "IN SETUP FR", Toast.LENGTH_LONG).show();
//        fromResume = true;
//        viewModel.refreshAudit();

        System.err.println("AUDPATH ON RES");
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (auditObjectsAdapter != null && audit != null) {
//            auditRefreshed();
//        }
//    }

    //    @Override
    void auditRefreshed(AuditEntity audit) {
//        super.auditRefreshed();
        initTitle(audit);
        setUpViewPager(audit);
        setUpAuditedPath();
        auditObjectsAdapter.update(audit.getObjects());

        Timber.d("ActivityMessage=refreshing audit named %s", audit.getName());
        Timber.v("SetupAuditPathActivity selectedIndex %d", selectedIndex);

        refreshSelectedIndex();
        updateButtonBar();
        badgeNotifierCommentsAudit();
    }

    void refreshSelectedIndex() {
        // Refresh selected with last one
        if (selectedIndex < 0) {
            if (auditObjectsAdapter.isEmpty()) {
                selectedIndex = 0;
            } else {
                selectedIndex = auditObjectsAdapter.getCount() - 1;
            }
        }
        auditedObjects.selectItemWithoutClick(selectedIndex);

    }

    // @AfterViews
    void initTitle(AuditEntity audit) {
        String title = getString(R.string.setup_objects_title) + " - " + getString(R.string.audit_item_name_format, audit.getName(), audit.getVersion());
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
    }

    //  @AfterViews
    void setUpViewPager(AuditEntity audit) {
//        RulesetEntity ruleSet = mRuleSetService.getRuleSet(audit.getRuleSetRef(), audit.getRuleSetVersion(), false);
//        final List<EquipmentCategoryEntity> categories = mRuleSetService.getRulsetObjects(ruleSet);
        final List<EquipmentCategoryEntity> categories = viewModel.getEquipmentCategories();
//        Timber.d("ActivityMessage=Retrieving %d categories related to the ruleset named %s (id=%d)", categories.size(), ruleSet.getType(), ruleSet.getId());

        final CategoryPagerAdapter categoriesAdapter = new CategoryPagerAdapter(getChildFragmentManager(), categories, audit);

        viewPager.setAdapter(categoriesAdapter);
        tabs.setViewPager(viewPager);
    }

    //  @AfterViews
    void setUpAuditedPath() {
        auditObjectsAdapter = new AuditObjectsAdapter(this.getContext());
        auditedObjects.setAdapter(auditObjectsAdapter);

        auditedObjects.setItemClickListener((parent, view, position, id) -> {
            selectedIndex = position;
            AuditObjectEntity auditObject = auditObjectsAdapter.getItem(position);
            launchAuditObjectsTest(!auditObject.getResponse().equals(ResponseModel.NO_ANSWER), auditObject);
        });

        registerForContextMenu(auditedObjects.getAuditPath());

        auditedObjects.getAuditPath().getViewTreeObserver().addOnGlobalLayoutListener(this);
    }

    @Override
    public void onGlobalLayout() {
        if (auditedObjects == null) return;
        float pathWitdh = auditedObjects.getAuditPath().getWidth();
        int auditObjectItemWidth = getResources().getInteger(R.integer.auditObjectItemWidth);
        final BigDecimal pathWitdhDb = BigDecimal.valueOf(pathWitdh);
        final BigDecimal auditObjectItemWidthBg = new BigDecimal(auditObjectItemWidth);
        final BigDecimal result = pathWitdhDb.divide(auditObjectItemWidthBg, BigDecimal.ROUND_HALF_UP);
        int numberAuditedObjects = result.intValue();
        float freeSpace = pathWitdh % auditObjectItemWidth;
        BigDecimal freeSpaceBg = BigDecimal.valueOf(freeSpace);
        BigDecimal numberAuditedObjectsLessOneBg = new BigDecimal(numberAuditedObjects - 1);
        BigDecimal result1 = freeSpaceBg.divide(numberAuditedObjectsLessOneBg, BigDecimal.ROUND_HALF_UP);
        int margin = result1.intValue();
        if (margin > MAX_ITEM_MARGIN) {
            margin = MAX_ITEM_MARGIN;
        }

        Timber.v("auditPath setUpAuditedPath " + pathWitdh + "auditObjectItemWitdh " + auditObjectItemWidth + " freeSpace " + freeSpace + " numberAuditedObjects " + numberAuditedObjects + " margin " + margin);

        auditedObjects.getAuditPath().setItemMargin(margin);
    }

    @AfterViews
    void setUpButtonBar() {
        leftButton.setText(R.string.setup_objects_button_back);
        rightButton.setText(R.string.setup_objects_button_audit);
    }

    //    @Background
    void createAuditObject(/*AuditEntity audit, */EquipmentEntity objectDescription, List<EquipmentEntity> children) {
//        AuditObjectEntity auditObject = modelManager.createAuditObject(audit, objectDescription);
//        for (EquipmentEntity child : children) {
//            modelManager.createChildAuditObject(auditObject, child);
//        }
//        Timber.d("ActivityMessage=Created audit object named %s, related to the ruleset named %s", auditObject.getName(), auditObject.getRuleSet().getType());
//        auditObject.refresh(getRefreshStrategy());
//        auditObjectCreated(auditObject);
        viewModel.createAuditObject(objectDescription, children).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new DisposableSingleObserver<AuditObjectEntity>() {
            //            @Override
            public void onSuccess(AuditObjectEntity auditObject) {
                auditObjectCreated(auditObject);
                dispose();
            }

            @Override
            public void onError(Throwable e) {
                dispose();
            }
        });
    }


    //    @Background
    @Override
    public void onUpdateAuditObjectChildren(final AuditObjectEntity parent, List<EquipmentEntity> childrenToCreate, List<AuditObjectEntity> childrenToRemove) {
//      modelManager.updateAuditObjectChildren(parent, childrenToCreate, childrenToRemove);
//        refreshAudit();
        viewModel.updateAuditObjectChildren(parent, childrenToCreate, childrenToRemove);
    }

    @Override
    public void onLaunchAuditObjectsTestRequested(final AuditObjectEntity parent) {
        launchAuditObjectsTest(true, parent);
    }


    @UiThread(propagation = UiThread.Propagation.REUSE)
    void auditObjectCreated(final AuditObjectEntity auditObject) {
        selectedIndex = auditObjectsAdapter.getCount();
        refreshSelectedIndex();
        // findAll application preference to know if he wants to audit object now
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        int applicationAuditObjectNowPreference = Integer.parseInt(sharedPreferences.getString(getString(R.string.setting_when_test_entries_key), "0"));

        switch (applicationAuditObjectNowPreference) {
            case 0:
                final NotificationDialogBuilder dialogBuilder = new NotificationDialogBuilder(getContext());
                dialogBuilder
                        .setInfo(getString(R.string.do_you_want_to_test_audit_object_now))
                        .setOption(getString(R.string.remember_test_now_answer))
                        .setCancelable(false)
                        .setTitle(R.string.audit_object_now_title)
                        .setPositiveButton(R.string.test_now, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                askToAuditObjectNowDialogDismiss(true, dialogBuilder.getOptionValue());
//                                refreshAudit();
                                launchAuditObjectsTest(false, auditObject);
                            }
                        })
                        .setNegativeButton(R.string.test_later, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                askToAuditObjectNowDialogDismiss(false, dialogBuilder.getOptionValue());
//                                refreshAudit();
                            }
                        })
                        .create();

                dialogBuilder.show();
                break;
            case USER_WANTS_TO_AUDIT_OBJECT_NOW:
//                refreshAudit();
                launchAuditObjectsTest(false, auditObject);
                break;
            case USER_WANTS_TO_AUDIT_OBJECT_LATER:
//                refreshAudit();
                break;
            default:
                break;
        }
    }

    //    @Background
    void deleteAuditObject(AuditObjectEntity auditObject) {
//        modelManager.deleteAuditObject(auditObject);
//        selectedIndex = selectedIndex - 1;
//        refreshAudit();
        viewModel.deleteAuditObject(auditObject).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                selectedIndex = -1;
                refreshSelectedIndex();
                dispose();
            }

            @Override
            public void onError(Throwable e) {
                dispose();
            }
        });
    }

    //    @Background
    void deleteAllAuditObjects() {
//        modelManager.deleteAllAuditObjects(audit);
//        selectedIndex = -1;
//        refreshAudit();

        viewModel.deleteAllAuditObjects().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                selectedIndex = -1;
                refreshSelectedIndex();
                dispose();
            }

            @Override
            public void onError(Throwable e) {
                dispose();
            }
        });
    }

    @Click(R.id.buttonbar_left_button)
    void onLeftButtonClicked() {
//        NavHostFragment.findNavController(this).navigateUp();

        onBack();
    }

    void onBack() {
        if (auditObjectsAdapter.getCount() == 1) {
            if (auditObjectsAdapter.getItem(0).getResponse() == ResponseModel.NO_ANSWER) {
                //case 1
                deleteAuditObject(auditObjectsAdapter.getItem(0));
            } else {
                //case 2
                viewModel.resetAuditObject(auditObjectsAdapter.getItem(0));
                refreshSelectedIndex();
            }
        }
        else if (auditObjectsAdapter.getCount() == 0){ //case 3
            viewModel.deleteAudit();
            NavHostFragment.findNavController(this).navigateUp();
        }


    }

    @Click(R.id.buttonbar_right_button)
    void onRightButtonClicked() {
        List<AuditObjectEntity> selectedObjectsList = new ArrayList<>();
        for (int i = 0; i < auditObjectsAdapter.getCount(); i++) {
            AuditObjectEntity auditObject = auditObjectsAdapter.getItem(i);

            if (!auditObject.isAudited()) {
                selectedObjectsList.add(auditObject);
            }
        }
        launchAuditObjectsTest(true, selectedObjectsList.toArray(new AuditObjectEntity[]{}));
    }

    @OptionsItem(R.id.delete_path)
    void onDeletePath() {
        askToDeleteAllAuditObjects();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        final AuditObjectEntity auditObject = auditObjectsAdapter.getItem(info.position);

        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.audit_objects_context_menu, menu);
        final EquipmentEntity objectDescription = viewModel.getAuditObjectDescription(auditObject);

        if (objectDescription.getSubObject().isEmpty()) {
            menu.findItem(R.id.action_object_characteristics).setVisible(false);
        } else {
            menu.findItem(R.id.action_object_characteristics).setVisible(true);
        }


    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final AuditObjectEntity auditObject = auditObjectsAdapter.getItem(info.position);

        int i = item.getItemId();
        if (i == R.id.action_object_update) {
            updateAuditObject(auditObject);
            return true;
        } else if (i == R.id.action_object_comment) {
            commentAuditObject(auditObject);
            return true;
        } else if (i == R.id.action_object_characteristics) {
            SelectAuditObjectCharacteristicsDialogBuilder.showCharacteriscticsAuditObject(getActivity(), auditObject, this);
            return true;
        } else if (i == R.id.action_object_detail) {
            showDetailsObject(auditObject);
            return true;
        } else if (i == R.id.action_object_remove) {
            askToDeleteAuditObject(auditObject);
            return true;
        } else {
            return super.onContextItemSelected(item);
        }

    }

    private void showDetailsObject(AuditObjectEntity auditObject) {

//        DetailsActivity_.intent(this)
//                .auditId(auditId)
//                .auditObjectId(auditObject.getId())
//                .startForResult(0);

        Bundle bundle = new Bundle();
        bundle.putLong("auditId", auditId);
        bundle.putLong("auditObjectId", auditObject.getId());
        NavHostFragment.findNavController(this).navigate(R.id.action_audit_obj_details, bundle);
    }


    private void updateAuditObject(final AuditObjectEntity auditObject) {

        // Use an EditText view to findAll user input.
        final EditText input = new EditText(getContext());
        input.setText(auditObject.getName());

        AlertDialog updateDialog = new OcaraDialogBuilder(getContext())
                .setTitle(R.string.audit_object_update_object_title) // title
                .setView(input)
                .setPositiveButton(R.string.audit_object_update_object_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String name = input.getText().toString();

                        if (StringUtils.isNotBlank(name)) {
                            updateAuditObjectName(auditObject, name);
                        }
                    }
                })
                .setNegativeButton(R.string.action_cancel, null)
                .create();

        updateDialog.show();
    }

    void updateAuditObjectName(AuditObjectEntity auditObject, String name) {
        viewModel.updateAuditObjName(auditObject, name);
    }

    private void commentAuditObject(AuditObjectEntity auditObject) {
        String attachmentDirectory = viewModel.getModelManager().getAttachmentDirectory(auditId).getAbsolutePath();
//        ListAuditObjectCommentActivity_.intent(this)
//                .auditObjectId(auditObject.getId())
//                .attachmentDirectory(attachmentDirectory)
//                .startForResult(0);

        Bundle bundle = new Bundle();
        bundle.putLong("auditObjectId", auditObject.getId());
        bundle.putString("attachmentDirectory", attachmentDirectory);
        NavHostFragment.findNavController(this).navigate(R.id.action_audit_obj_comments, bundle);
    }

    private void askToDeleteAuditObject(final AuditObjectEntity auditObject) {
        AlertDialog confirmDialog = new OcaraDialogBuilder(getContext())
                .setTitle(R.string.audit_object_remove_object_title) // title
                .setMessage(R.string.audit_object_remove_object_message) // message
                .setPositiveButton(R.string.audit_object_remove_object_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteAuditObject(auditObject);
                    }
                })
                .setNegativeButton(R.string.action_cancel, null)
                .setCancelable(true)
                .create();

        confirmDialog.show();
    }

    private void askToDeleteAllAuditObjects() {

        AlertDialog confirmDialog = new OcaraDialogBuilder(getContext())
                .setTitle(R.string.audit_object_remove_all_objects_title) // title
                .setMessage(R.string.audit_object_remove_all_objects_message) // message
                .setPositiveButton(R.string.audit_object_remove_all_objects_button, (dialog, which) -> deleteAllAuditObjects())
                .setNegativeButton(R.string.action_cancel, null)
                .setCancelable(true)
                .create();

        confirmDialog.show();
    }

    @OptionsItem(R.id.action_update)
    void onUpdateMenuItem() {
//        navController.navigateToAuditEditView(getContext(), auditId, 0);
        Bundle bundle = new Bundle();
        bundle.putLong("auditId", auditId);
        NavHostFragment.findNavController(this).navigate(R.id.action_edit_audit, bundle);
    }

    @OptionsItem(R.id.action_report)
    void onReportMenuItem() {
        // TODO changed
//        ResultAuditActivity_.intent(this).auditId(auditId).startForResult(RESULT_AUDIT_ACTIVITY_REQ_CODE);

        Bundle bundle = new Bundle();
        bundle.putLong("auditId", auditId);
        NavHostFragment.findNavController(getParentFragment()).navigate(R.id.action_audit_report, bundle);
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        if (requestCode == RESULT_AUDIT_ACTIVITY_REQ_CODE && resultCode == RESULT_OK)
//            finish();
//        super.onActivityResult(requestCode, resultCode, data);
//    }

    @OptionsItem(R.id.action_comment)
    void onCommentMenuItem() {
        String attachmentDirectory = viewModel.getModelManager().getAttachmentDirectory(auditId).getAbsolutePath();
//        ListAuditCommentActivity_
//                .intent(this)
//                .auditId(auditId)
//                .attachmentDirectory(attachmentDirectory)
//                .startForResult(0);

        Bundle bundle = new Bundle();
        bundle.putLong("auditId", auditId);
        bundle.putString("attachmentDirectory", attachmentDirectory);
        NavHostFragment.findNavController(this).navigate(R.id.action_audit_comments, bundle);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
//        badgeNotifierCommentsAudit();
        final MenuItem menuItem = menu.findItem(R.id.action_comment);

        View actionView = menuItem.getActionView();
        badge = (BadgeView) actionView.findViewById(R.id.comments_badge);

//        setupBadge();
        if (badge != null) {
            List<CommentEntity> nbrComments = viewModel.getAuditComments();
            if (nbrComments.isEmpty()) {
                badge.hide();
            } else {
                badge.setText(String.valueOf(nbrComments.size()));
                badge.setBadgeMargin(0, BADGE_VERTICAL_MARGIN);
                badge.show();
            }
        }
        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });
        if (badge == null)
            badge = new BadgeView(getContext(), menu.findItem(R.id.action_comment).getActionView());
        List<CommentEntity> nbrComments = viewModel.getAuditComments();
        if (!nbrComments.isEmpty()) {
            badge.setText(String.valueOf(nbrComments.size()));
            badge.setBadgeMargin(0, BADGE_VERTICAL_MARGIN);
            badge.show();
        }

    }

    /**
     * creates and displays a {@link BadgeView}
     */
    @UiThread(propagation = UiThread.Propagation.REUSE)
    public void badgeNotifierCommentsAudit() {
        if (target == null) {
            return;
        }
        if (badge != null) {
            badge.hide();
        }
        badge = new BadgeView(getContext(), target);
        List<CommentEntity> nbrComments = viewModel.getAuditComments();
        if (!nbrComments.isEmpty()) {
            badge.setText(String.valueOf(nbrComments.size()));
            badge.setBadgeMargin(0, BADGE_VERTICAL_MARGIN);
            badge.show();
        }
    }

    @Override
//    @Background
    public void onEquipmentSelected(final EquipmentEntity equipment) {

        // ask user to select children subObject (also named characteristic equipments)
        List<EquipmentEntity> children = new ArrayList<>();
        for (String ref : equipment.getSubObject()) {
            final EquipmentEntity subEquipment = viewModel.getAuditObjectDescriptionFromRef(ref);
            children.add(subEquipment);
        }

        if (children.isEmpty()) {
            createAuditObject(equipment, children);
        } else {
            // dialog to select characteristics
            triggerAlert(equipment);
        }
    }

    @UiThread
    void triggerAlert(final EquipmentEntity equipment) {
        final SelectAuditObjectCharacteristicsDialogBuilder selectCharacteristicsBuilder = new SelectAuditObjectCharacteristicsDialogBuilder(getContext(), equipment);
        selectCharacteristicsBuilder
                .setTitle(R.string.audit_object_characteristics_title)
                .setCancelable(false)
                .setPositiveButton(R.string.audit_object_characteristics_validate, (dialog, which) -> {
                    List<EquipmentEntity> selections = selectCharacteristicsBuilder.getSelectedCharacteristics();
                    createAuditObject(equipment, selections);
                })
                .create().show();
    }


    @UiThread(propagation = UiThread.Propagation.REUSE)
    void updateButtonBar() {
        updateRightButton();
    }

    private void updateRightButton() {

        // If no subObject button should not appear
        boolean visible = !auditObjectsAdapter.isEmpty();
        rightButton.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);

        // Button should be enabled if one or more object are not tested yet
        boolean enabled = false;
        for (int i = 0; i < auditObjectsAdapter.getCount(); i++) {
            AuditObjectEntity auditObject = auditObjectsAdapter.getItem(i);

            if (!auditObject.isAudited()) {
                enabled = true;
                break;
            }
        }
        rightButton.setEnabled(enabled);
    }

    private void askToAuditObjectNowDialogDismiss(boolean userWantsToAuditObjectNow, boolean rememberUserChoice) {
        if (rememberUserChoice) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            SharedPreferences.Editor editor = sharedPreferences.edit();
            if (userWantsToAuditObjectNow) {
                editor.putString(getString(R.string.setting_when_test_entries_key), "1");

            } else {
                editor.putString(getString(R.string.setting_when_test_entries_key), "2");
            }
            editor.apply();


        }
    }

//    @Override
//    public void onDestroy() {
//        auditedObjects.getAuditPath().getViewTreeObserver().removeOnGlobalLayoutListener(this);
//        super.onDestroy();
//    }
//    @Override
//    public void onBackPressed() {
//        if (fromListAudit) {
//            //ListAuditActivity_.intent(this).start();
//            super.onBackPressed();
//        } else {
//            CreateAuditActivity_.intent(this).start();
//        }
//        super.onBackPressed();
//    }
}
