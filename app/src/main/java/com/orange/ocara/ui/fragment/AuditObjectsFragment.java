/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.fragment;

import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.fragment.NavHostFragment;

import com.orange.ocara.R;
import com.orange.ocara.data.cache.model.AuditEntity;
import com.orange.ocara.data.cache.model.AuditObjectEntity;
import com.orange.ocara.data.cache.model.CommentEntity;
import com.orange.ocara.data.cache.model.QuestionAnswerEntity;
import com.orange.ocara.data.cache.model.ResponseModel;
import com.orange.ocara.data.cache.model.RuleAnswerEntity;
import com.orange.ocara.ui.activity.ListAuditObjectCommentActivity_;
import com.orange.ocara.ui.adapter.QuestionAndRulesAnswersAdapter;
import com.orange.ocara.ui.dialog.OcaraDialogBuilder;
import com.orange.ocara.ui.tools.RefreshStrategy;
import com.orange.ocara.ui.view.BadgeView;
import com.orange.ocara.ui.viewmodel.AuditObjectsViewModel;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import timber.log.Timber;

/**
 * Activity dedicated to managing an {@link AuditObjectEntity}
 */
@EFragment(R.layout.fragment_audit_object)
//@OptionsMenu(R.menu.audit_objects)
public abstract class AuditObjectsFragment extends BaseFragmentAuditMangment
        implements QuestionAndRulesAnswersAdapter.QuestionAndRulesListener {

    @Bean
    QuestionAndRulesAnswersAdapter questionAndRulesAnswersAdapter;

    @ViewById(R.id.buttonbar_left_button)
    Button leftButton;

    @ViewById(R.id.buttonbar_right_button)
    Button rightButton;

    @ViewById(R.id.rules_listview)
    ExpandableListView rulesListView;

    //    @Extra
    boolean editMode = false;

    //    @Extra
    long[] selectedObjects;

    private BadgeView badge;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @AfterViews
    void setUpRulesListView() {
        questionAndRulesAnswersAdapter.setDatas(this);
        rulesListView.setAdapter(questionAndRulesAnswersAdapter);
    }


    protected static String getAuditObjectName(AuditObjectEntity auditObject) {
        if (auditObject.getParent() == null) {
            return auditObject.getName();
        }
        return auditObject.getParent().getName() + " / " + auditObject.getName();
    }




    @UiThread(propagation = UiThread.Propagation.REUSE)
    public void badgeNotifierCommentsAuditObject() {

        AuditObjectEntity currentAuditObject = getViewModel().getCurrentAuditObject();

        View target = getActivity().findViewById(R.id.action_comment);
        if (target == null || currentAuditObject == null) {
            return;
        }
        if (badge != null) {
            badge.hide();
        }
        badge = new BadgeView(getContext(), target);
        List<CommentEntity> nbrComments = currentAuditObject.getComments();
        if (!currentAuditObject.getComments().isEmpty()) {
            badge.setText(String.valueOf(nbrComments.size()));
            badge.setBadgeMargin(0, 4);
            badge.show();
        }
    }


    @OptionsItem(R.id.action_comment)
    void auditObjectCommentItemClicked() {

        String attachmentDirectory = getViewModel().getAttachmentDirectoryForCurrentAuditObject();
//        if (attachmentDirectory != null) {
//            ListAuditObjectCommentActivity_
//                    .intent(this)
//                    .auditObjectId(getViewModel().getCurrentAuditObject().getId())
//                    .attachmentDirectory(attachmentDirectory)
//                    .start();


            Bundle bundle = new Bundle();
            bundle.putLong("auditObjectId", getViewModel().getCurrentAuditObject().getId());
            bundle.putString("attachmentDirectory", attachmentDirectory);
            NavHostFragment.findNavController(this).navigate(R.id.action_audit_obj_comments, bundle);

    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Handler handler = new Handler();


        handler.postDelayed(this::badgeNotifierCommentsAuditObject, 200);
    }


    protected void refreshQuestionList(List<QuestionAnswerEntity> questionAnswers) {
        questionAndRulesAnswersAdapter.update(questionAnswers);

        // Always 1 but to ensure all cases
        for (int i = 0; i < questionAndRulesAnswersAdapter.getGroupCount(); i++) {
            rulesListView.expandGroup(i);
        }

        updateActionBar();
        updateButtonBar();
    }

    private void updateActionBar() {
        updateTitle();

        final AuditObjectEntity currentAuditObject = getViewModel().getCurrentAuditObject();
        //modelManager.refresh(currentAuditObject, RULE_REFRESH_STRATEGY);
        if (currentAuditObject != null && currentAuditObject.getObjectDescription() != null) {
            updateLogo(currentAuditObject.getObjectDescription().getIcon());
        }
        badgeNotifierCommentsAuditObject();
    }


    private void updateLeftButton() {

        if (getViewModel().isFirstItemSelected()) {
            leftButton.setText(R.string.action_back);
        } else {
            leftButton.setText(R.string.audit_object_button_previous);
        }
    }

    private void updateRightButton() {

//        Timber.d(" mode BEG len %d", selectedObjects.length - 1);

        if (getViewModel().isLastItemSelected()) {
            rightButton.setText(R.string.audit_object_button_terminate);
        } else {
            rightButton.setText(R.string.audit_object_button_next);
        }
    }

    @UiThread
    protected void updateButtonBar() {
        updateLeftButton();
        updateRightButton();
    }

    protected void updateTitle() {
        AuditObjectEntity currentAuditObject;
        currentAuditObject = getViewModel().getCurrentAuditObject();
        if (currentAuditObject != null) {
            setTitle(getTitle());
        }
    }

    public void onBack() {

        Timber.d("On back pressed...");
        if (!getViewModel().hasUpdatedRules()) {
            finish();
            return;
        }

        AlertDialog confirmDialog = new OcaraDialogBuilder(getContext())
                .setTitle(R.string.audit_object_cancel_title) // title
                .setMessage(R.string.audit_object_cancel_message) // message
                .setPositiveButton(R.string.action_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getViewModel().revertUpdates();
                        finish();
                    }
                })
                .setNegativeButton(R.string.action_no, null)
                .create();

        confirmDialog.show();
    }


    //listener override
    @Override
    @Background
    public void updateRuleAnswer(RuleAnswerEntity ruleAnswer, ResponseModel response) {
        getViewModel().updateRuleAnswer(ruleAnswer, response);
//        ruleAnswer.updateResponse(response);
//        updatedRules.add(ruleAnswer);
        updateButtonBar();
        updateAdapter();
    }

    @UiThread
    void updateAdapter() {
        questionAndRulesAnswersAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setLogo(R.drawable.ic_app);
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBack();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
        @Override
        public void handleOnBackPressed() {
            // Handle the back button event
            onBack();
        }
    };


    protected abstract AuditObjectsViewModel getViewModel();

    protected abstract String getTitle();


}
