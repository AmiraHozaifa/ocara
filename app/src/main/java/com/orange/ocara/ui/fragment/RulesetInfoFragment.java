/*
 * Software Name: OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 * SPDX-License-Identifier: MPL v2.0
 *
 * This software is distributed under the Mozilla Public License v. 2.0,
 * the text of which is available at http://mozilla.org/MPL/2.0/ or
 * see the "license.txt" file for more details.
 */

package com.orange.ocara.ui.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.orange.ocara.R;
import com.orange.ocara.business.binding.ErrorBundle;
import com.orange.ocara.business.interactor.LoadRulesetTask;
import com.orange.ocara.business.interactor.UseCase;
import com.orange.ocara.business.model.RulesetModel;
import com.orange.ocara.databinding.FragmentRulesetInfoBinding;
import com.orange.ocara.ui.RulesetInfoUiConfig;
import com.orange.ocara.ui.activity.BrowseRulesetsActivity_;
import com.orange.ocara.ui.contract.RulesetInfoContract;
import com.orange.ocara.ui.tools.RuleSetUtils;
import com.orange.ocara.ui.viewmodel.RulesetInfoViewModel;
import com.orange.ocara.ui.viewmodel.RulesetInfoViewModelFactory;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.Locale;

import io.reactivex.CompletableObserver;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Activity dedicated to display information about a given ruleset
 */
@EFragment(R.layout.fragment_ruleset_info)
public class RulesetInfoFragment extends BaseFragment
//        implements RulesetInfoContract.RulesetInfoView
{

    //    @Extra
    RulesetModel ruleset;
    //
//    @Extra
    boolean rulesetUpgradable;

//    @ViewById(R.id.author_text)
//    TextView authorTextView;
//
//    @ViewById(R.id.name_text)
//    TextView nameTextView;
//
//    @ViewById(R.id.rule_category_text)
//    TextView ruleCategoryTextView;
//
//    @ViewById(R.id.comment_text)
//    TextView commentTextView;
//
//    @ViewById(R.id.version_text)
//    TextView versionTextView;
//
//    @ViewById(R.id.publication_date_text)
//    TextView publicationDateTextView;
//
//    @ViewById(R.id.button_download_rules)
//    Button mDownloadRulesButton;
//
//    @ViewById(R.id.button_show_rules)
//    Button mShowRulesButton;

    private ProgressDialog mDownloadRuleSetProgressDialog;

//    private RulesetInfoContract.RulesetInfoUserActionsListener actionsListener;

//    @Bean(RulesetInfoUiConfig.class)
//    RulesetInfoUiConfig uiConfig;

    FragmentRulesetInfoBinding binding;
    RulesetInfoViewModel rulesetInfoViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ruleset = RulesetInfoFragment_Args.fromBundle(getArguments()).getRuleSet();
        rulesetUpgradable = RulesetInfoFragment_Args.fromBundle(getArguments()).getRuleSetUpgradable();

        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_ruleset_info, container, false);
        View view = binding.getRoot();
//        rulesetInfoViewModel = new ViewModelProvider(this, new CreateSiteViewModelFactory(modelManager)).get(CreateSiteViewModel.class);
        rulesetInfoViewModel = new ViewModelProvider(this, new RulesetInfoViewModelFactory(getActivity(), ruleset, rulesetUpgradable)).get(RulesetInfoViewModel.class);
        binding.setLifecycleOwner(this);
        binding.setRulesetViewModel(rulesetInfoViewModel);
//        Toast.makeText(getActivity(), "CH", Toast.LENGTH_LONG).show();
//        checkRuleset();
        return view;
    }


    //
////    @AfterExtras
////    void initData() {
////        if (ruleset == null) {
////            Timber.e("Missing ruleset parameters in the activity extra");
////        }
////
////        actionsListener = uiConfig.actionsListener(this, rulesetUpgradable);
////    }
//
//    @Background


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        loadRuleSet();
    }

    @Background
    @AfterViews
    public void loadRuleSet() {

        System.err.println("loadRuleset subscribe");

        rulesetInfoViewModel.loadRulesetTask.executeUseCase(new LoadRulesetTask.LoadRulesetRequest(rulesetInfoViewModel.getRuleset()), new UseCase.UseCaseCallback<LoadRulesetTask.LoadRulesetResponse>() {
            @Override
            public void onComplete(LoadRulesetTask.LoadRulesetResponse response) {
//                        System.err.println("loadRuleset subscribe");
                showRuleset(response.getContent());
            }

            @Override
            public void onError(ErrorBundle errors) {
                showInvalidRuleset();
            }
        });

//        actionsListener.loadRuleset(this.ruleset);
//        rulesetInfoViewModel.loadRulesetObservable.subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<RulesetModel>() {
//            @Override
//            public void onSubscribe(Disposable d) {
//                System.err.println("loadRuleset subscribedddddddd");
//            }
//
//            @Override
//            public void onNext(RulesetModel rules) {
//                System.err.println("loadRuleset subscribedddddddd next");
//
//            }
//
//            @Override
//            public void onError(Throwable e) {
//                e.printStackTrace();
//                System.err.println("loadRuleset subscribedddddddd err");
//
//            }
//
//            @Override
//            public void onComplete() {
//                System.err.println("loadRuleset subscribedddddddd cmp");
//
//            }
//        });
    }

    //
    @UiThread
//    @Override
    public void showRuleset(RulesetModel ruleset) {
        if (ruleset.getType() != null) {
            binding.nameText.setText(ruleset.getType());
        }

        if (ruleset.getVersion() != null) {
            binding.versionText.setText(ruleset.getVersion());
        }

        if (ruleset.getAuthor() != null) {
            binding.authorText.setText(ruleset.getAuthor());
        }

        if (ruleset.getRuleCategoryName() != null) {
            binding.ruleCategoryText.setText(ruleset.getRuleCategoryName());
        }

        if (ruleset.getComment() != null) {
            binding.commentText.setText(ruleset.getComment());
        }

        if (ruleset.getDate() != null) {
            Locale locale = Locale.getDefault();
            String date = RuleSetUtils.formatRulesetDate(ruleset.getDate(), locale);
            binding.publicationDateText.setText(date);
        }

        if (ruleset.isLocallyAvailable()) {
            binding.buttonDownloadRules.setText(R.string.upgrade_rules);
        } else if (ruleset.isRemotelyAvailable()) {
            binding.buttonDownloadRules.setText(getString(R.string.download_rules));
        }

        checkRuleset();
    }

    //
//    /**
//     * validates the view elements according to the ruleset
//     */
    @Background
    void checkRuleset() {
        Timber.d("ActivityMessage=starting ruleset checking");
//        actionsListener.checkRulesetIsValid(ruleset);

        disableRulesetDetails();
        disableRulesetUpgrade();
        rulesetInfoViewModel.checkRulesetIsValid(ruleset);
//        rulesetInfoViewModel.checkRulesetExistance(this.ruleset).subscribe(new Observer<Boolean>() {
//            @Override
//            public void onSubscribe(Disposable d) {
//
//            }
//
//            @Override
//            public void onNext(Boolean valid) {
//                if (valid) {
//                    enableRulesetDetails();
//                } else {
//                    disableRulesetDetails();
//                }
//            }
//
//            @Override
//            public void onError(Throwable e) {
//                disableRulesetDetails();
//                showInvalidRuleset();
//            }
//
//            @Override
//            public void onComplete() {
//
//            }
//        });
//
//
//        rulesetInfoViewModel.checkRulesetIsUpgradable(this.ruleset).subscribe(new Observer<Boolean>() {
//            @Override
//            public void onSubscribe(Disposable d) {
//
//            }
//
//            @Override
//            public void onNext(Boolean valid) {
//                if (valid) {
//                    enableRulesetUpgrade();
//                } else {
//                    disableRulesetUpgrade();
//                }
//            }
//
//            @Override
//            public void onError(Throwable e) {
//                disableRulesetUpgrade();
//                showInvalidRuleset();
//            }
//
//            @Override
//            public void onComplete() {
//
//            }
//        });

    }

    @Click(R.id.button_show_rules)
    public void showRules() {

        String rulesetReference = ruleset.getReference();
        Integer rulesetVersion = Integer.parseInt(ruleset.getVersion());
        Timber.d("ActivityMessage=showing ruleset's rules;RulesetReference=%s;RulesetVersion=%d", rulesetReference, rulesetVersion);
        BrowseRulesetsActivity_
                .intent(this)
                .rulesetReference(rulesetReference)
                .ruleSetVersion(rulesetVersion)
                .start();
    }

    //
//    /**
//     * triggers the upgrade of the current ruleset to its newest version
//     */
    @Click(R.id.button_download_rules)
    public void downloadRuleset() {
        triggerUpgrade();
    }

    @Background
    void triggerUpgrade() {
        Timber.d("ActivityMessage=starting ruleset upgrade");
//        actionsListener.upgradeRuleset(ruleset);
        showProgressDialog();
        rulesetInfoViewModel.upgradeRuleset(this.ruleset).subscribe(new Observer<RulesetModel>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(RulesetModel rulesetModel) {
                showDownloadSucceeded();
                hideProgressDialog();
                showRuleset(rulesetModel);
            }

            @Override
            public void onError(Throwable e) {
                showDownloadFailed();
                hideProgressDialog();
            }

            @Override
            public void onComplete() {

            }
        });
    }


    @UiThread
    public void showProgressDialog() {
        Timber.d("ActivityMessage=showing progress dialog");
        mDownloadRuleSetProgressDialog = new ProgressDialog(getActivity());
        mDownloadRuleSetProgressDialog.setCanceledOnTouchOutside(false);
        mDownloadRuleSetProgressDialog.setCancelable(false);
        mDownloadRuleSetProgressDialog.setMessage(getString(R.string.downloading));
        mDownloadRuleSetProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDownloadRuleSetProgressDialog.setIndeterminate(true);
        mDownloadRuleSetProgressDialog.show();
    }


    @UiThread
    public void hideProgressDialog() {
        Timber.d("ActivityMessage=hiding progress dialog");
        if (mDownloadRuleSetProgressDialog != null) {
            mDownloadRuleSetProgressDialog.dismiss();
        }
    }

    @UiThread
    public void showDownloadSucceeded() {
        Timber.d("ActivityMessage=showing success toast");
        Toast.makeText(getActivity(), R.string.downloading_succeeded, Toast.LENGTH_LONG).show();
    }

    @UiThread
    public void showDownloadFailed() {
        Timber.d("ActivityMessage=showing failure toast");
        Toast.makeText(getActivity(), R.string.downloading_failed, Toast.LENGTH_LONG).show();
    }

    @UiThread
    public void showInvalidRuleset() {
        Timber.d("ActivityMessage=showing invalid toast");
        Toast.makeText(getActivity(), R.string.invalid_ruleset, Toast.LENGTH_LONG).show();
    }

    //
    @UiThread
    public void enableRulesetDetails() {
        Timber.d("ActivityMessage=enabling ruleset details");
        binding.buttonShowRules.setEnabled(true);
        binding.buttonShowRules.setVisibility(View.VISIBLE);
    }

    //
    @UiThread
    public void disableRulesetDetails() {
        Timber.d("ActivityMessage=disabling ruleset details");
        binding.buttonShowRules.setEnabled(false);
        binding.buttonShowRules.setVisibility(View.GONE);
    }

    @UiThread
    public void enableRulesetUpgrade() {
        Timber.d("ActivityMessage=enabling ruleset upgrade");
        binding.buttonDownloadRules.setEnabled(true);
        binding.buttonDownloadRules.setVisibility(View.VISIBLE);
    }

    @UiThread
    public void disableRulesetUpgrade() {
        Timber.d("ActivityMessage=disabling ruleset upgrade");
        binding.buttonDownloadRules.setEnabled(false);
        binding.buttonDownloadRules.setVisibility(View.GONE);
    }

    @LongClick(R.id.button_download_rules)
    public void displayDownloadRulesetLabel() {
        Toast.makeText(getActivity(), R.string.edit_audit_download_rules_button, Toast.LENGTH_SHORT).show();
    }

    @LongClick(R.id.button_show_rules)
    public void displayShowRulesLabel() {
        Toast.makeText(getActivity(), R.string.edit_audit_show_rules_button, Toast.LENGTH_SHORT).show();
    }
}
