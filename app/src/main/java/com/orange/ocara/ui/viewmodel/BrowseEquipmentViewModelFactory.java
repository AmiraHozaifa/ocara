package com.orange.ocara.ui.viewmodel;

import android.content.Context;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

@Deprecated
public class BrowseEquipmentViewModelFactory implements ViewModelProvider.Factory {
//    private BizConfig bizConfig;
//    private ModelManager modelManager;

    Context context;

    public BrowseEquipmentViewModelFactory(Context context) {
//        this.bizConfig = bizConfig;
//        this.modelManager = modelManager;
        this.context = context;
    }


    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new BrowseEquipmentsViewModel(context);
    }
}