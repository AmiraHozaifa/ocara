/*
 * Copyright (c) 2019 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 * distribute, sublicense, create a derivative work, and/or sell copies of the
 * Software in any work that is designed, intended, or marketed for pedagogical or
 * instructional purposes related to programming, coding, application development,
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works,
 * or sale is expressly withheld.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.orange.ocara.ui.viewmodel;

import android.content.Context;
import android.util.Pair;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.orange.ocara.data.cache.db.ModelManager;
import com.orange.ocara.data.cache.db.ModelManagerImpl_;
import com.orange.ocara.data.cache.model.AuditObjectEntity;
import com.orange.ocara.data.cache.model.QuestionAnswerEntity;
import com.orange.ocara.data.cache.model.ResponseModel;
import com.orange.ocara.data.cache.model.RuleAnswerEntity;
import com.orange.ocara.ui.tools.RefreshStrategy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public abstract class AuditObjectsViewModel extends OcaraViewModel {

    protected static final RefreshStrategy STRATEGY = RefreshStrategy.auditObjectRefreshStrategy();
    protected static final RefreshStrategy RULE_REFRESH_STRATEGY = RefreshStrategy.ruleAnswerRefreshStrategy();
    protected static final RefreshStrategy QUESTION_REFRESH_STRATEGY = RefreshStrategy.questionAnswerRefreshStrategy();

    //UI list liveData
    public MutableLiveData<List<QuestionAnswerEntity>> questionListLiveData;

    //param
    protected long[] selectedObjects;
    protected ModelManager modelManager;
    protected List<QuestionAnswerEntity> allQuestions;
    HashMap<Long, Set<RuleAnswerEntity>> updatedRulesAnswersPerObj;
    List<Pair<RuleAnswerEntity, ResponseModel>> updatedRulesWithOldResponse;

    public AuditObjectsViewModel(Context context, long[] selectedObjects) {
        super(context);
        this.selectedObjects = selectedObjects;

        questionListLiveData = new MutableLiveData<>();
        allQuestions = new ArrayList<>();
        updatedRulesAnswersPerObj = new HashMap<>();
        updatedRulesWithOldResponse = new ArrayList<>();

        modelManager = ModelManagerImpl_.getInstance_(context);

    }

    public String getAttachmentDirectoryForCurrentAuditObject() {
        AuditObjectEntity currentAuditObject = getCurrentAuditObject();
        if (currentAuditObject != null) {
            return modelManager.getAttachmentDirectory(currentAuditObject.getAudit().getId()).getAbsolutePath();
        }
        return null;
    }

    public void revertUpdates() {
        updatedRulesAnswersPerObj.clear();
        for (Pair<RuleAnswerEntity, ResponseModel> pair : updatedRulesWithOldResponse) {
            RuleAnswerEntity ruleAnswerEntity = pair.first;
            ResponseModel oldResponse = pair.second;
            ruleAnswerEntity.updateResponse(oldResponse);
//            addToRulesAnswers(ruleAnswerEntity);
        }
    }

    //background
    public void saveUpdates() {
        for (int i = 0; i < selectedObjects.length; i++) {
            AuditObjectEntity auditObject = modelManager.getAuditObject(selectedObjects[i]);
            modelManager.refresh(auditObject, QUESTION_REFRESH_STRATEGY);
            modelManager.updateAuditObject(auditObject, updatedRulesAnswersPerObj.get(auditObject.getId()));
        }
    }

    public void updateRuleAnswer(RuleAnswerEntity ruleAnswer, ResponseModel response) {
        updatedRulesWithOldResponse.add(new Pair<>(ruleAnswer, ruleAnswer.getResponse()));
        ruleAnswer.updateResponse(response);
        addToRulesAnswers(ruleAnswer);
    }

    private void addToRulesAnswers(RuleAnswerEntity ruleAnswer) {
        long ruleAnswerEntityObjectId = ruleAnswer.getQuestionAnswer().getAuditObject().getId();
        if (updatedRulesAnswersPerObj.containsKey(ruleAnswerEntityObjectId)) {
            updatedRulesAnswersPerObj.get(ruleAnswerEntityObjectId).add(ruleAnswer);
        } else {
            Set<RuleAnswerEntity> entities = new HashSet<>();
            entities.add(ruleAnswer);
            updatedRulesAnswersPerObj.put(ruleAnswerEntityObjectId, entities);
        }
    }

    public boolean hasUpdatedRules() {
        return (!updatedRulesAnswersPerObj.isEmpty());
    }

    /**
     * To findAll the current AuditObject based on the current QuestionAnswer
     *
     * @return current AuditObject
     */
    public abstract AuditObjectEntity getCurrentAuditObject();

    public abstract boolean isLastItemSelected();

    public abstract boolean isFirstItemSelected();
}
