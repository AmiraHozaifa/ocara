package com.orange.ocara.ui.routing;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.orange.ocara.ui.activity.CreateAuditActivity_;
import com.orange.ocara.ui.activity.MainActivity;
import com.orange.ocara.ui.activity.UpdateAuditActivity_;

import org.androidannotations.annotations.EBean;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * UI Routing Configuration
 */
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@EBean(scope = EBean.Scope.Singleton)
public class DestinationController implements Navigation {

    @Override
    public void navigateToHome(Context context) {
//        CreateAuditActivity_
//                .intent(context)
//                .auditId(null)
//                .start();
        Intent i = new Intent(context, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(i);
    }

    @Override
    public void navigateToHome(Context context, int resultCode) {

//        CreateAuditActivity_
//                .intent(context)
//                .auditId(null)
//                .startForResult(resultCode);
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void navigateToAuditEditView(Context context, long auditId, int resultCode) {
        UpdateAuditActivity_
                .intent(context)
                .auditId(auditId)
                .startForResult(resultCode);
    }

    @Override
    public void navigateToSiteCreateView(Context context, int resultCode) {
//        CreateSiteActivity_
//                .intent(context)
//                .startForResult(resultCode);

//        Intent intent=new Intent(context,CreateSiteActivity.class);
//        ((Activity)context).startActivityForResult(intent, resultCode);

    }

    @Override
    public void terminate(Context context) {

        // do nothing yet
    }
}
