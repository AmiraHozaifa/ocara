# Flavor INTEGRATION
This package contains the code and resources that are dedicated to the integration flavor.

That includes :
- folder **res/** for resources such as drawables
- file [google-services.json](https://developers.google.com/android/guides/google-services-plugin)

# Configuration
In **app/build.gradle**, please refer to the following attributes :
  - *android.productFlavors.integration*
  - *android.sourceSets.opensourceIntegration*
  - and eventually to *android.tasks*