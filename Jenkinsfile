#!groovy

/*
 * Software Name : OCARA
 *
 * SPDX-FileCopyrightText: Copyright (c) 2015-2020 Orange
 *
 * This software is confidential and proprietary information of Orange.
 * You shall not disclose such Confidential Information and shall not copy, use or distribute it
 * in whole or in part without the prior written consent of Orange
 *
 */

def variant = 'OrangewithoutsmtkDevRelease'
def testVariant = "testOrangewithoutsmtkDevReleaseUnitTest"
def lintVariant = "lintOrangewithoutsmtkDevRelease"
def git_url_ocara_opensource = 'git@gitlab.forge.orange-labs.fr:OCARASoftwareSolution/Ocara.git'
// def git_url_ocara_orange = 'git@gitlab.forge.orange-labs.fr:OCARASoftwareSolution/ocara-orange.git'

/*
 * git_credentials : identifiant de l'agent de déploiement entre GitLab et Jenkins
 *
 * 1- avec ssh-keygen, créer clé publique et clé privée
 * 2- créer une clé de déploiement dans GitLab et y coller la clé publique : GitLab > Settings > repository > Deploy Keys
 * 3- créer un nouvel identifiant (ie Credential) dans Jenkins, de type "SSH Username with private key", remplir les champs, dont le champ "Private Key", avec le contenu de la clé privée
 * Attention : le champ "passphrase" correspond à celle choisie lors de la génération de la clef.
 * 4- coller l'ID généré par Jenkins dans l'attribut git_credentials
 *
 * @see <https://faas.forge.orange-labs.fr/documentation/master/userguide/index.html#gitlab-add-sshkey> ajouter une clé de déploiement dans GitLab
 * @see <https://faas.forge.orange-labs.fr/documentation/master/userguide/index.html#jenkins-credential-add-sshkey> ajouter une clé privée dans Jenkins
 */
def git_credentials = '918a6981-ed19-4945-8e78-063a791b4a51'

def svn_credentials = '17c4873b-2ea9-40da-a0d0-17a834b4f4e4'
def configFileId_maven = 'b13d25f3-f537-48d2-891c-1f7148c59f06'
def configFileId_gradle = '2a8244e0-bdf3-4ff1-8cfb-b8a009817ad3'
// def configFileId_gcloud = 'c6df4b66-b58d-466f-ad00-cd7b84fb9d0c'

/*
 * destinataire des mails de déploiement
 */
def defaultDeliveryRecipient = 'ludovic.soltys@orange.com'

pipeline {
    agent {
        label 'android'
    }

    tools {
        jdk 'Oracle JDK 8u131'
    }

    options {
        buildDiscarder(logRotator(artifactNumToKeepStr: '3', numToKeepStr: '10'))
        gitLabConnection('Gitlab Orange')
        timestamps()
    }

    environment {
        GRADLE_OPTS = "-Xmx1536m -Dorg.gradle.daemon=false -Dfile.encoding=UTF-8 -Dhttp.proxyHost=\"${env.http_proxy_host}\" -Dhttp.proxyPort=\"${env.http_proxy_port}\" -Dhttp.nonProxyHosts=\"${env.http_proxy_non_proxy_hosts}\" -Dhttps.proxyHost=\"${env.http_proxy_host}\" -Dhttps.proxyPort=\"${env.http_proxy_port}\" -Dhttps.nonProxyHosts=\"${env.http_proxy_non_proxy_hosts}\""
        CUSTOM_TOOLS_LICENSES = tool name: 'Android SDK licenses', type: 'com.cloudbees.jenkins.plugins.customtools.CustomTool'
        CUSTOM_TOOLS_TOOLS = tool name: 'Android SDK tools 25.2.5', type: 'com.cloudbees.jenkins.plugins.customtools.CustomTool'
        CUSTOM_TOOLS_XTC = tool name: 'XTC 0.1.55', type: 'com.cloudbees.jenkins.plugins.customtools.CustomTool'
        CUSTOM_TOOLS_GCLOUD = tool name: 'Google Cloud SDK CLI 184.0.0', type: 'com.cloudbees.jenkins.plugins.customtools.CustomTool'
        GIT_TAG_NAME = gitTagName()
    }

    stages {
        stage('Checkout') {
            steps {
                deleteDir()

                notifyBuild('STARTED')
                updateGitlabCommitStatus name: 'build', state: 'running'

                dir('ocara-opensource') {
                    git branch: "${env.BRANCH_NAME}", credentialsId: git_credentials, url: git_url_ocara_opensource
                }
            }
        }

        stage('Build') {
            steps {
                /**
                 * Building all the variants that are interesting for Jenkins
                 */
                dir('ocara-opensource') {
                    configFileProvider([configFile(fileId: configFileId_gradle, targetLocation: "${env.HOME}/.gradle/gradle.properties")]) {
                        sh 'chmod +x ./gradlew'
                        sh "./gradlew clean"
                        sh "./gradlew assembleOrangewithoutsmtkPreprodRelease"
                        sh "./gradlew assembleOrangewithoutsmtkIntegrationDebug"
                        sh "./gradlew assembleOrangewithoutsmtkIntegrationRelease"
                        sh "./gradlew assembleOpensourcePreprodRelease"
                        sh "./gradlew assembleOpensourceIntegrationDebug"
                    }
                }
            }
            post {
                always {
                    dir('ocara-opensource') {
                        /**
                         * publishHTML: Publish HTML reports
                         *
                         * @param target : Nested Object
                         * - reportName : The name of the report to display for the build/project, such as "Code Coverage", Type: String
                         * - reportDir : The path to the HTML report directory relative to the workspace, Type: String
                         * - reportFiles : The file(s) to provide links inside the report directory, Type: String
                         * - reportTitles : The optional title(s) for the report files, which will be used as the tab names. If this is not provided, file names will be used instead, Type: String
                         * - keepAll : If checked, archive reports for all successful builds, otherwise only the most recent, Type: boolean
                         * - alwaysLinkToLastBuild : If this control and "Keep past HTML reports" are checked, publish the link on project level even if build failed, Type: boolean
                         * - allowMissing : If checked, will allow report to be missing and build will not fail on missing report
                         *
                         * <a href="https://jenkins.io/doc/pipeline/steps/htmlpublisher"/>
                         */
                        // Dexcount report
                        publishHTML(target: [allowMissing: true, alwaysLinkToLastBuild: true, keepAll: true, reportDir: 'app/build/outputs/dexcount/', reportFiles: "index.html", reportName: 'Dexcount'])
                        // Profile report
//                        publishHTML(target: [allowMissing: true, alwaysLinkToLastBuild: true, keepAll: true, reportDir: 'build/reports/profile', reportFiles: "profile*.html", reportName: 'reports'])

                        // Tell Jenkins to archive the apks
                        archiveArtifacts artifacts: '**/build/outputs/apk/**/*.apk', fingerprint: true
                        archiveArtifacts artifacts: '**/mapping.txt', fingerprint: true
                        stash includes: '**/build/outputs/apk/**/*.apk', name: 'artifacts'
                    }
                }
            }
        }

        stage("Testing") {
            parallel {
                stage('Unit Tests') {
                    steps {
                        dir('ocara-opensource') {
                            configFileProvider([configFile(fileId: configFileId_maven, targetLocation: "${env.HOME}/.m2/settings.xml")]) {
                                sh "./gradlew clean ${testVariant}"
                            }
                        }
                    }
                    post {
                        always {
                            dir('ocara-opensource') {
                                // JUnit report
                                junit '**/TEST-*.xml'
                                publishHTML(target: [allowMissing: true, alwaysLinkToLastBuild: true, keepAll: true, reportDir: "app/build/reports/tests/${testVariant}", reportFiles: "index.html", reportName: 'UnitTest'])
                            }
                        }
                    }
                }

//                stage('Functional Tests') {
//                    when {
//                        expression {
//                            (env.BRANCH_NAME =~ /master$/ || env.BRANCH_NAME =~ /develop$/)
//                        }
//                    }
//                    steps {
//                        dir('ocara-opensource') {
//                            sh "${env.CUSTOM_TOOLS_XTC}/xtc/xtc test app/build/outputs/apk/opensource/debug/app-opensource-debug.apk ce58c92da41d3b6239b1321133abdc47 --devices bf5dc01b --series \"master\" --user ${defaultDeliveryRecipient} --workspace app/build/outputs/apk/androidTest/opensource/debug/"
//                        }
//                    }
//                }

//                stage('Robo Test') {
//                    when {
//                        expression {
//                            (env.BRANCH_NAME =~ /master$/ || env.BRANCH_NAME =~ /develop$/)
//                        }
//                    }
//                    steps {
//                        dir('ocara-opensource') {
//                            configFileProvider([configFile(fileId: configFileId_gcloud, targetLocation: "./gcloud-key.json")]) {
//                                sh "${env.CUSTOM_TOOLS_GCLOUD}/google-cloud-sdk/bin/gcloud auth activate-service-account --key-file \"gcloud-key.json\" --project \"ocara-4589c\""
//                                sh "${env.CUSTOM_TOOLS_GCLOUD}/google-cloud-sdk/bin/gcloud firebase test android run --type robo --app app/build/outputs/apk/opensource/debug/app-opensource-debug.apk --device model=flounder,version=21,locale=en,orientation=landscape --timeout 90s"
//                            }
//                        }
//                    }
//                }
            }
        }

        /*
         * executes Lint, when NOT on branch 'master' or 'develop'. Scans the Android projects and
         * reports on potential bugs, performance, security and translation issues.
         *
         * when finished, publishes the results.
         *
         * @see <https://wiki.jenkins.io/display/JENKINS/Android+Lint+Plugin>
         */
        stage('Lint') {
            when {
                not {
                    expression {
                        (env.BRANCH_NAME =~ /master$/ || env.BRANCH_NAME =~ /develop$/)
                    }
                }
            }
            steps {
                dir('ocara-opensource') {
                    sh "./gradlew clean ${lintVariant}"
                }
            }
            post {
                always {
                    dir('ocara-opensource') {
                        // Lint report
                        androidLint pattern: '**/lint-results*.xml', unstableTotalHigh: '0'
                        publishHTML(target: [allowMissing: true, alwaysLinkToLastBuild: true, keepAll: true, reportDir: 'app/build/reports/', reportFiles: "lint-results-*.html", reportName: 'App Lint'])
                    }
                }
            }
        }

        /*
         * executes Sonar, when on branch 'master' or 'develop' only.
         *
         * when finished, records some artifacts.
         *
         * @see <https://jenkins.io/doc/pipeline/tour/tests-and-artifacts/>
         */
//        stage('Sonar') {
//            when {
//                expression {
//                    (env.BRANCH_NAME =~ /master$/ || env.BRANCH_NAME =~ /develop$/)
//                }
//            }
//            steps {
//                dir('ocara-opensource') {
//                    /**
//                     * la variable "Sonar-FaaS" est défini dans : Jenkins > configuration > SonarQube servers
//                     *
//                     * see http://10.192.35.187/jenkins/configure
//                     */
//                    withSonarQubeEnv('Sonar-FaaS') {
//                        sh "./gradlew sonarqube -Dsonar.projectVersion=${GIT_TAG_NAME} -Dsonar.scm.provider=git -Dsonar.verbose=true --scan"
//                    }
//                }
//            }
//            post {
//                always {
//                    dir('ocara-opensource') {
//                        archiveArtifacts artifacts: '**/build/outputs/apk/**/*.apk', fingerprint: true
//                        archiveArtifacts artifacts: '**/mapping.txt', fingerprint: true
//                    }
//                }
//            }
//        }

        /*
         * When on branch 'master',
         * - adds the artifacts that were produced to the SVN repo.
         * - sends a mail about the result
         */
        stage('Deploy') {
            when {
                branch 'master'
            }
            steps {
                echo 'Deploying to SVN'
                retry(3) {
                    commitArtifactsToSVN(svn_credentials)
                }
                notifyDeployedVersion(defaultDeliveryRecipient)
            }
        }
    }

    /*
     * after any process,
     * - updates the commit status in GitLab
     * - cleans the workspace
     * - sends a mail about the result
     *
     * @requires gitlab-plugin
     * @see <https://jenkins.io/doc/pipeline/steps/gitlab-plugin/#code-updategitlabcommitstatus-code-update-the-commit-status-in-gitlab>
     */
    post {
        failure {
            updateGitlabCommitStatus name: 'build', state: 'failed'
        }
        success {
            updateGitlabCommitStatus name: 'build', state: 'success'
            cleanupWorkspace()
        }
        aborted {
            updateGitlabCommitStatus name: 'build', state: 'canceled'
        }
        always {
            notifyBuild(currentBuild.result)
        }
    }
}

/*
 * Send a release email
 * Relies on :
 * - APPLICATION_NAME
 * - GITLAB_PROJECT_URL
 * - SVN_TAGS_URL
 * - GIT_TAG_NAME
 * - PREVIOUS_GIT_TAG
 * - BUILD_URL (injected by default by Jenkins)
 */
def notifyDeployedVersion(String recipient) {
    if (GIT_TAG_NAME != null) {
        dir('ocara-opensource') {
            // Using env.XXX instead of the environment directive to use the env vars in the email template
            env.GIT_TAG_NAME = gitTagName()
            env.GIT_PREVIOUS_TAG_NAME = gitPreviousTagName()
            env.SVN_TAGS_URL = 'https://svn-oab.si.fr.intraorange/svn/A10303/tags/Prod'
            env.APPLICATION_NAME = 'Ocara'
            env.GITLAB_PROJECT_URL ='https://gitlab.forge.orange-labs.fr/OCARASoftwareSolution/Ocara'
            env.JIRA_PROJECT_URL = 'https://jira-build.orangeapplicationsforbusiness.com/projects/OCARAB'
        }
        dir('ocara-orange') {
            echo 'Deploying to SVN'
            emailext(subject: "Nouvelle version - ${APPLICATION_NAME}",
                    body: '${SCRIPT, template="template.html"}',
                    mimeType: 'text/html',
                    to: "${recipient}",
                    recipientProviders: [[$class: 'DevelopersRecipientProvider']])
        }
    } else {
        error "Tag is malformed"
    }
}

/*
 * Commits the artifacts that have been stashed, and push them to SVN
 *
 * @param credentials the credential to be used to push to SVN
 */
def commitArtifactsToSVN(String credentials) {
    if (GIT_TAG_NAME != null) {
        sh "rm -Rf svn-tmp; mkdir -p svn-tmp"
        dir('svn-tmp') {
            withCredentials([usernamePassword(credentialsId: credentials, passwordVariable: 'PROJECT_ACCOUNT_PASSWORD', usernameVariable: 'PROJECT_ACCOUNT_USERNAME')]) {
                sh "svn co ${SVN_TAGS_URL} . --depth empty --trust-server-cert --non-interactive --username ${PROJECT_ACCOUNT_USERNAME} --password ${PROJECT_ACCOUNT_PASSWORD}"
                sh "mkdir -p v${GIT_TAG_NAME}_jenkins_${BUILD_ID}"
                unstash name: 'artifacts'
                sh "find . -type f | grep -i apk\$ | xargs -i cp {} v${GIT_TAG_NAME}_jenkins_${BUILD_ID}"
                sh "svn add --parents v${GIT_TAG_NAME}_jenkins_${BUILD_ID}/*.apk;"
                sh "svn commit -m \"v${GIT_TAG_NAME} apks added automatically by Jenkins\" --trust-server-cert --non-interactive --username ${PROJECT_ACCOUNT_USERNAME} --password ${PROJECT_ACCOUNT_PASSWORD}"
            }
        }
    } else {
        error "The tag name must not be null, did you forget to create a Tag? Did you push it?"
    }
}

/*
 * Get the Git tag name
 *
 * @return the tag name
 */
def gitTagName() {
    desc = sh(script: "git describe --tags ${GIT_COMMIT}", returnStdout: true)?.trim()
    if (isTag(desc)) {
        return desc
    }

    return null
}

/*
 * Get the previous Git tag name
 *
 * @return the previous tag name, can be null
 */
def gitPreviousTagName() {
    desc = sh(script: "git describe --tags ${GIT_COMMIT}^", returnStdout: true)?.trim()
    if (isTag(desc)) {
        return desc
    }

    return null
}

/*
 * Cleans the workspace
 */
def cleanupWorkspace() {
    cleanWs patterns: [[pattern: '**/build/intermediates/**', type: 'INCLUDE'],
                       [pattern: '**/build/outputs/mapping/**', type: 'INCLUDE'],
                       [pattern: '**/build/generated/**', type: 'INCLUDE'],
                       [pattern: '**/build/outputs/apk/**/*.apk', type: 'INCLUDE'],
                       [pattern: '**/.gradle/**', type: 'INCLUDE'],
                       [pattern: '**/svn-tmp/**', type: 'INCLUDE']]
}

/*
 * Send a generic email and a mattermost message
 *
 * @param buildStatus the build status
 */
def notifyBuild(String buildStatus = 'STARTED') {
    // Build status of null means successful
    buildStatus = buildStatus ?: 'SUCCESS'

    // Default values
    def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
    def summary = "${subject} (${env.BUILD_URL})"
    def details = """<p>STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
    <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${
        env.BUILD_NUMBER
    }]</a>&QUOT;</p>"""

    // Override default values based on build status
    if (buildStatus == 'STARTED') {
        colorName = 'warning'
    } else if (buildStatus == 'SUCCESS') {
        colorName = 'good'
    } else {
        // 'FAILURE' or 'UNSTABLE' or 'DANGER'
        colorName = 'danger'
    }

    // Send notifications
    mattermostSend color: colorName, message: summary

    emailext(subject: subject,
             body: details,
             mimeType: 'text/html',
             recipientProviders: [[$class: 'DevelopersRecipientProvider']]
    )
}

// NonCPS methods
@NonCPS
boolean isTag(String desc) {
    match = desc =~ /.+-[0-9]+-g[0-9A-Fa-f]{6,}$/
    result = !match
    match = null // prevent serialisation
    return result
}